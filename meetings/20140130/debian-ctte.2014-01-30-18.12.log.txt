18:12:06 <dondelelcaro> #startmeeting
18:12:06 <MeetBot> Meeting started Thu Jan 30 18:12:06 2014 UTC.  The chair is dondelelcaro. Information about MeetBot at http://wiki.debian.org/MeetBot.
18:12:06 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
18:12:09 <Diziet> Oh bum.
18:12:11 <dondelelcaro> sorry that I'm lagging
18:12:14 <Diziet> dondelelcaro: Hi.
18:12:19 * bdale is here now
18:12:20 <Diziet> Damn, I knew something was wrong.
18:12:25 <Diziet> Hi both of you.
18:12:26 <dondelelcaro> no worries
18:12:30 * Diziet <- can't work bots.
18:12:39 <keithp> Diziet: I think it's mostly a matter of making sure everyone has a chance to read the proposed vote and make sure they feel comfortable ranking amongst those choices
18:12:44 <Diziet> Right.
18:12:57 <bdale> sorry, was afk for a while and lost track of time
18:13:04 <Diziet> Basically Philipp observed that the compromise "multiple" text that I ended up with after talking to Don fails to deal with the question of depending (or requiring) particular inits.
18:13:33 <Diziet> Personally I think that's important and if the rest of you don't object to me putting it on the ballot I'd prefer for it to be explicitly rejected by being voted down, rather than just not considered.
18:13:46 <rra> I think we could have a much clearer vote on that topic, if it still requires a vote, after we've picked a default.  I think the nature of the question is much different if we're using systemd vs. upstart.
18:13:56 <bdale> I agree with rra
18:14:11 * keithp too
18:14:16 <Diziet> rra: I think it's intertwined with the default and I vigorously object to a sequential ballot.
18:14:33 <Diziet> If you think the answers are different in the different cases you can rank them appropriately IMO.
18:14:56 <rra> The problem is that now we are voting on 18 options, if I'm counting properly.
18:15:08 <Diziet> No, we'll leave some of those out.
18:15:11 <rra> If we include that combinatoric.
18:15:19 <bdale> how would you handle it in a single ballot?  my problem with doing it all at once is that the ballot seems to get overly complicated, and I think we could spend more time tweaking the ballot than just having multiple votes
18:15:22 <Diziet> I don't think we need to include the text about multiple options in the openrc or sysvinit cases.
18:15:31 <dondelelcaro> the only need for different options is if someone's ranking of the main options is dependent on the secondary options
18:15:36 <Diziet> dondelelcaro: Mine is.
18:16:15 <Diziet> Ie, my ranking isn't compatible with asking the questions separately.
18:16:22 <dondelelcaro> OK
18:16:36 <bdale> Diziet: so I gather the draft ballot I saw from you in email isn't yet actually a ballot you want to vote on?
18:17:07 <Diziet> bdale: It is a ballot _I_ want to vote on but the process is (supposed to be) that everyone gets to propose amendments of their own.
18:17:18 <Diziet> bdale: Or maybe I misunderstand you.
18:17:35 <Diziet> So I guess at most we'd be looking at 3x upstart, 3x systemd, openrc, sysvinit, GR, FD.#
18:17:42 <rra> I'm not sure that I understand.  The ballot you proposed allows explicit dependencies on non-default init systems, which I thought you were opposed to.
18:17:52 <Diziet> rra: Oh I see.
18:18:12 <bdale> Diziet: I was reacting to...
18:18:15 <bdale> <Diziet> Personally I think that's important and if the rest of you don't object to me putting it on the ballot I'd prefer for it to be explicitly rejected by being voted down, rather than just not considered.
18:18:28 <bdale> that made it sound to me like you don't think your latest draft is complete yet?
18:18:31 <aba> sorry for being late
18:18:42 <Diziet> There's a spectrum there: no explicit dependencies (what I want); things should work if feasible and maintainers should take patches (Don's compromise text); nothing on the subject (Russ, Keith, Bdale).
18:18:46 <Diziet> AIUI
18:19:04 <bdale> I consider Don's "compromise text" and saying nothing at all to be completely equivalent
18:19:08 <rra> Oh, do you feel like what's included in M is an acceptable compromise?
18:19:09 <Diziet> bdale: Right, in that sense.  I have hesitated putting those on the ballot because I wasn't sure if you would mind.
18:19:39 <bdale> because I believe the compromise text just captures existing best practice / normal Debian behavior
18:20:01 <rra> I'm personally fine with M and happy to vote it above the non-M options, so if you feel like that addresses your concerns, maybe we don't need another variation?
18:20:09 <aba> I'm not sure all people agree what are best practives in this context
18:20:16 <Diziet> rra: Ideally I would prefer to be able to vote in favour of my own preferred resolution.
18:20:40 <Diziet> OK, then, if everyone is happy with the text in M as it stands then we can put M in all the versions.
18:20:47 <Diziet> And then I would definitely like a stronger form.
18:20:52 <bdale> what is "M" in this context?
18:21:01 <keithp> bdale: don's compromise text
18:21:06 <aba> 727708_initsystem/draft-resolution.txt - rider M
18:21:21 <bdale> I have no problem with Don's text being included
18:21:34 <Diziet> bdale: Everything between
18:21:34 <aba> I'd prefer it to be there
18:21:37 <Diziet> == optional rider M (Multiple init systems) ==
18:21:38 <Diziet> and
18:21:41 <Diziet> == rider for all versions except GR ==
18:21:41 * bdale finds lettering things as confusing as Diziet apparently finds numbering things...
18:21:46 <Diziet> in that file that aba mentions.
18:21:55 <aba> but it doesn't answer on "could packages depend on a specific init system"
18:21:57 <rra> The problem with numbering is that voting is then ambiguous between options and ranks.
18:22:04 <aba> which I think should be explcitly answered by us
18:22:26 <keithp> aba: do you think that should be a separate vote?
18:23:05 <bdale> aba: I think that's a follow-on question not related to the choice of default.  Diziet apparently thinks it needs to be voted at the same time since it *would* change his ordering of defaults.  In that case, I think we just need to figure out how to include the right choices.
18:23:08 <Diziet> aba: I agree that it would be better to say that explicitly one way or the other.
18:23:15 <Diziet> bdale: Right.
18:23:27 <bdale> I'm happy to rank down options I don't think make sense or don't want to support, so having a longer ballot isn't horrible .. just annoying
18:23:56 <aba> bdale: it would change the position of "further discussion" for me
18:24:13 <Diziet> Is there anyone who wants the question of whether dependency on a particular init system is OK to go unanswered ?
18:24:22 <Diziet> I mean, who wants the TC just to not answer that question.
18:24:30 <rra> No, I don't think that would be a good idea.
18:24:37 <Diziet> If not then we should have explicit "yes" and "no" options.
18:24:39 <bdale> I'd be fine not answering the question, leaving the default that it's ok to have such dependencies
18:24:40 <rra> It's pretty clearly part of the question under contention.
18:24:52 <bdale> because that's the way I'd vote
18:25:02 <rra> bdale: My concern is that people won't assume that's what us saying nothing actually means.
18:25:05 <Diziet> bdale: Would you _prefer_ not to answer it, in the sense that you want a version on the ballot which _doesn't_ answer that question ?
18:25:08 <bdale> but I don't feel compelled to keep the rest of you from expressing your opinions on the matter, so I'm ok with explicitly voting it
18:25:17 <aba> I think we should speak it out, even if we use the default
18:25:21 <Diziet> bdale: OK good.
18:25:33 <Diziet> How about then if I just negate my wording
18:25:38 <bdale> rra: I don't share that concern, fwiw
18:25:45 <rra> Okay.  Maybe I'm too paranoid.
18:25:51 * aba agrees with rra
18:26:00 <aba> (also on the last sentence)
18:26:05 <bdale> that's actually an interesting observation
18:26:15 <Diziet> So I have for "not allowed"   Software outside of an init system's implementation may not require a specific init system to be pid 1, although degraded operation is tolerable.
18:26:31 <Diziet> For "allowed" it would read    Software outside of an init system's implementation may require require a specific init system to be pid 1.
18:27:14 <aba> I would put the second as "as long as the interoperation req is followed" (in nicer words)
18:27:19 <Diziet> (Along with Don's text about sensible patches.)
18:27:33 <bdale> s/software outside of an init system/any package/ in the second option?
18:27:35 <Diziet> aba: I'm afraid I don't follow.
18:27:40 <Diziet> bdale: Yes, that would be better.
18:27:51 <bdale> and lose the doubling of require
18:27:52 <Diziet> "Any software"
18:28:03 <bdale> ok
18:28:06 <Diziet> | Any software may require a specific init system to be pid 1.
18:28:09 <aba> Diziet: we can't say "you may depend on one init system" and "you need to interoperate with any where you have sensible patches"
18:28:22 <Diziet> aba: OIC
18:28:59 <dondelelcaro> aba: but we can say that you can initially depend on a single init system, but should accept patches to interoperate
18:29:04 <Diziet> aba: I think surely we aren't saying that you are allowed to depend on one init system even if there are sensible patches to support others" ?
18:29:08 <aba> e.g. extending the last sentence of M with "Any package might depend on the specific init system(s) where it interoperates with"?
18:29:14 <bdale> so the way to do both might be to instead of saying you can depend on a particular init system, to instead say it's ok to not work with all init systems equally?
18:29:17 <aba> dondelelcaro: absolutly.
18:29:28 <Diziet> | Software may require a specific init system to be pid 1.
18:29:33 <Diziet> | However, where feasible [stuff about patches]
18:30:23 <Diziet> | However, where feasible, software should interoperate with all init systems; maintainers are encouraged to accept [blah blah]
18:30:38 <Diziet> Is that capturing your views ?
18:31:39 <dondelelcaro> yeah, I think so
18:31:46 <Diziet> Is the para
18:31:57 <Diziet> | Debian intends to support multiple init systems, for the foreseeable future, and so long as their respective communities and code remain healthy.
18:32:01 <Diziet> consensus then ?
18:32:14 <Diziet> Or does it only want to be in one of the versions ?
18:32:31 <Diziet> Or does the "allow tight coupling" version want a weaker form of it ?
18:32:32 <aba> re the first - ok-ish for me.
18:32:33 <bdale> I'm bothered by the "Debian intends" assertion
18:32:49 <bdale> I think multiple init systems are a reality
18:33:02 <Diziet> Suggest an alternative wording ?
18:33:05 <bdale> it's that way of phrasing it that I'm somehow just not comfortable with
18:33:06 <Diziet> "intents to continue to support" ?
18:33:09 <keithp> more like 'the TC acknowledges that multiple init systems will exist'?
18:33:15 <bdale> keithp: right
18:33:18 <bdale> something more like that
18:33:34 <bdale> acknowledges, accepts, recognizes
18:33:39 <Diziet> That's an empty phrase.  They might continue to exist but not be in Debian.
18:33:48 <aba> keithp: sounds good, but with an "and we don't indend to change that with this decision"
18:34:07 <aba> or "we want to keep that status"
18:34:10 <Diziet> "Multiple init systems will continue to exist within Debian"  ?
18:34:27 * keithp has to disappear now, alas. have fun kids
18:34:32 <Diziet> keithp: Ta.
18:34:40 <bdale> so my problem with your text is that there is no "Debian" to intend.  we as the TC have been asked to pick a default, and we assume picking a default doesn't mean excluding other options, but whether we'll actually support more than one in the future is a function of what happens in the future, not some top-down intent
18:35:28 <Diziet> The purpose of this phrase is to stop the choice of default being interpreted as a decision to drop or desupport others.
18:35:35 <aba> bdale: can we put it as "by choosing an default, we don't want to put limits on the init systems in use but encourage people to experiment with different sets of init"?
18:35:38 <Diziet> _We_ know that's not what a decision on the default means.
18:35:53 <Diziet> But I think it's necessary to make it clearr.
18:36:11 <Diziet> "Debian will continue to welcome contributions of support for multiple init systems" ?
18:36:46 <Diziet> "Debian will continue to welcome contributions of support for other init systems" ?
18:36:53 <Diziet> (since the previous para set the default)
18:37:05 <dondelelcaro> or just "This decision is limited to selecting a default initsystem, not dropping support for all other init systems"
18:37:08 <bdale> yeah, again what's really bothering me here is the assumption that anything we don't state explicitly will result in bad bahaviors .. if enough of you feel that's real that we need to be explicit about defaults that already exist and *should* be well understood, etc, then I'll go along with it... it's just not how I think people should be treated who generally act in good faith
18:37:29 <Diziet> bdale: Being explicit and avoiding misunderstanding is not the same as assuming bad faith.
18:37:53 <Diziet> dondelelcaro: That wording would satisfy me too.
18:38:02 <Diziet> bdale: Is there a version that you wouldn't want to oppose ?
18:38:11 <Diziet> I mean, some wording.
18:38:47 <bdale> I won't oppose a ballot with such wording, I just don't think it should be necessary to include
18:39:19 <aba> it looks to me that people are more ... agitated with the init system then with other questions.
18:39:38 <Diziet> OK, I'll go with  | This decision is limited to selecting a default initsystem; we will continue to welcome contributions of support for all init systems
18:39:52 <bdale> s/will /
18:39:57 <aba> also even if we don't make a final decision what is not supported, the non-defaults will be supported worse than others (as vorlon said last time, "end game for the init system")
18:40:07 <Diziet> bdale: sure.
18:41:39 <bdale> aba: I'm not convinced that it has to be so, though I think it's fairly obvious that if we don't pick upstart as default that its days are numbered.
18:42:23 <bdale> ok, so
18:42:35 <bdale> Diziet: you plan to update a draft with all that's been discussed here?
18:42:52 <KGB-3> 03Ian Jackson 05master 2ffbc80 06debian-ctte 10727708_initsystem/draft-resolution.txt 727708: tight/loose coupling texts, from irc
18:42:57 <Diziet> bdale: No, I don't plan to :-).
18:43:34 <Diziet> If you would care to take a look at what I've just pushed that would be great.
18:43:43 * bdale chuckles .. pulling
18:44:55 <dondelelcaro> Diziet: I think those options work for me
18:45:22 <Diziet> With those, we're going to have to have {openrc,sysvinit}-{tight,loose} on the ballot.
18:45:31 <Diziet> So that's going to be 8 real options, plus GR and FD.
18:46:32 <dondelelcaro> OK
18:46:57 <bdale> looks ok
18:47:17 <dondelelcaro> we should probably publish the draft to give cjwatson, vorlon, and keithp a chance to suggest changes, but I don't see why we can't start voting this weekend or something like that
18:47:18 <bdale> so a total of 10 options on the ballot
18:47:19 <Diziet> Thanks for your help.  Next time we get bogged down in drafting details we should do it via irc again.
18:47:26 <Diziet> bdale: Yes.
18:47:27 <bdale> Diziet: I think so, too
18:47:33 <Diziet> dondelelcaro: Yes.
18:47:42 <bdale> there's a time for real-time interaction, and joint drafting is one of them
18:47:47 <dondelelcaro> yeah
18:47:51 <Diziet> Why don't we say we'll call for votes on Monday night unless anyone objects before then.
18:47:52 <bdale> right
18:48:02 <dondelelcaro> sounds good to me
18:48:03 <rra> That sounds good here.
18:48:03 <bdale> Diziet: I'd love to start voting sooner, frankly
18:48:08 <Diziet> Yes.
18:48:23 <Diziet> But with people being away this weekend I don't think we can rely on them being around.
18:48:38 <dondelelcaro> I'm ok with voting sooner too, but I think at least 24 hours before calling would be good
18:48:41 <Diziet> I think it's important that any option anyone wants to put on the ballot can be there; that's how the constitutional process is supposed to work.
18:48:46 <Diziet> We don't want to have to cancel another one...
18:48:52 <ansgar> Monday night or when all people said they are okay with the options, whichever comes first?
18:48:59 <Diziet> ansgar: Yes.
18:49:11 <bdale> ok, but given the comments on the ballot I posted, I genuinely think this captures everything that has been asked for
18:49:25 <Diziet> Another 3 days extra won't hurt.
18:50:20 <dondelelcaro> #agreed start voting monday night or whenever everyone oks the ballot
18:50:29 <Diziet> We could put a message out on the bug saying "please no more messages on this subject from non-TC-members; if you have an objection to the proposed ballot please contact a TC member" ?
18:50:29 <dondelelcaro> anything else on this?
18:50:34 <bdale> not from me
18:50:39 <Diziet> No, I think that's it.
18:50:59 <aba> Diziet: I don't think that makes a difference (re no more messages)
18:51:13 <dondelelcaro> #topic #719738 lvm2 - Add systemd support
18:51:23 <dondelelcaro> this is done now; I've closed the bug
18:51:28 <dondelelcaro> #topic Additional Business
18:51:33 <bdale> I'm with aba, I think such a message would only wave red in front of the folks who are unhappy about the process and do nothing useful
18:51:46 <Diziet> bdale: Fair enough.
18:52:14 <bdale> I don't have any other business today .. regards to everyone at FOSDEM, I won't be there this year
18:52:42 <dondelelcaro> OK. I believe the next meeting is the 27th of february
18:52:53 <dondelelcaro> I need to get moving, so if there's nothing else, I'll stop here. ;-)
18:53:10 <bdale> thanks, dondelelcaro, as usual I appreciate your effort running these
18:53:13 <Diziet> Thanks.
18:53:17 <dondelelcaro> bdale: no problem; sorry I was running late
18:53:22 <dondelelcaro> totally got sidetracked
18:53:24 <bdale> I was too... ;-)
18:53:33 <dondelelcaro> #endmeeting