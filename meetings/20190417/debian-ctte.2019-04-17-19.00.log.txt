19:00:03 <marga> #startmeeting
19:00:03 <MeetBot> Meeting started Wed Apr 17 19:00:03 2019 UTC.  The chair is marga. Information about MeetBot at http://wiki.debian.org/MeetBot.
19:00:03 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
19:00:09 <marga> #topic Roll Call
19:00:15 <marga> Margarita Manterola
19:01:59 <OdyX> Didier Raboud
19:02:57 <marga> :-/
19:03:11 <marga> Gunnar said that he wouldn't be able to attend.
19:03:30 <marga> I didn't see anything from the rest, did anybody else say they weren't coming?
19:05:12 <marga> Alright, let's move on. I guess it's just the two of us...
19:05:19 <OdyX> Cute :-)
19:05:24 <marga> #topic Review of previous meeting AIs
19:05:33 <marga> http://meetbot.debian.net/debian-ctte/2019/debian-ctte.2019-03-20-19.00.html
19:05:40 <marga> Is the log of the previous meeting
19:05:56 <marga> We had two AIs and failed at both, and so we still have the same two bugs open...
19:06:13 <OdyX> yeah. No input happened whatsoever on either.
19:06:20 <marga> #topic #904558 What should happen when maintscripts fail to restart a service
19:06:32 <marga> So, I drafted this: https://oasis.sandstorm.io/grain/Wh98knZvAHaZgdzqp4DYQE
19:06:49 <marga> It's an etherpad on sandstorm, I couldn't find if there was a better pad to be used.
19:07:05 <marga> I guess I'd appreciate feedback on it, and then I'd send it after the meeting.
19:07:31 <OdyX> it needs login ftr
19:07:41 <marga> ah :(
19:07:56 <OdyX> I usually use either https://framapad.org or https://pad.riseup.net
19:08:01 <marga> Try now?
19:08:10 <OdyX> better
19:08:17 <OdyX> ah no, not better
19:08:19 <OdyX> sorry.
19:08:30 <marga> Ok, will move to rise up, give me a sec
19:08:57 <marga> https://pad.riseup.net/p/ntoBv46kJXfLrKEDQSz_
19:10:54 <OdyX> I'd expand slightly on the "the TC does not engage"
19:11:16 <OdyX> the core point is that we can't do it _as a body_, and we need larger help.
19:12:48 <OdyX> otherwise, it's good.
19:13:01 <marga> Ok, I've added an extra sentence, is that enough, or more?
19:13:04 <OdyX> Maybe propose a place to gather that working-group.
19:13:16 <marga> -project? -devel?
19:14:54 <OdyX> hrm. Both seem suboptimal.
19:15:05 <Mithrandir> hi, sorry for being late, I was in a car.
19:15:21 <marga> I don't think we have a no-bikeshedding-development list
19:15:32 <OdyX> Any working group will need guidance and steering
19:16:01 <OdyX> and that one will need someone™ with time.
19:16:51 <smcv> hi
19:17:10 <smcv> sorry for the delay, reading scrollback
19:17:34 <marga> Hi, Mithrandir and smcv.  I was asking OdyX for feedback on https://pad.riseup.net/p/ntoBv46kJXfLrKEDQSz_ which is what I intend to send to close the maintscripts bug
19:18:01 <marga> Happy to hear your feedback as well
19:20:01 <smcv> my only reservation about this is that it's very possible that it's going to give us another Debian-specific solution that no other distro uses, resulting in Debian maintaining a weird Debianism for all time
19:20:31 <marga> RIght, but this is a need that all distros have, no?
19:20:59 <smcv> all distros that do significant surgery on a running system, at least
19:21:07 <marga> So, maybe include that if possible this should be distro-agnostic and shared with other distros?
19:21:18 <smcv> perhaps
19:21:41 <smcv> on one hand, maintainer scripts (in the form that they take in Debian) *are* very Debian-specific
19:21:49 <marga> Yes
19:21:56 <marga> But notifying maintainers is a common problem.
19:22:07 <marga> sysadmins, sorry
19:22:14 <marga> It's a common, unsolved problem.
19:22:15 <smcv> on the other hand, "like o.fd.Notifications, but for the OS" is not a distro-specific need
19:23:06 <smcv> I'm not saying the solution should necessarily involve D-Bus, but it would be nice if it didn't involve too many Debian-specific interfaces either
19:23:21 <marga> Yup
19:23:54 <smcv> for instance something that's full of deb822 would not be likely to encourage cross-distro or cross-desktop or otherwise cross-ecosystem communication
19:23:57 <marga> I'm wary of having dbus in the pipeline, but I guess that at this point it's in the pipeline of so many things already...
19:24:03 <OdyX> Not making it systemd-specific will be… costly.
19:24:34 <marga> Anyway, let's not go into design :)
19:25:04 <marga> Is there something else we should recommend regarding how the group should be formed / operate?
19:25:13 <smcv> yeah, the only reason I mentioned D-Bus is that o.fd.Notifications is an example of an interface that *has* been widely successful cross-desktop and cross-distro
19:25:34 <Mithrandir> that mail looks reasonable to me, fwiw.
19:25:37 <smcv> and it might be mistaken for a solution to this need (it isn't suitable, but it's in a vaguely similar space)
19:26:51 <smcv> mail looks good to me
19:26:57 <marga> ok, thanks
19:27:10 <marga> #topic #923450 Requirements for being pre-dependency of bin:init
19:27:52 <marga> Last time we discussed this at length and gave the AI to Niko to reply to the bug
19:28:28 <marga> I think OdyX had summarized the consensus as: "as things stand, we recognize that the bin:init maintainers are the effective gatekeeper, but it's fine as is; we just recommend that they specify their requirements better, in a README.Debian for example"
19:29:18 * smcv reads last time's minutes
19:29:35 <marga> The minutes are too short.  I'm skimming through the log
19:29:54 <smcv> yeah, sorry, that's what I meant
19:30:43 <smcv> I think something I would want to see in any set of requirements for being an init implementation is the principle that asking to be an init alternative should be the last thing you do before your init is ready for wide use
19:31:39 <OdyX> indeed
19:31:40 <smcv> as in, replacing the default init or another well-supported init with your new init should result in a system that still basically works and is supportable
19:31:51 <smcv> not a system that needs a lot of handholding to be usable
19:32:08 <marga> Sure.  But what does it mean for a system to work and be supportable? That's part of what the guidelines should say
19:32:17 <smcv> (I don't know where runit is on a scale from "needs lots of handholding" to "everything works like you'd expect")
19:32:22 <marga> The reporter asked us to write those guidelines
19:32:34 <marga> And we said that they should be written by the maintainers of the package.
19:33:13 <OdyX> It's very different to be in the install path of a "init" package, and being the default
19:34:04 <marga> smcv, to recap the original bug: first it needed a lot of handholding, the systemd maintainers tested it and reported issues. The runit dev fixed those issues and came back. They tested it again, it still needed some handholding. After a couple of rounds they stopped responding to the bug. Eventually the runit maintainer uploaded an NMU to delayed that was nacked by mail, but allowed to proceed and not reverted and it's what's in testing now.
19:36:04 <smcv> on one hand it's hard to swap init implementation to one that init doesn't have as an alternative, and that holds back adoption of non-default inits
19:36:33 <marga> Yeah
19:36:33 <smcv> on the other hand how many inits is every system service maintainer expected to be able to support?
19:36:55 <marga> Only systemd and sysv?
19:36:58 <ansgar> smcv: All currently?
19:37:37 <OdyX> FSVO "support"
19:38:06 <OdyX> The expectation is AFAIK "merge reasonable-looking patches, test on your usual init"
19:42:28 <marga> So, last time we seemed to have consensus on letting the systemd maintainers own the guidelines.  Just asking them to publish them
19:43:33 <OdyX> I haven't moved much
19:43:51 <marga> smcv, do you think you can take the AI of closing the bug?
19:44:13 <marga> You seem to have ideas of what the guidelines should be, so you can maybe add those, as your personal suggestions. :)
19:44:16 <smcv> would it maybe be helpful to suggest having a lower bar for accepting init alternatives in experimental than in unstable?
19:45:04 <smcv> ok, I'll try to summarize on the bug this weekend
19:45:19 <marga> Thanks@
19:45:27 <smcv> with a reply with my own suggestions (or something)
19:45:40 <Mithrandir> fwiw, as systemd maintainer, we did not have much trouble with not shipping /sbin/init for a while and just recommending people use init= in grub.
19:46:20 <marga> #action smcv to close the bug letting parties know that we think the status quo of systemd maintainers being gatekeepers is ok, and that they should publish the guidelines for inclusion.
19:46:53 <marga> Alright, let's move on to 'Any other business'
19:46:58 <marga> #topic Any other business
19:47:33 <OdyX> How was the perspective of half-DPLship marga?
19:47:42 <marga> scary
19:47:56 <marga> But I guess I encouraged enough people to run :)
19:48:24 <marga> I read something about DebConf in the backlog. Who's planning on attending? Should we have the traditional TC talk?
19:48:38 <marga> I'm planning to go but I haven't finalized my plans yet.
19:49:36 <smcv> I won't be at debconf this time round
19:49:49 <OdyX> Not this year either.
19:52:14 <marga> Mithrandir, ?
19:54:15 <marga> I guess he went away.
19:54:30 <marga> Anyway, it seems there's nothing else to discuss, so let's close now
19:54:35 <marga> #endmeeting