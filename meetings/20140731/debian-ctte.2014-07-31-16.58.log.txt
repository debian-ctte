16:58:24 <dondelelcaro> #startmeeting
16:58:24 <MeetBot> Meeting started Thu Jul 31 16:58:24 2014 UTC.  The chair is dondelelcaro. Information about MeetBot at http://wiki.debian.org/MeetBot.
16:58:24 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
16:58:28 <dondelelcaro> #topic Who is here?
16:58:31 <dondelelcaro> Don Armstrong
16:58:38 <dondelelcaro> (sorry; didn't have the logs started fast enough)
16:58:40 <bdale> Bdale Garbee
16:59:00 <vorlon> Steve Langasek
16:59:05 <Diziet> Ian Jackson
16:59:33 <vorlon> I don't believe cjwatson is making it to the meeting today
16:59:40 <aba> Andi Barth
16:59:47 * bdale pokes keithp out of band
17:00:20 <dondelelcaro> let me see if I can poke rra, but in the meantime we can get started
17:00:29 <bdale> wfm
17:01:04 <bdale> I know keithp is traveling today, don't know if he'll be able to make it .. just SMS'ed him
17:01:08 <dondelelcaro> #topic Next Meeting?
17:01:10 <dondelelcaro> cool
17:01:22 <dondelelcaro> I think the next meeting is currently scheduled for Debconf
17:01:31 <bdale> we have a session scheduled
17:01:37 <dondelelcaro> so whenever that BoF/talk actually ends up being scheduled is when it will happen
17:01:40 <dondelelcaro> cool
17:01:43 <bdale> either Mon or Tue at 10am, I forget which is this and which is SPI BoF
17:01:58 <dondelelcaro> OK; I won't be able to make either of those, I think.
17:02:22 <bdale> fwiw, I'll be at Debconf through Wed night, I have a conflict Thu through the holiday weekend
17:02:37 <dondelelcaro> yeah; I'll only be there until sunday night or very early monday monring
17:02:56 <bdale> ack from keithp, he's working on joining us
17:03:09 <dondelelcaro> the normal time for the september meeting is the 25th at 17:00 UTC; I'll just pencil that in, and if it doesn't work for people, it can be changed at the august meeting
17:03:22 <dondelelcaro> #action dondelelcaro to pencil in september meeting on 25th at 17:00 UTC
17:03:29 <bdale> sounds plausible, thanks
17:03:36 <Diziet> That's slightly awkward for me but we can change it later.
17:03:42 <dondelelcaro> Diziet: cool
17:03:52 <dondelelcaro> yeah, whatever you guys agree on at the meeting is fine by me
17:04:01 <dondelelcaro> #topic #717076 Decide between libjpeg-turbo and libjpeg8 et al.
17:04:14 <dondelelcaro> #action dondelelcaro confirm the vote on this and get it on website
17:04:15 <bdale> not sure the next irc meeting time will really be on agenda at debconf session
17:04:20 <dondelelcaro> OK
17:04:35 <bdale> those sessions are usually more a quick rundown on closed in the last year and open issues then Q&A
17:04:35 <dondelelcaro> true; I guess I can bandy it out through e-mail too
17:04:45 <aba> I'm also not sure that mixing a live and IRC meeting is so helpful
17:04:45 <bdale> probably a better plan
17:04:49 <keithp> sorry I'm late
17:05:00 <bdale> aba, yeah that's been tough in the past
17:05:03 <dondelelcaro> I think this issue just needs someone (me?) to deal with it
17:05:12 <dondelelcaro> so I'll take care of that
17:05:13 <bdale> yep
17:05:15 <bdale> thanks
17:05:16 <dondelelcaro> #topic #636783 constitution: super-majority bug
17:05:34 <dondelelcaro> vorlon: thanks for commenting on the agenda, BTW; I had forgotten where some of these were
17:05:35 <Diziet> Matthew V asked me to integrate my text.
17:05:37 <Diziet> I haven't done that.
17:05:54 <Diziet> It needs what amounts to a code a review from someone.
17:05:55 <dondelelcaro> #action Diziet to integrate text as asked by Matthew V
17:06:04 <dondelelcaro> ah, OK
17:06:32 <Diziet> Matthew foolishly seemed to volunteer to do that.
17:06:41 <Diziet> I will produce a version with the text integrated.
17:06:50 <bdale> thanks
17:07:09 <dondelelcaro> Diziet: thanks; If you want, we can probably review it too
17:07:17 <Diziet> That would probably be helpful.
17:07:24 <Diziet> Probably best done after I have produce alleged new text.
17:07:24 <bdale> if it's in the repo once integrated, we can all look
17:07:29 <Diziet> Willdo.
17:07:31 <dondelelcaro> cool
17:07:31 <vorlon> dondelelcaro: "until Sunday night" - so you're only at DebConf for one day of talks?
17:07:38 <dondelelcaro> vorlon: yeah, unfortunately
17:07:43 <Diziet> #action Diziet to put resolution integrated text into git
17:07:56 <dondelelcaro> vorlon: I think I'm scheduled to stay Sunday night
17:08:17 <dondelelcaro> vorlon: it conflicts with burningman, and I agreed to go to that well before debconf was actually scheduled. :(
17:08:36 <dondelelcaro> #topic #636783 constitution: casting vote
17:08:54 <vorlon> ah :)
17:08:56 <Diziet> It seems people don't want to fix this by passing the hot potato to the DPL.
17:09:07 <bdale> correct
17:09:32 <dondelelcaro> and we wanted to expand to 9; is that still the case?
17:09:34 <Diziet> So are we settled on changing the size of the committee ?  NB that we are still really only talking about a situation where we have N-vs-N splits (or a three-way M-M-M).
17:09:43 <Diziet> I think that is the best answer.
17:10:00 <bdale> I think changing the size to an odd number and trying to keep the ctte chairs "full" so that's a meaningful change is a good idea
17:10:03 <bdale> makes the simple things simple
17:10:14 * keithp wants to know if we get black robes
17:10:16 <Diziet> Does anyone object ?
17:10:27 <aba> well, I'm not sure that's really a good idea
17:10:34 <Diziet> aba: OK what would you propose ?
17:10:39 <aba> because increasing the number ... has other side effects
17:10:41 <bdale> aba: the size change, or the black robes?
17:10:46 <vorlon> keithp: yes, with fuzzy slipper
17:10:46 <aba> bdale: the robes are ok
17:10:49 <vorlon> s
17:11:11 <Diziet> So long as it doesn't involve any pointy hats.
17:11:13 <aba> basically this is a ugly hack, and when someone steps back it falls on our feet
17:11:26 <aba> also IIRC how long we took last time to select someone ...
17:11:36 <bdale> that was mostly my fault
17:11:37 <Diziet> aba: I think we just want to make it not happen in the worst most important cases.
17:11:41 <bdale> I got .. "distracted"
17:11:50 <aba> if you are all convinced it's a good think I won't stand in the way, but I'm not happy with this proposal
17:12:05 <Diziet> aba: I'm certainly happy to hear an alternative.
17:12:10 <aba> bdale: I don't think that explains all the time, but well.
17:12:20 <Diziet> In the most recent case we carefully went to some lengths to make sure the ctte was full and everyone voted.
17:12:20 <dondelelcaro> aba: would it be enough if the DPL (or someone else) had the casting vote on top of increasing the size?
17:12:51 <aba> dondelelcaro: well, this are seperate issues for me. I think transfering the casting vote to the DPL would be a good thing.
17:13:10 <Diziet> aba: Personally I agree.
17:13:29 <aba> increasing the size to 9, well - I'm not sure it helps the ctte, but we can still do it.
17:13:47 <bdale> aba: I think it helps, and I don't see how it hurts.  what am I missing?
17:13:49 <aba> perhaps it's better for getting it done to do both, and people expect we won't need the casting vote anymore
17:13:54 <Diziet> Would we be willing to put forward two alternatives and have the project decide ?
17:14:11 <vorlon> aba: we do not have a consensus that the DPL should have a casting vote
17:14:15 <vorlon> I am strongly opposed to this
17:14:15 <Diziet> Or put it up as two separate/related GRs ?
17:14:22 <vorlon> the DPL is a political role, not a technical one
17:14:23 <bdale> and the current DPL has said he doesn't want it
17:14:26 <aba> let me ask a different question:
17:14:34 <aba> do we agree we need a casting vote?
17:14:41 <Diziet> vorlon: The most difficult decisions we have had have had strong political components.
17:15:12 <dondelelcaro> does anyone object to having a casting vote at all?
17:15:14 <vorlon> Diziet: makes no difference to me
17:15:23 <aba> if so, what are our options? keep status quo, move it to DPL. what else?
17:15:32 <Diziet> aba: We need some way to make a decision if the TC is deadlocked.
17:15:47 <aba> Diziet: I agree to that, but I'd be interessted if someone here disagrees
17:15:56 <vorlon> Diziet: the DPL is not selected for technical decision-making, and giving the DPL the casting vote distorts both roles
17:15:59 <bdale> nobody has put forth a truly satisfying solution that allows the TC without external participants to decide a question when we're split evenly in a way that we all agree is fair
17:16:04 <Diziet> While the init system thing has had what I think is a bad outcome and has been very bad for the TC, it has at least relieved most of the project of the endless flamewars.
17:16:15 <bdale> that's why I like changing the size to an odd number, it's a hack, but I think it's a useful one
17:16:31 <bdale> it takes the probability of needing the casting vote from slim to very very very slim
17:16:32 <keithp> bdale: it's the usual hack though
17:16:34 <Diziet> We could change the size to an odd number and remove the casting vote.
17:16:35 <Maulkin> (You could have a third party who's !dpl...)
17:16:49 <aba> Maulkin: who? ftp-master? *run*
17:16:51 <Diziet> Maulkin: Brilliant idea.  The pope ? :-)
17:17:05 <dondelelcaro> Diziet: which pope? ;-)
17:17:06 <vorlon> we could eliminate casting votes entirely and automatically refer to GR
17:17:24 <bdale> that's a thought
17:17:28 <Diziet> vorlon: I think that would have been tolerable in the recent case (perhaps even better).
17:17:38 <dondelelcaro> vorlon: that's an interesting idea
17:17:39 <aba> vorlon: doesn't sound too bad
17:17:47 <Maulkin> Well, a former ctte chair, or someone who could look at the things separately. Or you could simply not make a de... what vorlon said.
17:17:54 <Maulkin> Sorry, I'll keep quiet now :)
17:17:55 <vorlon> we had already discussed how, in the recent case, we thought this might get reviewed by the project via GR
17:18:01 <bdale> it wasn't clear the project actually wanted the responsibility or to have a GR, though
17:18:14 <vorlon> that hasn't happened, not because everyone was happy with the decision but because people were too tired of it
17:18:17 <bdale> it again, to me at least, feels like "passing the buck" on something we're supposed to be responsible for
17:18:54 <Diziet> Having this very disputed decision go to the body with the greatest legitimacy would perhaps have improved the feelings about it.
17:19:01 <bdale> I'd really like the TC to be able to render a decision without having to "punt", which I guess means I'm still somewhat nominally in favor of having the casting vote exist.
17:19:06 <aba> actually, I think I'd suggest the project different possibilites how to deal with that case.
17:19:15 <vorlon> put differently, if ever there was a decision by the TC that should have been subject to review via GR, it would have been this one; so why didn't that happen?
17:19:18 <bdale> but having had to use it, I can tell you that having to do so was a *terrible* thing that I don't want to ever repeat
17:19:55 <bdale> vorlon: the conversations I've had since make it seem to me that people really wanted "us" to make a decision more than they all wanted to get involved
17:20:04 <Diziet> vorlon: Because it took too long to get to the point of actually trying to vote on it rather than browbeat people, with the result that everyone was fed up to the back teeth of it.
17:20:18 <aba> bdale: some even would have prefered us to flip a coin to a gr
17:20:29 <bdale> I've heard that too, aba
17:20:34 <vorlon> bdale: the aftershocks on the mailing list showed that this was not a unanimous position
17:20:58 <bdale> nothing like this is going to be unanimous
17:21:13 <bdale> but the inputs I have received have been about 50:1 positive that we were able to reach a decision
17:21:26 <vorlon> yes - but the people thanking you for it are going to be self-selecting
17:21:36 <Diziet> bdale: You're going to be hearing from a selective sample given your role.
17:21:44 <bdale> yes, always
17:21:47 <aba> Diziet: as always
17:21:50 <vorlon> biased towards those that are happy to have not had to have a GR, rather than those who are upset about the outcome :)
17:21:52 <bdale> I don't argue that
17:22:07 <bdale> but the fact that the project did *not* want a GR speaks volumes to me
17:22:30 <aba> vorlon: how much do you disagree to the DPL having the casting vote? Do you just not like it, or would it be worse?
17:23:10 <vorlon> aba: "I am strongly opposed to this"
17:23:21 <vorlon> aba: I consider it a total non-starter
17:23:23 <weasel> (why would the DPL get a vote?  what does he have to do with the tech-ctte?)
17:23:28 <Diziet> Can I propose a fudge: increase size to 9, remove casting vote.
17:23:37 <Diziet> If we really want to avoid a GR we can fill a vacant spot.
17:23:57 <bdale> if we remove the casting vote and condorcet ever leads us to a "tie" again, are we just hung then?
17:24:02 <aba> Diziet: I'd prefer to have a default exit. If that is a GR, that'd be ok with me
17:24:05 <keithp> Diziet: 3-3-3 splits (or even 1-1-1-1-1-1-1-1-1) are still possible, of course
17:24:18 <Diziet> keithp: Yes, but I think vanishingly unlikely in practice.
17:24:31 <dondelelcaro> should we just get these options written up in e-mail?
17:24:32 <Diziet> aba: There is always the option of a GR.
17:24:43 <bdale> so my take is that we should make them vanishingly unlikely and then keep the casting vote so that we can always have an outcome
17:24:51 <aba> but it's possible one of us is just hidden in work / vacation / whatever, and we are back at 8
17:25:03 <vorlon> "review by the full project" is also an outcome
17:25:06 <bdale> the idea that the TC reaches an outcome and the project can over-rule with a GR seems like a good combo to me .. always has
17:25:09 <aba> vorlon: of course.
17:25:13 <keithp> aba: in that case, 'further discussion' would be fine
17:25:29 <Diziet> aba: Then either (a) the question isn't very important in which case it doesn't matter much and we vote again until they turn up or (b) it is important in which case we throw them off for not contributing.
17:25:43 <bdale> yeah, I'd rather see failure lead to FD than straight to GR, I think, but I haven't thought this through
17:25:50 <aba> ok, back to the beginning: Though I still don't like the 9-people-hack, I think we should go on with that.
17:26:21 <Diziet> OK so we have rough consensus on 9.
17:26:22 <aba> (probably I'm way less unhappy with that than vorlon with moving the casting vote)
17:26:33 <bdale> aba: I think so, yes
17:26:34 <Diziet> We have rough consensus against DPL casting vote.
17:26:43 <Diziet> Remaining question is with 9 do we remove casting vote or keep it ?
17:26:54 <Diziet> (We have rough consenus on no automatic GR fallback, it seems clear.)
17:27:01 <bdale> I think we should keep it, but work very hard to never use it again
17:27:04 <Diziet> I would prefer remove it.
17:27:04 <aba> I'd like to keep it then
17:27:28 <vorlon> do we keep or remove the casting vote> let me flip a coin to decide ;P
17:27:28 <dondelelcaro> should we just have two options for this part of the GR?
17:27:35 <vorlon> dondelelcaro: yes
17:27:46 <bdale> that's an easy solution
17:27:55 <Diziet> I thought you didn't like ballots with lots of options ? :-)
17:28:01 <keithp> Diziet: two ballots
17:28:02 <bdale> I don't
17:28:14 <bdale> right now, there seem to be two options and FD
17:28:17 <Diziet> It needs to be one ballot with three options since the questions are related.
17:28:42 <bdale> three?
17:28:50 <Diziet> Or is it just that you don't want more options than voters in which case we can have a single GR with each possible answer to each of the 6 constitutional questions :-)
17:28:57 <bdale> change to 9 with casting, change to 9 without casting, FD
17:29:10 <Diziet> I can live with that.
17:29:26 <Diziet> #action Diziet to draft casting vote to 3 options: change to 9 with casting, change to 9 without casting, FD
17:29:28 <keithp> That seems good to me
17:29:43 <dondelelcaro> #topic #636783 constitution: TC member retirement/rollover
17:29:54 <Diziet> I still think we should have the constitution only as a backstop.
17:30:04 <Diziet> We should expect the next-to-retire member to organise their own replacement.
17:30:19 <Diziet> If we do it that way we avoid having to specify exact rollover periods, procedural complications etc.
17:30:22 <dondelelcaro> I think we were still working out the details of this  and the TC chair retirement on -project
17:30:37 <Diziet> Yes.
17:30:50 <Diziet> I think more contributions from other TC members would be useful in that discussion.
17:31:10 <aba> Diziet: if we want to get this rolling, we should make at some point a formal decision that we want it. It doesn't need to be in the constitution though
17:31:28 <Diziet> aba: Well, another option would be a TC resolution setting out what we expect.
17:31:36 <Diziet> We could throw off latestayers ourselves.
17:31:52 <aba> Diziet: "formal decision" meant a tc resolution
17:31:55 <Diziet> Yes.
17:32:06 <Diziet> If that would suffice then we don't need to put it to GR and no-one will actually put it to a GR if we stick to it.
17:32:28 <Diziet> Do we think that's best ?  If so then I can draft a suitable resolution, but we need to agree on rough rotation rate.
17:32:47 <dondelelcaro> I don't think we need a GR myself
17:33:04 <bdale> what's current consensus?
17:33:06 <vorlon> Diziet: I don't think I have anything more to contribute to that discussion, I just think the policy for rotating members off the TC should be decided before we start dying of old age ;-)
17:33:11 <Diziet> I'm happy to do this inside the TC.  It's more flexible that way and if the TC doesn't do it there's the GR as a possibility.
17:33:15 <Diziet> vorlon: OK.
17:33:34 <bdale> Diziet: I'd rather the TC take care of this internally
17:33:39 <Diziet> bdale: On schedule ?  Somewhere between ~1 and ~2 new members per year (measuring it that way).
17:33:46 <bdale> ok
17:33:53 <Diziet> #agreed TC to set out formally own policy on retirement in own resolution
17:34:39 <dondelelcaro> Diziet: are you OK drafting the resolution?
17:34:39 <Diziet> #action Diziet to draft a proposal
17:34:42 <dondelelcaro> awesome
17:34:46 <dondelelcaro> #topic #636783 constitution: TC chair retirement/rollover
17:34:48 <Diziet> You'll have to actually reply about rate.
17:34:53 <bdale> if we go to 9 members, we can add one soonish, which gives us ample time to work on this before we need to retire someone
17:35:41 <Diziet> chair> I think at the very least we need to fix the bug that the TC can't replace the chair.
17:36:06 <bdale> a vote of confidence provision, or something like that?
17:36:13 <Diziet> If we have some regular replacement of members then having the chair reelected after each new TC member joins would solve the rotation problem.
17:36:17 <keithp> How about annual chair election?
17:36:30 <bdale> Diziet: right, I liked that idea
17:36:32 <Diziet> keithp: That's necessary I think (one way or another) but not sufficient.
17:36:37 <bdale> re-elect on each change of ctte membership
17:36:59 <Diziet> bdale: Yes, a no confidence.  I would suggest that any TC member could start a ballot on the chairmanship at any time and it would work the same way: people just rank people.
17:37:10 <vorlon> I wonder why it should be elected at all rather than a rotation
17:37:18 <keithp> vorlon: because of the casting vote
17:37:22 <bdale> vorlon: because we tried a rotation and it failed
17:37:42 <bdale> I kind of got left sitting in the chair
17:37:47 <bdale> I've never been "elected" to it
17:37:47 <Diziet> Also in practice right now I would like Don to be chairman officially because he's actually doing all the chairman stuff.
17:38:19 <vorlon> bdale: I didn't think it was failing at all; the TC chair's formal duties in the constitution didn't relate to what we were trying to do with the role at the time
17:38:19 <aba> well, and also I think we should allow the ctte to replace the chair (same as removing a ctte member)
17:38:34 <vorlon> bdale: "got left sitting in the chair" - I think you mean you neglected to tag out? :)
17:38:37 <Diziet> Does anyone object to TC election automatic trigger on new TC member, and manual trigger by another TC member ?
17:38:55 <keithp> Diziet: I like the automatic election trigger
17:38:56 <Diziet> I wouldn't have automatic trigger on TC member leaving 'cos that's a bit uncontrolled and people might forget there's an automatic election...
17:38:58 <vorlon> keithp: there are other functions of the TC chair besides the casting vote, but regardless I don't see why it's better for the TC to elect someone to this role than to rotate it
17:39:47 <bdale> Diziet's point about Don leads me to observe that there's a difference between being chair with the things the constitution gives the chair as extra responsibilities, and being the ctte's "admin"
17:40:10 <dondelelcaro> right; I think what we originally tried to do with rotation, was give each member an issue to be in charge of
17:40:12 <aba> I'm not sure we need the auto-reelect in the consititution (we could just agree on it), but we need the "we could decide to replace the chair" in the constitution
17:40:15 <dondelelcaro> and that wasn't very successful
17:40:18 <bdale> I did not particularly enjoy being the project secretary after Manoj's departure from that role in the middle of a messy GR, for example
17:40:44 <aba> bdale: I think you tried a few things not too enjoyable
17:41:03 * bdale shrugs
17:41:38 <Diziet> bdale: admin> I think that the chairman's extra powers ought to be exercised by someone who has a sense of the proper organisation and management of the ctte, so the two roles ought to coincide.
17:41:41 <bdale> I don't think of myself as a masochist, but I fear my history of involvement in Debian might lead some to disagree.
17:41:41 <aba> Diziet: I think nobody disagreed with your question yet
17:41:47 <Diziet> That's normal in other committees.
17:41:51 <Diziet> aba: Good :-).
17:42:07 <Diziet> #action Diziet to draft TC election automatic trigger on new TC member, and manual trigger by another TC member ?
17:42:13 <aba> Diziet: being masochist? yes, I fear that
17:42:14 <Diziet> ... only without the ?
17:42:23 <Diziet> aba: *snort*
17:42:44 <dondelelcaro> #topic #681419 Depends: foo | foo-nonfree
17:42:45 <Diziet> vorlon: If you want some other approach, perhaps you could write it up in an email ?
17:42:45 <bdale> s/TC election/TC chair election/
17:42:46 <keithp> Diziet: manual trigger might want a second?
17:42:52 <Diziet> bdale: Yes, that's what I meant.
17:43:11 <Diziet> keithp: I have no object to that.  If I forget it, remind me.
17:43:16 <keithp> will do
17:43:16 <aba> I'd consider it a normal resolution
17:43:19 <Diziet> s/object/&ion
17:43:20 <Diziet> aba: Right.
17:43:24 <dondelelcaro> I think vorlon was going to CFV for this one; IIRC, that hasn't happened yet
17:43:25 <vorlon> Diziet: other approach to chair selection?  I guess so
17:43:31 <Diziet> vorlon: Yes.
17:43:35 <dondelelcaro> (sorry; jumping ahead)
17:43:37 <vorlon> dondelelcaro: right, I remembered roughly an hour ago that I had the action on thiso ne
17:43:40 <Diziet> But having a second is helpful because otherwise you can get ambushed by sudden quorum.
17:43:44 <aba> so we have a minimum of two for decision, but no minumum for proposal
17:43:54 <Diziet> aba: Let's take this to email.
17:43:57 <aba> Diziet: sure
17:44:13 <Diziet> #action Diziet to report on quorum vs seconding for TC chair election
17:44:25 <Diziet> So on to the actual #topic:
17:44:50 <Diziet> All has been said.
17:44:52 <Diziet> Surely.
17:44:59 <dondelelcaro> #ACTION: vorlon To CFV on cjwatson_draft.txt, A vs B vs FD
17:45:20 <dondelelcaro> #topic #741573 menu systems and mime-support
17:45:43 <Diziet> I don't think this discussion is going anywhere right now.  Is it finished ?
17:45:49 <Diziet> I am ready to vote.
17:46:09 <Diziet> I'm not sure if the `fdo menus only' option draft text is complete yet.
17:46:29 <vorlon> I'm way behind on reading that thread, so I'm not ready to vote
17:46:45 <vorlon> though this shouldn't necessarily wait on me
17:46:50 <Diziet> Right.
17:47:14 <aba> I also think looking at the discussion we could vote, but I need to spend a bit of time before voting
17:47:17 <keithp> Diziet: I wrote that as a strawman to see what objections people had; I'm reasonably convinced by the result that the best solution is to let packages pick what they want to support
17:47:35 <Diziet> keithp: So does that mean you're happy with my draft after all ?
17:47:38 <vorlon> "what they want to support" meaning two systems?
17:48:20 <keithp> vorlon: allowing packages to not have to support the debian menu package
17:48:24 <keithp> that's a good start
17:48:27 <Diziet> Note that "let packages pick" isn't quite the same as my draft which says "the policy for each system should say which packages it would be good to have a menu item for" and implicitly maintainers are expected to accept patches for whichever is applicable.
17:48:46 <keithp> right, which isn't quite what I want
17:48:55 <Diziet> keithp: Do you mean that maintainers should be allowed to reject otherwise-reasonable patches which add a trad Debian menu entry ?
17:48:59 <keithp> Yes
17:49:22 <aba> why? are the maintenance costs too high?
17:49:27 <Diziet> Do you intend to write that up ?
17:49:33 <Diziet> I mean as a draft decision.
17:49:45 <Diziet> You could probably take my most recent draft text as a starting point.
17:49:46 <aba> (I wouldn't expect maintainers to create such patches, but I would expect them to apply patches)
17:49:47 <keithp> Diziet: yes, I will take my current proposal and replace it
17:49:52 <vorlon> well, it sounds like there are still no proposals on the table that I don't consider terrible, then
17:49:57 <Diziet> My proposal isn't in git right now.
17:50:17 <aba> vorlon: I'm not convinced there is a non-terrible proposal possible
17:50:20 <bdale> Diziet: be good to get it there, so we have all the text where we can see it at ocne
17:50:22 <bdale> once
17:50:46 <vorlon> aba: the end goal here should be to have a single menu system in Debian that all packages can integrate with and does the right thing in each context
17:50:55 <vorlon> and I think that fdo makes a better basis for this
17:50:58 <keithp> vorlon: I'm thinking that packages should be allowed to ship fdo .desktop as the only menu support
17:51:02 <bdale> vorlon: I agree
17:51:20 <vorlon> keithp: yes, packages do that already today and it's busted
17:51:30 <aba> keithp: I agree with that statement. However, if there's a patch available why not apply it?
17:51:38 <keithp> vorlon: the problem is that we have no migration path for people using the current debian menu stuff without the menu package adding support for .desktop files, and I don't think it's reasonable to require that package to change
17:51:44 <KGB-3> 03Ian Jackson 05master 4e20cab 06debian-ctte 03741573_menu_systems/iwj_draft.txt #741573: My draft from 22nd May
17:51:51 <vorlon> keithp: why is that unreasonable?
17:51:52 <aba> (same as with languages - packages are ok to ship only with C, but need to add l10n if patches are there)
17:51:54 <bdale> Diziet: thanks
17:52:04 <dondelelcaro> someone should really just write the patches to get the menu package to support desktop files too
17:52:11 <keithp> vorlon: a general reluctance to require people to do work
17:52:14 * dondelelcaro wonders if he's just volunteering himself again
17:52:23 <aba> .oO(probably)
17:52:25 <bdale> dondelelcaro: sure sounded like it, but I won't hold you to it
17:52:40 <keithp> dondelelcaro: that would be awesome
17:52:42 <vorlon> keithp: we should be able to say what the correct technical solution is even if there's not currently someone stepping up to implement it
17:52:45 <Diziet> dondelelcaro: What will you do about transferring the last n years' curation effort (put into the trad entries) into the fdo desktop files ?
17:53:13 <vorlon> but right now we're all over the map on what people think is the "solution", which is an obstacle to people stepping up
17:53:14 <bdale> vorlon: I agree on being able to articulate the correct tech solution, but mandating it without a known path to the work getting done is harder for me
17:53:14 <keithp> vorlon: right, that's what I was going to write up; softening my current proposal to allow packages to ship menu files if they want to
17:53:45 <dondelelcaro> Diziet: don't know yet, but mapping of curation is certainly possible.
17:53:47 <keithp> Diziet: allow a debian-specific entry that specifies the menu path
17:53:58 <dondelelcaro> Diziet: but that's kind of not on topic here
17:54:17 <dondelelcaro> whoever actually does the work needs to figure that out, and until the work exists, we shouldn't assume that it will
17:54:27 <keithp> dondelelcaro: there's no obvious mapping; I think we'd need debian-specific information in the .desktop file
17:54:40 <vorlon> which is supportable
17:54:43 <Diziet> vorlon: How about a resolution which at the current time has the same effect as mine but which laments the lack of integration and says that we would like there to be integration and when it exists people should use it ?
17:54:47 <keithp> dondelelcaro: right. We have a strawman .desktop to menu conversion program and it doesn't work at all well
17:55:12 <Diziet> dondelelcaro: Well it is if you are planning to obsolete all the existing entries.  It's not a SMOP; there's a transition plan to be done.
17:55:45 <keithp> Diziet: the transition would be to have menu support .desktop and then have packages migrate to .desktop
17:55:45 <vorlon> Diziet: "same effect as yours" - yours which? I only see a draft from keithp in git
17:55:53 <Diziet> vorlon: pull
17:56:08 <vorlon> ah
17:56:10 <Diziet> Mine that I sent on the 22nd of May and you should have read then :-).
17:56:18 <vorlon> yes, I already said I was behind on that thread
17:56:20 <Diziet> But I just committed it to git too.
17:56:49 <dondelelcaro> OK
17:56:52 <vorlon> Diziet: yeah, I disagree categorically with that
17:56:55 <bdale> Diziet: I read it, but I like it less now than I did then.  The more time goes on, the more I think resolving the challenges to letting the fdo format be our default makes sense
17:57:04 <Diziet> (Can I make a procedural observation ?  I think being 2 months `behind' on a thread really means you're just not reading it and aren't participating in that decision.  That's fine of course.)
17:57:22 <keithp> bdale: agreed, but how to make progress without fixing the menu package?
17:57:31 <bdale> fix the menu package
17:57:34 <vorlon> anything short of the TC setting a roadmap for getting us back to a single menu system is, to me, a failure
17:58:07 <dondelelcaro> do we have anything else to discuss here?
17:58:13 <Diziet> vorlon: You also have to define what happens at each stage of the roadmap.  In particular, what happens right now when the work of combining the systems isn't done.
17:58:30 <Diziet> Bearing in mind that transitions of this kind usually take years.
17:58:48 <keithp> Diziet: the transition is effectively done for most of us already
17:59:03 <Diziet> I looked in the archive and lots of software ships menu files.
17:59:09 <vorlon> Diziet: yes; but this resolution is certainly not a roadmap, it's a one-off change to policy which resolves nothing
17:59:10 <Diziet> See my stalwart example of `bc'.
17:59:14 <keithp> But few people actually use them
17:59:53 <Diziet> vorlon: How about a resolution which says `this is what is happening for now but this is unsatsifactory and we would like to see a roadmap which meets both sets of use cases with a single set of technology' ?
18:00:08 <Diziet> keithp: We don't know how many people use them.
18:00:19 <vorlon> Diziet: and who's going to provide this roadmap?
18:00:22 <Diziet> Matthew said he uses them to discover functionality in newly-installed software.
18:00:23 <keithp> Diziet: gnome, kde and xfce do not use menu files
18:00:24 <bdale> I'm pretty sure I've never used bc outside of a script someone else wrote
18:00:29 <Diziet> vorlon: Whoever wants to provide a roadmap!
18:00:32 <vorlon> we've already had interminable discussions about this on debian-policy and elsewhere
18:00:53 <Diziet> The proposed policy changes were deficient because they didn't recognise the goals of the trad menu system.
18:01:06 <Diziet> bdale: And everyone is just like you.  I get it.
18:01:18 <keithp> vorlon: do you want to work with me to fix my proposal?
18:01:19 <bdale> no, it's pretty clear that's not true
18:01:24 <bdale> that's why both of us are here, etc etc
18:02:04 <vorlon> keithp: want to, yes; have time to before September, no
18:02:13 <Diziet> My point is that "I don't use this" is no argument at all, whereas "Alice uses this" is quite compelling in an area where we have no reliable measurements given that we as the TC usually get practically no user feedback.
18:02:42 <Diziet> I don't use the trad menu either.  The difference between us is that I don't want to throw things away because I don't have the other guy's use case.
18:03:35 <Diziet> keithp: Maybe we should just ditch the non-desktoppy wms then.
18:04:00 <dondelelcaro> lets move on if possible
18:04:02 <dondelelcaro> #topic #746715 init system fallout
18:04:13 <keithp> Diziet: I've always executed commands from the command line; don't see why anyone would want menus...
18:04:15 <Diziet> We had a resolution about this but it wasn't posted to dda and put on the website ?
18:04:16 <dondelelcaro> #action dondelelcaro to confirm and post to website
18:04:23 <dondelelcaro> right
18:04:23 <Diziet> OK, good.
18:04:25 <bdale> Diziet: I'm not asking to throw anything away.  But I do find it unsettling to contemplate requiring other people to do work to support something I don't use, and I can't figure out how to objectively measure how much anyone else uses
18:04:28 <dondelelcaro> #topic #750135 Maintainer of aptitude package
18:04:48 <Diziet> I was going to write up a process document and didn't.
18:04:51 <dondelelcaro> OK
18:04:58 <Diziet> I've taken on too much stuff and would like someone else to handle this.
18:05:13 <Diziet> (sorry)
18:05:14 <dondelelcaro> sounds reasonable; can someone volunteer?
18:05:28 <bdale> on 730135?
18:05:40 <dondelelcaro> on #750135, yeah.
18:05:58 <aba> Diziet: any idea you could share with us?
18:06:19 <dondelelcaro> aba: the previous log and the summary has some of it
18:06:24 <Diziet> Right.
18:06:43 <dondelelcaro> Diziet should propose some kind of process which we will refine and/or try out on #750135 and revise/drop/use for #752400. Including the maintainer perhaps providing private response to us, and also perhaps an irc conversation.  (Diziet, 18:02:52)
18:06:49 <Diziet> Briefly: we should invite private and public feedback; we should invite present maintainer to talk to us privately.
18:07:00 <aba> yep, the usuals
18:07:31 <aba> I cannot promise to get it done, but I could at least try
18:07:37 <Diziet> Thanks.
18:08:02 <dondelelcaro> cool
18:08:30 <dondelelcaro> #action aba to propose a process to try out on #750135 Including the maintainer perhaps providing private response to us, and also perhaps an irc conversation.
18:08:37 <dondelelcaro> #topic Additional Business
18:09:04 <bdale> I'll mention again here that we have a session scheduled at Debconf, I hope everyone on the ctte attending will be there
18:09:15 <Diziet> I should be but I haven't checked the draft schedule yet.
18:09:30 <bdale> either Mon or Tue at 10am, I forget which is tech ctte and which is SPI BOF
18:09:45 <aba> bdale: I'm not going, sorry.
18:10:21 <bdale> I have nothing else today
18:10:22 <dondelelcaro> cool; anything else?
18:10:54 <bdale> dondelelcaro: thanks again for running this meeting series, they're helpful
18:11:03 <keithp> see several of you in a couple of weeks in Portland
18:11:18 <Diziet> TTFN all.
18:11:22 <dondelelcaro> no problem!
18:11:25 <dondelelcaro> #endmeeting