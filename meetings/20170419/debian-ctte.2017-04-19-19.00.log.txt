19:00:32 <bremner> #startmeeting
19:00:32 <MeetBot> Meeting started Wed Apr 19 19:00:32 2017 UTC.  The chair is bremner. Information about MeetBot at http://wiki.debian.org/MeetBot.
19:00:32 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
19:00:37 <OdyX> bremner: there, thank you.
19:00:42 <OdyX> Agenda is https://anonscm.debian.org/cgit/collab-maint/debian-ctte.git/plain/meetings/agenda.txt
19:01:03 <OdyX> Let's start with the round of names, to see who's here.
19:01:06 <OdyX> Didier Raboud
19:01:12 <marga> Margarita Manterola
19:01:13 <hartmans> Sam Hartman
19:01:18 <keithp> Keith Packard
19:01:21 <bremner> David Bremner
19:02:01 <OdyX> Mithrandir, fil ?
19:02:56 <Mithrandir> Tollef Fog Heen
19:03:09 <OdyX> #topic Next Meetings?
19:03:19 <nthykier> NB: OdyX you are not a chair
19:03:25 <marga> I think bremner needs to make you chair
19:03:29 <bremner> oh crap.
19:03:32 <OdyX> That's ironic. :-P
19:03:37 <bremner> #chair OdyX
19:03:37 <MeetBot> Current chairs: OdyX bremner
19:03:39 <OdyX> #topic Next Meetings?
19:03:58 <OdyX> #info We decided on Third Wednesday in a month, 19 UTC
19:04:31 <OdyX> bremner: you're the most concerned, as newcomer; is that something you can generally work with?
19:04:36 <bremner> yes
19:04:37 <hartmans> bremner: was mumbling about that being problematicish this month.
19:04:42 <hartmans> Will that work going forward?
19:04:49 <bremner> going forward it's fine
19:04:55 <bremner> atm I'm supervising an exam
19:05:00 <bremner> "supervising"
19:05:09 <OdyX> Good. Let's move on then
19:05:17 <OdyX> #topic Welcome David Bremner!
19:05:31 <OdyX> I actually entirely forgot that, but, congrats bremner, and welcome!
19:05:39 <bremner> o/
19:05:47 <marga> \o/
19:06:10 <OdyX> #topic Review of previous meetings' TODOs
19:06:27 <OdyX> Mithrandir had 857257 to close, afaik done.
19:06:28 <hartmans> aaaargh.  Did I still forget to close the stupid javascript bug?
19:06:42 <OdyX> marga had 846002 to close; where does that stand?
19:06:53 <marga> I didn't do it :-/
19:07:02 <marga> I didn't do any one of the things I said I would :-/
19:07:17 <OdyX> don't worry; let's focus on solution-finding, rather than fingerpointing.
19:07:36 <OdyX> marga: is it something you want to get done, or would rather pass on to someone else?
19:07:54 <marga> I don't have any particular interest in doing it myself
19:08:00 <marga> It just needs to get done by someone
19:08:12 <marga> I volunteered because I thought I would have time, but it turned out I didn't
19:08:49 <OdyX> Ack. Let's discuss it at that agenda item then.
19:09:01 <OdyX> fil pushed the new member process, thanks to him.
19:09:15 <OdyX> And I didn't move a single iota on the systemd/policy/etc things, sorry for that.
19:09:22 <OdyX> #topic #860520 Voting for TC Chair
19:09:45 <OdyX> That's merely on the agenda because it's an open bug.
19:10:00 <OdyX> #action everyone to vote on #860520 (TC Chair).
19:10:07 * marga nods
19:10:42 <OdyX> (besides the funky hat, it's actually a quite administrative job; and I'm pondering to invest some time to automate more of that job)
19:10:46 <OdyX> Let's address that in the varia.
19:10:58 <OdyX> #topic #850887 Decide proper solution for binutils' mips* bug
19:11:02 <Mithrandir> who, if any, want to be chair? Electing somebody who doesn't have time to shoulder the burden is something we should avoid.
19:11:23 <keithp> pretty clear I don't have the bandwidth at this point
19:11:42 <OdyX> #topic #860520 Voting for TC Chair
19:11:58 <OdyX> Mithrandir: I'm fine with the workload, currently.
19:12:05 <marga> I wouldn't mind if I had the time, which I don't.
19:12:09 <bremner> since we're saying who not, I'd rather not.
19:12:40 <OdyX> It's just a quite boring job (update the agenda manually, send meeting reminders [forgot this time], update the ical by hand, etc.
19:12:47 <OdyX> so it's not motivating.
19:12:55 <OdyX> And it's mostly about doing things all of us could do.
19:13:38 <OdyX> But as mentionned, I'm willing to invest slightly in tooling to ease that burden (such as auto-generate an agenda somewhere, send automated mails, etc.
19:13:48 <Mithrandir> nod. I'm myself at -0-ish.  I don't really have the bandwidth, but I'm hardly alone in that.
19:13:56 <bremner> could the "organizing the next meeting" part rotate?
19:14:18 <hartmans> bremner:  You could, although the impact if someone drops the ball is significant.
19:14:23 <OdyX> Barring a more motivated candidate, I guess I'm the least horrible placeholder.
19:14:37 <bremner> hartmans: ack.
19:14:56 <OdyX> (not that I think any of you would do a horrible job, sorry if my english went through badly).
19:15:02 <hartmans> But a chair could certainly delegate almost all of the job (except for the casting vote)
19:15:21 <OdyX> Actually, the casting vote is the only thing in the constitution about what the chair should do.
19:15:49 <OdyX> The rest is administrativia that falls in the hands of the chair as default go-to person I suspect.
19:16:03 <bremner> is there someone who'd be more willing to be secretary than chair?
19:16:37 <keithp> yeah, don used to act as secretary, but not chair
19:16:45 <OdyX> That's an interesting perspective.
19:17:06 <bremner> I'd be willing to consider next time the question came up
19:17:31 <OdyX> I see the chair position as rather honorary (and the poor soul that needs to break ties when absolutely needed), so it's a nice addon for some secretary work.
19:18:31 <OdyX> Let's move on, and maybe move that (important) discussion to list.
19:18:59 <OdyX> Now to the three other topics:
19:19:02 <OdyX> #topic #850887 Decide proper solution for binutils' mips* bug
19:19:15 <hartmans> I'm busy closing it now
19:19:19 <OdyX> Good, thank you.
19:19:28 <OdyX> #topic #846002 blends-tasks must not be priority:important
19:19:42 <OdyX> This one also died by expiration, and needs closing.
19:20:17 <marga> Died by expiration?
19:20:20 <marga> I thought we voted
19:20:27 <OdyX> ah yes, indeed, sorry.
19:20:29 <marga> This is the one that I had to close and failed to do.
19:21:06 <marga> I should have time this weekend.
19:21:10 <OdyX> marga: would it help if we drafted an closing message just after the meeting?
19:21:42 <OdyX> I could give a hand this weekend, modulo coordination, if that helped.
19:21:45 <marga> Not really, I haven't had dinner and had a very long day at work. But I have Saturday free for Debian work.
19:22:17 <OdyX> Great, thanks!
19:22:36 <marga> I'll draft the closing message and ask for review before sending.
19:22:50 <OdyX> #action marga to draft closing message to #846002 this week-end
19:22:57 <OdyX> #topic #839172 TC decision regarding #741573 menu policy not reflected yet
19:23:12 <OdyX> This one is basically archived, and could be a bug against Policy.
19:23:36 <OdyX> I have it on my TODO; but it needs serious involvment.
19:23:36 <hartmans> i plan to include the following statement in the mips closure.
19:23:47 <OdyX> I want to try tackle that at DebConf.
19:23:59 <hartmans> I want to make sure no one things I'm so off base that this is problematic even as my individual opinion (and to collect any other immediate comments)
19:24:02 <keithp> OdyX: that seems like a good plan; get wording proposed and push the change then?
19:24:02 <hartmans> As an individual, I note that this is the second significant issue
19:24:02 <hartmans> brought before the TC in the stretch time frame with regard to the
19:24:02 <hartmans> toolchain.
19:24:02 <hartmans> Communication was somewhat challenging on both issues.
19:24:02 <hartmans> The project as a whole might want to consider how we can better balance
19:24:03 <hartmans> these issues and get folks working on the toolchain the resources they
19:24:03 <hartmans> need both to produce an effective toolchain and to communicate with the
19:24:05 <hartmans> rest of the project.
19:24:05 <hartmans> The committee is of course available for a resource if useful in such a
19:24:07 <hartmans> discussion.
19:24:49 <OdyX> hartmans: looks good to me.
19:24:56 <Mithrandir> I have no problems with that
19:25:10 <keithp> yeah, recognizing the meta issue and trying to work to avoid problems in the future. very nice.
19:25:31 <bremner> with the caveat that I wasn't "here", it looks ok
19:25:55 <OdyX> bremner: that will always be a cornercase
19:25:58 <hartmans> ok, unless marga has something in the next minute or so will send
19:26:06 <hartmans> sorry to disrupt policy
19:26:22 <marga> Sorry, looks good to me as well
19:26:50 <OdyX> hartmans: it's hardly disrupted. That point is just a monthly nag to get me started (but it's a scary, huge and complicated subject that needs english-writing skills).
19:26:57 <OdyX> #topic #836127 New TC Members
19:27:08 <OdyX> We still have one seat free, and we're in mid-April.
19:27:20 <OdyX> I entirely agree with hartmans that we're factually slow.
19:27:27 <marga> Yeah, I've been meaning to write back to the candidates with something
19:27:36 <marga> But in order to do that, I need to know with what
19:27:41 <OdyX> And I've already phrased that concern in the past: we're spending _a lot_ of our energy to find new people.
19:27:48 <Mithrandir> what do we want the process for the next candidate to look like?
19:28:32 <OdyX> And do we want to provide transparency to bremner about the other candidates (and use that set of people as the pool for the new seat), or do we want to start afresh?
19:28:37 <hartmans> Mithrandir:  For me to feel better about the process, I'd like it to be less of a popularity/name recognition contest.
19:29:23 <hartmans> That is, I'd like to figure out better criteria on which we could  judge candidate contributions and judge how effective they would be at 1) solving the kind of problems that come before the tc and 2) not dropping the ball.
19:29:39 <bremner> so stop 0, job description?
19:29:44 <hartmans> While asking for a pony  I'd like to run the process fast enough that the candidate is still psyched about joining the TC when that happens.
19:29:44 <bremner> err, step
19:29:48 <OdyX> Also, as a matter of fact, if I got my calculus correct, keithp's term expires at the end of 2017, but mine and Mithrandir only in 2018, which means we only get one new seat at the end of this year.
19:30:13 <keithp> that's a help at least :-)
19:30:23 <OdyX> keithp: thank you for that :-P
19:30:27 <Mithrandir> hartmans: I agree with your concerns. I don't know how we'd go about judging those skills/features up front.
19:31:18 <marga> I'm on the same camp
19:31:25 <OdyX> I'm wondering if the "TC nominates new member" doesn't conflict explictely with our willingness to get more diversity from the project.
19:31:51 <hartmans> Mithrandir:  We must know someone who's gone through studying leadership selection processes to maximie diversity and good results.
19:31:54 <marga> I tried to find whatever data I could from the candidates that would help us find candidates with those qualities, but it's hard.
19:31:54 <OdyX> (but that's a constitutional amendment discussion, maybe for another time)
19:32:04 <hartmans> I will admit that all I've been exposed to is problems no real good solutions.
19:32:28 <keithp> OdyX: we can use whatever process we want to select candidates
19:32:30 <bremner> you ask candidates why they think they'd be good at the job?
19:32:31 <hartmans> marga:  And I think you did better than I imagined possible.
19:32:43 <bremner> s/you/we could/
19:32:48 <OdyX> keithp: good point.
19:32:49 <keithp> yes, the last round was amazing in comparison
19:32:50 <hartmans> I think it's almost impossible with a process this closed to get good data
19:32:55 <Mithrandir> hartmans: pointing out the problems is the first step, so I'm thankful for you doing that, even if neither of us has any ready solutions. :-)
19:33:44 <hartmans> however, I think OdyX asked an important question: do we go forward and fill from the candidates we have or do we cgo through another round.
19:33:59 <hartmans> I'll note that we have more candidates and better data on them now that at any previous time I've been on the TC.
19:34:11 <OdyX> It's quite hard to go more public with candidacies, as we then order and pick people out of a set, for quite personal reasons.
19:34:17 <marga> I think we should pick a second candidate from the list of candidates that we have.
19:34:21 <hartmans> So, I'm somewhat tempted to fill from candidates before us.
19:34:35 <OdyX> That's my thinking too.
19:35:02 <hartmans> OdyX:  The whole space is hard:-)
19:35:30 <OdyX> We would therefore share the private discussion about all non-bremner candidates to bremner, right ?
19:35:31 <keithp> marga: I agree, we have a good list
19:35:38 <keithp> OdyX: right
19:35:48 <marga> Alright, so I'll mail the remaining candidates with an update and say that we are still deciding on the second position
19:35:49 <Mithrandir> it feels a bit like stringing them along, but we could ask them if they're still interested.
19:36:06 <marga> And ask them if they are still interested.
19:36:31 <bremner> as a technical issue, I'm not on the private alias yet.
19:36:31 <OdyX> I think it's important to tell them "with which sauce they're being swallowed" (don't know if that translates very good away from french).
19:36:53 <OdyX> #action OdyX to check with DSA about bremner on the private alias.
19:36:54 <keithp> OdyX: a nice metaphor in any case :-)
19:37:04 <Mithrandir> if somebody files an RT ticket to get bremner added, I can do it.
19:37:12 <Mithrandir> but I'd rather not both file the ticket and solve it
19:37:45 <OdyX> what I mean by that, is that I think we owe them transparency about what is happening (either we close the round with thanks, or we keep them in the decision basket).
19:37:59 <OdyX> #action OdyX to file an RT ticket to get bremner added.
19:38:18 <OdyX> I'm wondering if we don't also owe that transparency to the project.
19:39:05 <marga> Yes, I agree we owe them transparency, and that's what I was saying I'll do.  It seemed the consensus was that we keep them in the basket?
19:39:19 <OdyX> Yes; we're in agreement.
19:39:31 <hartmans> only if it's a cream sauce:-)
19:39:41 <OdyX> do we welcome / call for new nominations ?
19:39:51 <OdyX> and do we announce that on d-d-a ?
19:39:58 <bremner> what's the downside?
19:40:11 <bremner> aside from more boring work for OdyX ;)
19:40:25 <marga> bremner, waiting until all the nominations are in to discuss
19:40:31 <bremner> ack.
19:41:19 <marga> I don't know the answer.  We could welcome new nominations until EOM?
19:41:19 <OdyX> I think (although I dislike the concept) that we're better off with deciding in the current set, especially if we have another seat opening in 8 months.
19:42:47 <Mithrandir> I'd rather we just work with what we have, if we follow a similar timeline as last time we'd be looking at new nominations in 4-5 months
19:42:57 * marga nods
19:43:18 <OdyX> ack. I can draft a mail to d-d-a with that, and marga to the candidates ?
19:43:41 <marga> OdyX, I'm confused, what's the mail to d-d-a going to say?
19:43:53 <bremner> should we have a nomination push at debconf for the next ones?
19:44:02 <OdyX> marga: that we're deciding out of the current set?
19:44:15 <marga> OdyX, doesn't sound worth announcing to me
19:44:18 <OdyX> I'm fine with saying nothing too, or just to debian-ctte@
19:44:24 <marga> It's not like people care that much :)
19:44:42 <marga> Yeah, -ctte would be fine
19:44:46 <bremner> agree
19:44:49 <marga> #action Marga will email all other candidates informing them of the state, asking if they are still interested
19:45:42 <OdyX> #action OdyX will publish the plan to debian-ctte@
19:46:17 <OdyX> #action OdyX to Bounce the non-bremner mails to bremner.
19:47:22 <OdyX> #topic Additional Business
19:47:31 <OdyX> Let's start with DebConf17.
19:47:46 <Mithrandir> I'll be there
19:47:50 <OdyX> I've filed a talk proposal for the TC presentation, but that's a detail.
19:47:55 <OdyX> Who's going to be there?
19:47:56 <Mithrandir> but not for much/any of debcamp
19:48:02 <bremner> I will be there
19:48:03 <OdyX> (It's been immensely useful last year)
19:48:03 <marga> I'm planning on being there, haven't done the visa dance yet.
19:48:11 <hartmans> I think I'll be there.  I have not booked travel yet.
19:48:16 <hartmans> I'm trying to drag my boss along.
19:48:24 <OdyX> Booked travel, need to check for visa.
19:49:03 <keithp> I've got a room booked, will book travel closer to the event
19:49:17 <OdyX> Good. fil is a regular DebConf'er, I expect hir to be there
19:49:30 <OdyX> Now about administrativia
19:49:36 <hartmans> keithp:  Would you mind forwarding me the hotel info?  It'd be nice for me to stay at a hotel with other debian folk
19:49:46 <OdyX> What part of what I used to be doing is really useful ?
19:50:06 <marga> This sounds like a trick question.
19:50:08 <OdyX> We have the agenda.ics, the meetings/agenda.txt and I've manually sent reminder mails with lame copy-pastes.
19:50:32 <hartmans> I found the on-irc reminders fairly useful.  I found the agenda updates useful.  I didn't find the reminder emails that useful.
19:50:34 <OdyX> marga: I'm genuinely interested, and I failed this time because $work and $VAC
19:50:35 <marga> I think all of those are useful.  I think the most useful one is the agenda, as this has allowed me to chair meetings when you were not there.
19:51:43 <OdyX> There also used to be updates to the Debian website: https://www.debian.org/devel/tech-ctte but there was nothing since 2015
19:51:58 <OdyX> and we have the history of past bugs on the git.
19:53:09 <OdyX> Since weasel mentionned it yesterday, the idea has been tinkering in my brain to make use of someting like ikiwiki (as dpl.debian.org or dsa.debian.org).
19:53:32 <OdyX> It would be basically the same content, but at least on a public, web-friendly place.
19:53:52 <Mithrandir> agenda.txt and ics + aroudn-meeting-start reminder are the usual bits around meetings that are useful for me
19:53:56 <marga> I'm not a fan of ikiwiki, but I guess that if someone else is doing the work, I wouldn't mind :)
19:54:44 <hartmans> i'm a huge fan of ikiwiki over debian web cvs and wml:-)
19:55:01 <OdyX> The fact that the website documentation is not up-to-date with regards to our decisions irritates me, and I'd love for us to have something better than pure git.
19:55:24 <OdyX> (and I'm not irritated by anyone in particular, I'm saying it's a bad state of fact)
19:55:27 <marga> Makes sense.  I once tried to use BTS's tags, and they were all mostly wrong.
19:56:37 <OdyX> Okay. What I get from that round of advice is that the agenda.txt is good (but could be auto-generated from the BTS) …
19:57:01 <OdyX> That the agenda.ics is useful (but, as we agree for a regular schedule, could be auto-generated)
19:57:29 <OdyX> that the meeting reminders are also useful (but could also be automated)
19:57:40 <OdyX> Mailing list vs pingall ?
19:58:29 <marga> I like both
19:58:55 <OdyX> I suspect we can only ping the TC members though.
19:58:57 <marga> I'm usually around on IRC, but not when travelling, so the mail reminder is useful for then.
19:59:05 <marga> Yeah, I guess that would make sense.
19:59:16 <OdyX> Okay, I will spend some energy for these things, to reduce the administrative burden somewhat.
19:59:29 <OdyX> Now there's another idea: what about a kanban board?
19:59:45 <OdyX> The "todos from last IRC meeting" are way too easily lost.
19:59:52 <marga> I'd be willing to try it, not sure how accessible it would be for hartmans
20:00:43 <OdyX> On the other hand, that brings me to another varia: our (collective) responsiveness.
20:00:47 <hartmans> I am not familiar with kanban
20:01:10 <OdyX> If we need tooling to remind us of 4-months old TODO items, then maybe the tooling is the wrong answer.
20:01:35 <OdyX> hartmans: it's basically a TODO | DOING | DONE board with items and assignees.
20:01:52 <marga> hartmans, it's a tool for tracking work, who's doing it and it what state is done.  There are several implementations of it, mostly web based.
20:02:01 <hartmans> website? or gui app with client server protocol?
20:02:14 <hartmans> web based are likely to be ok
20:02:22 <bremner> a crude attempt at org-mode in a browser
20:02:27 <hartmans> Speaking of responsiveness: I'm getting dragged into a debugging session at work
20:02:46 <marga> Yeah, we are over the hour
20:02:49 <marga> We should close up
20:03:05 <OdyX> Indeed, sorry.
20:03:22 <OdyX> Let's first try to get these few bugs closed, and a new member nominated.
20:03:27 <OdyX> And get stretch out!
20:03:33 <marga> Indeed :)
20:03:42 <OdyX> #endmeeting