====================
#debian-ctte Meeting
====================


Meeting started by OdyX at 18:00:57 UTC. The full logs are available at
http://meetbot.debian.net/debian-ctte/2016/debian-ctte.2016-07-28-18.00.log.html
.



Meeting summary
---------------
* Check-in round  (OdyX, 18:01:25)

* #830344: How should the TC help with a project roadmap?  (OdyX,
  18:06:59)
  * ACTION: all TC member who haven't mailed their extensive opinion
    should do so in the next week.  (OdyX, 18:20:54)
  * ACTION: keithp to champion the TC's response to 830344  (keithp,
    18:21:50)

* Approval of the Minutes of the 2016-07-03 Breakfast Meeting  (OdyX,
  18:21:59)
  * ACTION: hartmans to check in breakfast meeting minutes.  (hartmans,
    18:23:51)

* 2016-07-03 Actions  (OdyX, 18:25:22)

* Actions - Actions without votes  (OdyX, 18:26:15)
  * AGREED: Bugs really uncontroversial just just be closes, anybody in
    the project can obviously object, thereby triggering a concrete
    vote.  (OdyX, 18:36:52)

* Actions - Email Response Time  (OdyX, 18:37:21)

* #830978: Browserified javascript and DFSG 2  (OdyX, 18:43:11)
  * AGREED: hartmans to champion #830978 about browserified JS.  (OdyX,
    19:07:11)

* New member selection process  (OdyX, 19:07:25)

* #830978: Browserified javascript and DFSG 2  (OdyX, 19:08:22)
  * AGREED: hartmans to close #830978 and suggest talking to FTP Masters
    if they see fit.  (OdyX, 19:12:31)

* New member selection process  (OdyX, 19:12:35)

* Additional Business (varia)  (OdyX, 19:17:15)

Meeting ended at 19:19:42 UTC.




Action Items
------------
* all TC member who haven't mailed their extensive opinion should do so
  in the next week.
* keithp to champion the TC's response to 830344
* hartmans to check in breakfast meeting minutes.




Action Items, by person
-----------------------
* hartmans
  * hartmans to check in breakfast meeting minutes.
* keithp
  * keithp to champion the TC's response to 830344
* **UNASSIGNED**
  * all TC member who haven't mailed their extensive opinion should do
    so in the next week.




People Present (lines said)
---------------------------
* hartmans (82)
* OdyX (78)
* aba (43)
* keithp (36)
* Mithrandir (34)
* marga (27)
* dondelelcaro (24)
* ScottK (5)
* Ganneff (4)
* mehdi (4)
* helmut (3)
* MeetBot (2)
* jcristau (1)




Generated by `MeetBot`_ 0.1.4

.. _`MeetBot`: http://wiki.debian.org/MeetBot
