19:01:18 <OdyX> #startmeeting
19:01:18 <MeetBot> Meeting started Wed Oct 18 19:01:18 2017 UTC.  The chair is OdyX. Information about MeetBot at http://wiki.debian.org/MeetBot.
19:01:18 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
19:01:39 <OdyX> #topic Introduction round
19:01:41 <hartmans> Sam Hartman
19:01:46 <bremner> David Bremner
19:02:06 <OdyX> Didier Raboud
19:03:02 <OdyX> fil, Mithrandir, marga, keithp : ping
19:05:09 <hartmans> Even if it's just us, I'd like to at least talk about new members and the modemmanager bug
19:05:09 <Mithrandir> Tollef Fog Heen
19:05:20 <OdyX> hartmans: my plan too.
19:06:28 <OdyX> #topic Review of previous meetings' TODOs
19:06:42 <OdyX> I had a writeup about the new members to do, but let's topic it specifically.
19:07:14 <OdyX> Let's move on.
19:07:20 <OdyX> #topic #877024 modemmanager should ask before messing with serial ports
19:07:36 <OdyX> I haven't interacted on that front, but I'm quite happy with the activity on it.
19:08:04 <hartmans> So, I think we need to draft a resolution.
19:08:07 <bremner> As I mentioned outside the meeting, I feel like it's premature to involve the committee in a formal way
19:08:24 <hartmans> I'd sort of prefer to focuse on a technical policy resolution rather than an override the maintainer resolution.
19:08:34 <hartmans> It sounds like if there were policy to follow the maintainer would not object to following it.
19:08:45 <bremner> because non-committee methods of reaching concensus are clearly making progress.
19:09:14 <OdyX> yep. I'm with bremner on that front.
19:10:17 <OdyX> I feel a resolution is bound to be generic, and I don't fancy too generic resolutions much.
19:10:57 <hartmans> .... I'm disappointed, but sounds like I'm in the rough here
19:11:00 <OdyX> We're mostly in agreement that blind-addressing random USB devices is bad™, and that specific upstream seems to agree, is making steps in that direction, and is being very helpful.
19:11:28 <Mithrandir> hartmans: can you expn on why you're disappointed?
19:11:34 <hartmans> Upstream seems to agree, but the debian maintainer has been fairly absent from the discussions, right?
19:11:47 <OdyX> hartmans: I'd like to understand your disappointment; in what way would you welcome a formal resolution.
19:12:13 <OdyX> ?
19:12:16 <hartmans> I think that  there's active discussion with upstream.
19:12:43 <hartmans> But I think other methods within Debian have been tried and have not produced anything, and I think Debian needs to figure out what its needs are and those might be different from upstream.
19:12:45 <OdyX> well, if I were a maintainer with an upstream active in the discussion, where I mostly followed what upstream did, I would also back off until dust settles and follow consensus.
19:13:05 <hartmans> And absent the debian maintainer stepping in, I think the TC is a reasonable place to set policy on blind addressing usb devices
19:13:36 <hartmans> If we'd seen the maintainer indicate they were doing that, I'd agree with you and David more
19:14:13 <OdyX> we did not explicitely ask the maintainer, did we ?
19:14:23 <hartmans> How would people feel about polling the maintainer?
19:14:40 <hartmans> My opinion is that if we get silence from the maintainer it would be better for us to rule
19:14:57 <hartmans> but I think I'd agree that if  the maintainer indicates they will follow upstream we're probably good.
19:15:10 <OdyX> It feels like a trap to said maintainer, frankly.
19:15:12 <Mithrandir> the initial bug filed was not Cc-ed to the maintainer.
19:15:17 <bremner> why would the maintainer _not_ follow upstream? this seems like a strange discussion to me.
19:15:22 <OdyX> unless phrased _very_ openly.
19:15:35 <hartmans> bremner:  Well, upstream is *not* changing the default behavior
19:15:42 <hartmans> bremner:  upstream is adding a new command line switch
19:15:57 <OdyX> They're saying "here's what Debian could do if these are its requirements".
19:16:05 <OdyX> It seems to me the discussion is having a good output.
19:16:06 <bremner> once that exists, someone can ask that it be enabled.
19:16:28 <bremner> I am worried that our intervention at this point may create more problems than it solves
19:16:30 <OdyX> If the maintainer follows upstream blindly despite the public discussion in front of the TC, I'd be very much in favour of a resolution.
19:16:35 <OdyX> But it seems premature to me.
19:16:40 <Mithrandir> I would be happy for somebody to poke the maintainers pointing out the bug and asking if they have any input other than what's in there already?
19:16:54 <hartmans> I'd be happy to do that.
19:17:06 <OdyX> Great.
19:17:20 <Mithrandir> there's no explicit indication they know of the bug.  I think Michael follows the tech-ctte list, but I don't actually know.
19:17:23 <bremner> sure, no objection to some informal discussions
19:17:33 <OdyX> #action hartmans to poke the maintainers pointing out the bug and ask if they have any input other than what's in there already.
19:17:37 <OdyX> Great.
19:17:44 <OdyX> That would move us forward.
19:18:01 <OdyX> Also, I prefer to not assume they have been silent on purpose.
19:18:13 <hartmans> agreed
19:18:26 <Mithrandir> we should probably make it part of our standard checklist to poke the maintainers if they're not Cc-ed on the initial bug filing
19:18:37 <hartmans> probably even if they are
19:18:39 <Mithrandir> (or otherwise obviously aware, such as by contributing to the bug)
19:18:51 <OdyX> We don't have such a checklist.
19:18:56 <OdyX> Should we get one started ?
19:19:54 <bremner> sounds like work ;)
19:20:13 <OdyX> Mithrandir: would you be willing to start a _very_ small one with that in place ?
19:20:17 <Mithrandir> maybe? at the risk of severe topic drift: We've been thinking about various workflows to ensure bugs don't get lost, maybe what we need is a standard procedure we can grab when something comes in?
19:20:32 <Mithrandir> I can surely add ten lines of text to git and we can see if it's useful.
19:20:43 <Mithrandir> if it is, great, if not, well, that was those 15-30 minutes.
19:20:47 <bremner> the policy people have some (possibly overcomplicated) set of tags they use to document workflow
19:20:51 <OdyX> #action Mithrandir to start an initial "bug handling" checklist.
19:20:54 <OdyX> #save
19:21:12 <Mithrandir> bremner: I think the BTS is a terrible workflow system.
19:21:26 <bremner> except for all the other ones...
19:21:40 <Mithrandir> anyway, let's move on?
19:21:44 <OdyX> Yep
19:21:49 <bremner> anyway, not wedded to it, just mentioned an existing solution to a similar problem
19:21:59 <OdyX> #topic recruitment process.
19:22:05 <OdyX> I played with how to get a discussion moving.
19:22:59 <OdyX> Context is: our private ranking vote there is likely unconstitutional, so Q_ suggested to have a public discussion about that.
19:23:36 <OdyX> So my current thinking is that a procedures/recruitment.md text is likely a good discussion starter.
19:24:07 <OdyX> With that in place (which doesn't exist yet), we could/should have a discussion on d-ctte and/or d-project
19:24:19 <OdyX> But that's just outlining and discussing the process.
19:24:30 <hartmans> makes sense
19:24:48 <bremner> ok, so nothing to object to yet ;)
19:25:06 <OdyX> But the start of that hypothetical file is "get a candidates' list"
19:25:24 <OdyX> and I think dondelelcaro had a set of d-d-a emails for the call for candidacies.
19:25:30 <OdyX> so we need to act on that now.
19:25:44 <hartmans> Well, can we act on that before people know if our rank vote will be private?
19:25:50 <OdyX> Knowing keithp's term expires in 2.5 months
19:26:23 <OdyX> hartmans: well, nothing prevents us to "figure out a ranking" in private, without a vote
19:26:39 <hartmans> That's not OK.
19:26:44 <OdyX> (yeah, I know, we use condorcet because it's a ranking process we know and use)
19:26:57 <hartmans> There's only so much rules lawyering that seems ethical to tolerate
19:27:07 <OdyX> sure, I was playing silly. Sorry for that.
19:27:30 <bremner> we can ask people later if the process is about to become public, if they still want to participate
19:27:32 <hartmans> Either it's OK to do the ranking vote in private, or it's not.  Calling it something else to get around an openness requirement strikes me as way over the line.
19:27:58 <hartmans> bremner:  makes sense and an easy thing to do
19:28:48 <OdyX> hartmans: indeed. Sorry for suggesting that.
19:29:01 <OdyX> So we can launch the two processes in parallel, can we ?
19:29:12 <hartmans> yep
19:29:15 <bremner> ack
19:29:21 <hartmans> FYI mail sent to modemmanager maintainer
19:29:51 <OdyX> okay. I can focus on writing down the process, but need someone else to launch the call for candidacies.
19:30:01 <OdyX> (or I can happilly swap… )
19:30:32 <bremner> I probably am pretty useless for the next few weeks.
19:31:12 <bremner> after that, I can help with either task.
19:31:24 <OdyX> Okay.
19:31:44 <OdyX> #action OdyX to draft an initial write-down of the process, and launch a discussion
19:32:15 <OdyX> #action OdyX to send the very first d-d-a calling for candidacies.
19:32:19 <OdyX> #topic Varia
19:32:22 <OdyX> #save
19:32:29 <OdyX> Anything else relevant for the TC ?
19:34:26 <Mithrandir> nothing from me
19:34:31 <bremner> me neither
19:34:41 <OdyX> #endmeeting