18:00:04 <dondelelcaro> #startmeeting
18:00:04 <MeetBot> Meeting started Thu Feb 26 18:00:04 2015 UTC.  The chair is dondelelcaro. Information about MeetBot at http://wiki.debian.org/MeetBot.
18:00:04 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
18:00:07 <keithp> good morning!
18:00:10 <dondelelcaro> #topic Who is here?
18:00:12 <dondelelcaro> Don Armstrong
18:00:32 <dondelelcaro> (I'm on a bus, so I'm going to be really laggy, and will have to disapear in about 25 minutes until I walk to where I'm going...)
18:00:49 * keithp is here
18:01:32 <dondelelcaro> bdale, vorlon, aba cjwatson: ping
18:01:32 <cjwatson> dondelelcaro: Please tell me what you want and I'll reply when I'm around.
18:01:39 <dondelelcaro> heh
18:03:19 <vorlon> hi
18:03:42 <dondelelcaro> cool
18:04:29 <dondelelcaro> #topic Next Meeting?
18:04:41 <vorlon> #agreed we should have a next meeting
18:07:12 <vorlon> next topic? ;)
18:07:31 <dondelelcaro> 2015/03/26 17:00 UTCcurrently it's scheduled for 2015/03/26 17:00 UTC
18:07:35 <ScottK> Don't you need to argue about the color of the meeting first?
18:07:47 <dondelelcaro> sorry; I'm on a really laggy connection
18:07:58 <dondelelcaro> topic #769972 New member selection process
18:08:01 <dondelelcaro> #topic #769972 New member selection process
18:08:29 <dondelelcaro> does anyone object to the top candidates now? I believe the next step is just to vote on them
18:08:40 <dondelelcaro> should we be voting on them in a single block? or individually?
18:09:37 <keithp> dondelelcaro: individually seems like it makes some sense?
18:09:46 <dondelelcaro> ok
18:09:54 <vorlon> if we all agree on the top two candidates, and we're filling two slots, what reason is there for further voting?
18:10:09 <vorlon> isn't the next step to just present our recommendations to the DPL?
18:10:13 <dondelelcaro> vorlon: we have to vote publicly
18:10:32 <vorlon> before or after we consult the DPL?
18:10:35 <vorlon> which AFAIK we haven't done yet
18:10:52 <dondelelcaro> I think we can consult the DPL at any point; but we have to publicly vote to recommend them to the DPL
18:10:57 <vorlon> oh, ok
18:11:03 <vorlon> then that's fine with me
18:11:06 <keithp> vorlon: iirc, we vote to recommend; that's how we know what names to offer
18:11:19 <dondelelcaro> but we can just include the top condidates and make it a simple yes/FD vote
18:11:27 <dondelelcaro> ISTR that's what we did previously
18:11:28 <keithp> agreed
18:11:29 <vorlon> clearly we should rotate people out of the TC more often so that I remember the procedure ;)
18:11:45 <dondelelcaro> heh
18:12:08 <keithp> vorlon: I think we'll have that opportunity
18:12:20 <keithp> vorlon: although, I suspect *you* may not get a lot more practice...
18:12:54 * Maulkin nods - basically what dondelelcaro/keithp said.
18:13:00 * Maulkin re-lurks
18:13:03 <dondelelcaro> ok; I guess we can contact leader@, and ask first, then do a public vote to recommend
18:13:15 <dondelelcaro> #action dondelelcaro or bdale to contact leader@ to verify top candidates
18:13:27 <dondelelcaro> #action dondelelcaro to start vote to recommend candidates after leader@ weigh in
18:13:41 <dondelelcaro> #topic #636783 constitution: various bugs
18:14:04 <dondelelcaro> last meeting, we decided that we'd drop these if no one stepped forward to champion them; does anyone want to champion them?
18:14:24 <vorlon> not I
18:14:38 <vorlon> it would be great if Diziet would continue to drive these IMHO
18:14:42 <aba> sorry for being late
18:14:47 <dondelelcaro> aba: no worries
18:14:55 <dondelelcaro> I think we can just let these happen through the normal GR process?
18:14:57 <aba> please give me 2 minutes to read backlog
18:15:03 <aba> dondelelcaro: I would step forward for those
18:15:03 <dondelelcaro> sure
18:15:09 <dondelelcaro> aba: OK
18:15:26 <vorlon> dondelelcaro: yes, that's what I mean wrt Diziet driving :)
18:15:40 <dondelelcaro> right
18:15:45 <keithp> aba: if you want to propose the patch to the ctte, we can vote on whether to propose a GR, right?
18:15:54 <aba> keithp: right.
18:16:06 <dondelelcaro> ok; that's good enough for me
18:16:14 <aba> but if Diziet wants to drive it outside of the ctte that's ok for me either
18:16:38 <dondelelcaro> #agreed aba to champion #636783 perhaps in consultation with Diziet
18:16:47 <dondelelcaro> #topic #741573 menu systems and mime-support
18:17:05 * keithp hangs head in shame having done nothing on this topic since new $dayjob
18:17:14 <dondelelcaro> heh
18:17:28 <dondelelcaro> but that's still where we're planning on going with this one, right?
18:17:31 <keithp> yes
18:17:43 <dondelelcaro> #action keithp continue discussing with -policy
18:17:57 <dondelelcaro> #topic #771070 Coordinate plan and requirements for cross toolchain packages in Debian
18:19:02 <dondelelcaro> I've done a tiny bit of coordination here, but it looks like the main parties have actually done the hard work themselves
18:19:16 <dondelelcaro> hopefully in a bit there will be an announcement regarding their agreement and plan going forward
18:19:20 <keithp> awesome
18:19:34 <keithp> bdale and I spent a bunch of time at fosdem talking to the interested parties
18:19:46 <dondelelcaro> I'd like to keep this bug open, but I don't think the CTTE needs to do anything but maybe make a statement congratulating the parties on working together
18:20:02 <keithp> once resolved, we can close it
18:20:20 <dondelelcaro> (though we should keep an eye on it, and try to help keep it moving forward as people)
18:20:23 <dondelelcaro> right
18:20:27 <dondelelcaro> #agreed keep this bug open for the time being
18:20:36 <dondelelcaro> #topic #750135 Maintainer of aptitude package
18:20:54 <dondelelcaro> from my notes, this one was going to be something that aba was working on
18:21:10 <dondelelcaro> aba: is that still the plan?
18:21:34 <aba> I'm not unhappy if someone else takes over. Otherwise I hope to find time during my upcoming vacations
18:21:38 <aba> (i.e. dayjob)
18:21:57 <dondelelcaro> okie dokie; I'm not on fire to take over this one myself, because it seems hrad.
18:22:02 <dondelelcaro> s/hrad/hard/
18:22:29 <dondelelcaro> #action aba to hope to find time to deal with this unless someone else wants to take over
18:22:36 <dondelelcaro> #topic Additional Business
18:23:09 <dondelelcaro> anything else?
18:25:03 <aba> looks like not
18:25:11 <dondelelcaro> okie dokie; hope to see you all next time.
18:25:15 <dondelelcaro> #endmeeting