Technical Committee Monthly meeting agenda
==========================================

> #startmeeting

## Review of previous meeting AIs

## #topic #932795  How to handle FTBFS bugs in release architectures

## #topic #934948  Unnecessary dependencies vs multiple binary packages

## #topic The nature of the TC and whether we want to change something

## #topic Additional Business

> #endmeeting
