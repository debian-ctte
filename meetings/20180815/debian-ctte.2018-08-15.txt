====================
#debian-ctte Meeting
====================

Meeting started by marga at 19:05 UTC. The full logs are available in
debian-ctte.2018-08-15.log.txt.

Meeting summary
---------------

* Round of names. (marga, 19:05)

* #904302 Whether vendor-specific patch series should be permitted in
  the archive (marga, 19:08)
  * ACTION: Mithrandir to draft the resolution so that we can vote on it
    (marga, 19:13)

* #904558 What should happen when maintscripts fail to restart a service
  (marga, 19:13)
  * ACTION: Marga to write up a summary of the discussion here and send
    it to the bug.  Discussion to continue there. (marga, 19:51)

* Any interesting things to share from DC18? (marga, 19:53)

* Any additional business? (marga, 19:59)

Meeting ended at 20:07 UTC.
