17:00:15 <dondelelcaro> #startmeeting
17:00:15 <MeetBot> Meeting started Thu Oct 30 17:00:15 2014 UTC.  The chair is dondelelcaro. Information about MeetBot at http://wiki.debian.org/MeetBot.
17:00:15 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
17:00:17 <dondelelcaro> #topic Who is here?
17:00:19 <dondelelcaro> Don Armstrong
17:00:23 <Diziet> Ian Jackson
17:00:23 <rra> Russ Allbery
17:00:53 <dondelelcaro> vorlon (Steve) sends in his semi-regrets
17:00:54 <cjwatson> Colin Watson
17:01:26 <dondelelcaro> bdale, keithp, aba: ping
17:02:26 <dondelelcaro> those three are long-time idle, so I'll just continue on
17:02:35 <dondelelcaro> #topic Next Meeting?
17:03:33 <dondelelcaro> currently this is Thursday the 27th at 18:00 UTC iirc.
17:03:42 <cjwatson> Works for me
17:03:54 <dondelelcaro> unfortunately, I will probably be in a location with really bad internet, so I might not be available
17:04:06 <keithp> sorry for being a few minutes late
17:04:17 <dondelelcaro> but I should know that week; someone should just take over for me and run the meeting
17:04:33 <Diziet> Would the previous or the next week be better for you ?
17:05:07 <cjwatson> I'm probably unavailable the previous week
17:05:15 <cjwatson> Though the next week works too
17:05:18 <dondelelcaro> the next week would, but unless lots of people have trouble with thanksgiving, we might as well just work it
17:05:36 <dondelelcaro> does anyone have a problem with moving that back one week?
17:06:00 <keithp> not me
17:06:15 <Diziet> Fine by me
17:06:23 <cjwatson> Fine by me
17:06:32 <dondelelcaro> #action dondelelcaro to tentatively move next meeting back a week, and ask on -ctte if anyone else has a problem
17:06:43 <dondelelcaro> #topic #636783 constitution: super-majority bug
17:07:01 <Diziet> Can we put the constitutional stuff to the back of the queue given the release timing and everything else ?
17:07:10 <dondelelcaro> sure
17:07:20 <rra> Yeah, the week before would be better for me -- I'll have company over Thanksgiving.  (Sorry, people talking to me.)
17:07:26 <dondelelcaro> topic #681419 Depends: foo | foo-nonfree
17:07:33 <dondelelcaro> #topic #681419 Depends: foo | foo-nonfree
17:07:46 <dondelelcaro> I just need to finalize this
17:07:52 <dondelelcaro> #action dondelelcaro to finalize #681419
17:08:01 <dondelelcaro> #topic #741573 menu systems and mime-support
17:08:19 <dondelelcaro> I think keithp wrote up the start of the ballot for this already
17:08:26 <Diziet> I have to say I'm not convinced by Keith's categorisation.
17:08:51 <Diziet> "Require" might mean lots of different things.
17:09:02 <keithp> Diziet: I promised to write up one more position and haven't gotten to that yet
17:09:30 * bdale just got online
17:09:37 <bdale> at LF BOD in-person meeting today
17:09:38 <dondelelcaro> #action keithp to write up one more position for #741573
17:09:59 <keithp> too much travel in my life
17:10:11 <Diziet> categorisation> I mean as found in keithp_alternatives.txt eg
17:10:57 <dondelelcaro> OK. Do we have anything else for this bug?
17:11:23 <dondelelcaro> #topic #762194 Automatic switch to systemd on wheezy->jessie upgrades
17:11:24 <bdale> not from me
17:11:26 <keithp> Diziet: which is why I promised to write up the middle position more completely
17:11:49 <dondelelcaro> I think the next three topics are really kind of all the same; is that a correct characterization?
17:12:03 <dondelelcaro> #765803 Ask before changing init system when upgrading to jessie and Inform about init systems when installing jessie
17:12:11 <dondelelcaro> #746578 libpam-systemd: for upgrade safety, swap or-dependency to systemd-shim|systemd-sysv
17:12:14 <dondelelcaro> for reference
17:12:27 <rra> The last is somewhat different, but it probably makes sense to discuss them all together.
17:12:28 <Diziet> 765803 and 762194 are the same
17:12:28 <bdale> they certainly could all fall in to one discussion bucket from my perspective
17:12:40 <Diziet> 746578 is a detail which depends on the former
17:13:02 <dondelelcaro> right
17:13:13 <dondelelcaro> let me just merge #765803 and #762194
17:13:13 <Diziet> I would like to get to a vote about 762194/765803 (switching existing systems) ASAP.
17:13:26 <Diziet> I can't imagine there can be much more to be said about the principle of the question.
17:13:38 <rra> I'm feeling generally uncomfortable about changing init systems on dist-upgrade.  I think we went through two release cycles with dependency-based boot IIRC; it was optional and then it became automatic.
17:13:40 <bdale> as I indicated in email, I think voting on those while the GR is in process isn't clearly a good idea
17:13:44 <Diziet> And at this stage of the release it is still possible to make a decision on this without making too much disruption.
17:14:02 <Diziet> bdale: The GR is going to be another 3 weeks.
17:14:05 <rra> However, I would feel a lot more comfortable voting to not change init systems on upgrade if someone had a fairly complete proposal for exactly what dependencies between various packages they propose.
17:14:09 <Diziet> We should decide on the automatic switch before then IMO
17:14:19 <rra> I don't think this is trivial, and it would be nice to have a fleshed alternative that someone has actually tested.
17:14:45 <rra> As Tollef pointed out, it's important to get the right behavior between the various ways of bootstrapping a new Debian install.
17:14:54 <bdale> Diziet: I'd have been happy to act on this sooner if the GR were not already in process
17:15:00 <Diziet> At the moment the dependencies we have have known serious bugs, which is that your init system can be swapped if you install some packages.
17:15:30 <Diziet> I don't think it's reasonable to say that we need a tested alternative given how bad the situation is right now.
17:15:46 <rra> Well, then I'm unreasonable.  *shrug*
17:15:57 <rra> The current situation worked trivially and reliably for me.
17:16:01 <ansgar> Diziet: I think it's unclear that it is a serious bug if there is an open GR on that topic.
17:16:02 <dondelelcaro> if we don't have a tested alternative, it's not clear to me what we'd actually be mandating maintainers to do
17:16:18 <bdale> without some sort of tested alternative to vote on, all we're going to be doing if we vote is to apply "stop energy", right?
17:16:35 <Diziet> rra: Have you seen the reports of switching inits as a result of package installations ?
17:16:48 <rra> Diziet: Yes, we're switching init systems on dist-upgrade right now.
17:16:57 <Diziet> bdale: The detailed implementation is AIUI mostly 746578
17:16:57 <rra> I know that.  That's the whole point of the bug.  :)
17:16:59 <vorlon> fwiw I remain unpersuaded that we should try to avoid switching init on upgrade
17:17:05 <vorlon> (hi, work meeting deferred)
17:17:16 <Diziet> rra: No, I mean, if you are running jessie and say apt-get install blatherwossname, you might find that your init is changed.
17:17:26 <rra> Diziet: Yes, that's a special case of dist-upgrade.
17:17:43 <rra> It's all part of the same dependency structure.
17:17:51 <Diziet> Forget about what it's a special case of.  It's surely not desirable.
17:18:00 <cjwatson> I'm as yet undecided regarding switching init on upgrade, but switching on leaf package installations suggests to me that something is excessively fragile and needs attention
17:18:01 <rra> If we're going to switch init systems on dist-upgrade, that necessarily implies we're switching init systems for particular package upgrades.
17:18:18 <cjwatson> This is true, but those particular packages should be deep in the stack, not some random daemon
17:18:19 <Diziet> rra: A good reason not to do the former!
17:18:43 <cjwatson> (I don't know the details of this, but I'm inferring from Diziet's use of metasyntactic variables that they aren't particularly intrinsically important packages)
17:18:46 <keithp> Diziet: I think this is why having this discussion while the GR is pending is not useful
17:18:51 <rra> I will say that the feedback I'm getting from people outside of Debian, including people who are actually fairly fond of systemd in the abstract, is that having the init system change on dist-upgrade is very undesirable.
17:18:56 <Diziet> cjwatson: You're right.  They're applications, in the reports I've seen.
17:19:02 <bdale> the thing I wouldn't want is for my init system to be changed out without having some chance to say "stop that!"
17:19:08 <rra> There are a lot of people with customized init scripts and other hacks that will need to be reworked during the switch.
17:19:14 <Diziet> bdale: That's exactly what's happening right now.
17:19:19 <bdale> on the other hand, I'm routinely careful about dist-upgrades for exactly that reason
17:19:23 <rra> This was also the case with dependency-based boot, but in that case we could detect the problems.  Here, we can't easily.
17:19:39 <bdale> Diziet: I know, but I don't think the "fix" is to require that it not happen, I think the fix is to ensure the user knows it's about to happen
17:19:45 <dondelelcaro> my problem with this particular issue is we don't currently have a set of changes which need to be made now, with a set of patches which are tested
17:19:52 <rra> Diziet, cjwatson: That's a good point -- I think what I'm driving at is that if we decide not to change init systems on dist-upgrade, that all goes way anyway.
17:19:58 <rra> dondelelcaro++
17:19:58 <Diziet> The dependency based boot wasn't done at the level of installing different packages.  So it could degrade to a non-dependency-based approach when it detected problems.
17:20:11 <cjwatson> rra: We certainly have common ground there
17:20:21 <dondelelcaro> I think we're mostly agreed that switching randomly is bad, but no one has yet done the work to actually outline a solution to this
17:20:25 <Diziet> dondelelcaro: We have the swap dependency patch, which is at the very least a dramatic improvement.
17:20:45 <dondelelcaro> Diziet: right, but I think that's just one small part of the whole issue
17:20:49 <Diziet> Are you really saying that we shouldn't make that change because we don't know for sure that the result is perfect ?
17:20:54 <cjwatson> Tollef seems happier with systemd-shim since the removal of the cloned-and-hacked dbus policy
17:20:55 <dondelelcaro> Diziet: no.
17:20:58 <cjwatson> (AIUI)
17:21:03 <rra> I think the dependencies on libpam-systemd should be swapped regardless; I haven't yet seen a good reason not to do that.
17:21:11 <dondelelcaro> Diziet: I'm saying that unless the only change is the libpam-systemd change, I don't know what else is being proposed
17:21:17 <rra> Maybe I'm missing something, but if so, someone should explain it to me.  :)
17:21:26 <cjwatson> Yeah, there were formerly some defensible reasons, but they seem to have been dealt with
17:21:45 <Diziet> rra: OK.
17:21:51 <ansgar> rra: I think all potential issues with having systemd-shim|systemd-sysv are dealt with.
17:22:00 <Diziet> Can we say something like "swapping init as a result of leaf packages is not desirable"
17:22:03 <dondelelcaro> is there anyone who disagrees with the libpam-systemd dependency swap?
17:22:20 <ansgar> Diziet: That conflicts with the GR options doesn't it?
17:22:45 <rra> Diziet: If the GR that says packages can freely depend on a specific init system wins, that contradicts that decision.
17:22:50 <Diziet> And something like "our decision in February was not intended as a mandate to swap init system on upgrades"
17:22:55 <dondelelcaro> and, if there's no one who disagrees, is there anyone who disagrees with at least getting a ballot written up to do that override? (libpam-systemd)
17:23:07 <rra> I would really rather not pass a TC decision right now that contradicts one of the GR ballot options.
17:23:12 <rra> Down that path lies constitutional madness.
17:23:23 <ansgar> dondelelcaro: I'm not sure you still need an override for that.
17:23:28 <Diziet> rra: How about we put it as a tentative piece of advice, or something.
17:23:34 <Diziet> I'm sure we can fix that with wording.
17:23:35 <dondelelcaro> ansgar: well, if we decide it, we're going to override the maintainer.
17:23:39 <rra> dondelelcaro: I'm in favor of that assuming that we need to decide things at all.
17:23:44 <cjwatson> I don't see a reason not to prepare it and start discussing, at least
17:23:59 <Diziet> I haven't seen anyone suggest we shouldn't just overrule.
17:24:10 <rra> Diziet: I would rather see a concrete proposal for what dependencies should be changed.
17:24:18 <bdale> me too
17:24:23 <dondelelcaro> ok; at least in this bit, I think we can make headway without deciding everything else
17:24:25 <Diziet> rra: 746578 has a concrete proposal
17:24:26 <rra> Rather than just saying something vague like saying no leaf packages should result in init system changes.
17:24:43 <dondelelcaro> #agreed libpam-systemd to flip dependencies
17:24:43 <Diziet> I will write something up which I think has half a chance of you agreeing with it.
17:24:55 <Diziet> If you disagree with the specific wording please let me know.
17:25:05 <Diziet> #action Diziet to draft resolution on #746578
17:25:13 <bdale> works for me
17:25:14 <rra> Diziet: 746578 is the libpam-systemd dependencies, isn't it?
17:25:18 <dondelelcaro> rra: right
17:25:19 <rra> I'm fine with deciding that now.
17:25:22 <Diziet> rra: Yes.
17:25:25 <Diziet> Good.
17:25:26 <rra> I think we're actually in agreement there.
17:25:36 <rra> And I think that's orthogonal to the GR.
17:25:42 <rra> And indeed, it has a concrete proposal.
17:25:57 <Diziet> That's what I meant.
17:26:05 <rra> Ah, okay, we're on the same page then.
17:26:10 <ansgar> dondelelcaro: Well, I mean the systemd maintainers weren't too opposed to it with the last changes (IIRC).
17:26:14 <Diziet> One reason swapping it was opposed is that it was necessary to ensure the swap-on-upgrade
17:26:29 <rra> I think Steve analyzed the dependency tree and found that wasn't the case, didn't he?
17:26:36 <Diziet> I think given the rest of the comments we have to punt on the wider question
17:26:58 <Diziet> Or:
17:27:06 <dondelelcaro> ansgar: *shrug*; if they've already flipped it by the time we decide, great.
17:27:24 <rra> I continue to be fine with passing a simple GR saying that our decision in February was not intended to imply any position on whether init should be changed during upgrade.
17:27:33 <rra> I don't know if that's helpful or not, but it would be fine with me to say that, since it's true.
17:27:40 <Diziet> How about this:  "We aren't convinced we want to swap on upgrades and we invite submissions of a concrete set of technical changes to ensure that existing machines keep their existing inits, while new Linux installations get systemd"
17:27:41 <vorlon> there was a statement from one of the systemd maintainers (mbiebl, I think?) that he wanted admins to have to explicitly make the choice of systemd-shim over systemd-sysv, separately from choosing sysvinit over systemd-sysv
17:27:42 <rra> Er, not a GR.
17:27:45 <rra> A simple TC decision.
17:27:54 <Diziet> rra: Right.
17:27:56 <Diziet> Good.
17:28:07 <vorlon> I certainly disagree with that
17:28:18 <rra> vorlon: Ah, yes, I remember that.  But I don't think that makes sense.  At least, I don't understand what the point would be.
17:28:29 <Diziet> vorlon: You think our decision in February was intended to imply a change on upgrade ?
17:28:31 <rra> Taking a step back, I expect us to have two supported configurations for logind in jessie.
17:28:35 <rra> 1) systemd-sysv.
17:28:41 <rra> 2) sysvinit-core plus systemd-shim.
17:28:49 <vorlon> rra: right; so unless someone marshalls an argument that he finds persuasive, this seems to be a matter for TC override of a maintainer
17:29:03 <rra> I don't see where people have two real choices.  Either they're using systemd, or they're using sysvinit-core and will need systemd-shim if they need logind.
17:29:26 <Diziet> I think we're on agreement on that.
17:29:30 <vorlon> Diziet: sorry, I don't understand how that question is germane to the current discussion
17:29:31 <rra> If they've already chosen to use sysvinit-core, we should just give them systemd-shim and move on with our lives.
17:29:45 <cjwatson> vorlon,Diziet: I think you two are perhaps suffering from crosstalk
17:29:53 <Diziet> vorlon: You said    17:28 <vorlon> I certainly disagree with that     but I don't know what you were disagreeing with
17:29:57 <rra> Diziet: vorlon was disagreeing with the statement he was relaying.
17:30:00 <rra> His previous line.
17:30:00 <vorlon> yes
17:30:01 <Diziet> OIC
17:30:03 <Diziet> Right.
17:30:05 <Diziet> OK.
17:30:20 <Diziet> I don't think we need any more discussion here on 746578 (swap deps)
17:30:31 <dondelelcaro> right
17:30:32 <Diziet> Can we get anywhere on 762194/765803 (automatic switch) ?
17:30:49 <bdale> not until the GR closes
17:30:51 <Diziet> 17:27 <rra> I continue to be fine with passing a simple [decison] saying that our decision in February was not intended to imply any position on whether init should be changed during upgrade.
17:30:51 <dondelelcaro> does anyone disagree with having a simple tc statement on automatic switching?
17:31:07 <vorlon> I'm not sure I see how deciding that conflicts with the current GR
17:31:15 <bdale> dondelelcaro: I find such statements relatively pointless, but wouldn't object strongly
17:31:15 <rra> I don't think making that statement conflicts.
17:31:19 <Diziet> bdale opposes it but it sounds like we have a majority for it, and it would certainly move the debate forward.
17:31:25 <vorlon> dondelelcaro: what is that statement meant to be?
17:31:43 <vorlon> as I said above, I actually still think that automatically switching is the right thing to do on upgrade
17:31:44 <rra> I think deciding anything stronger potentially conflicts with the ballot option allowing packages to depend on an init system.
17:31:47 <Diziet> Can we get as far as    "We aren't convinced we want to swap on upgrades and we invite submissions of a concrete set of technical changes to ensure that existing machines keep their existing inits, while new Linux installations get systemd"   ?
17:31:56 <dondelelcaro> vorlon: what Diziet and rra described; interpreting our previous decision as to not do automatic switching
17:31:56 <keithp> vorlon: if packages are allowed to depend on a specific init system, then you *will* be switching on upgrade to jessie if you have those packages installed
17:32:08 <rra> vorlon: It's just a clarification in case anyone thought the TC had already decided in favor of automatic init swaps.
17:32:16 <rra> I don't know that it's important.
17:32:23 <rra> But if it helps, I'm fine with saying it, since it's true.
17:32:25 <bdale> dondelelcaro: the problem I have is that if we say that, in the current climate, it's actually an assertion against switching
17:32:30 <Diziet> I think it is important.  Several people have interpreted the TC decision that way.
17:32:45 <cjwatson> I can see the benefits in terms of simplicity of Debian's default stack of switching, but I'm very concerned about its effects on deployed systems for the sorts of reasons rra was relaying earlier
17:32:49 <vorlon> however, notifying the admin that this has been done, so they have the opportunity to select a non-default init before reboot, is important
17:33:08 <Diziet> Can we get as far as    "We aren't convinced we want to swap on upgrades and we invite submissions of a concrete set of technical changes to ensure that existing machines keep their existing inits, while new Linux installations get systemd"   ?
17:33:11 <vorlon> rra, Diziet: ah; I hadn't seen that anyone was relying on TC authority for the actions they were taking in this regard
17:33:12 <cjwatson> And the grub2 patch that's on me to deal with would help, it's true
17:33:18 <Diziet> (I keep asking this because no-one is saying yes or no...)
17:33:21 <bdale> Diziet: I completely understand how important you think this is.  But to re-start the GR process, and then call for votes here in parallel just doesn't sit well
17:33:27 <rra> vorlon: To be clear, I hadn't either, but Diziet said he'd seen it.
17:33:36 <vorlon> and I wouldn't expect withdrawing any presumed TC authority to change what the maintainers are doing
17:33:44 <vorlon> so such a statement indeed seems a no-op to me
17:33:45 <rra> vorlon: Yeah, agreed there.
17:34:12 <dondelelcaro> Diziet: I don't think that stating that we don't want to swap on upgrades is something we can agree on
17:34:13 <bdale> vorlon: right .. I see it as either a no-op, or given the current climate, a softly negative assertion
17:34:25 <dondelelcaro> Diziet: at least, not while the GR is happening which seems to directly address this part of the question
17:34:28 <Diziet> dondelelcaro: That's not the question.  The question is whether it's something that would pass a TC vote.
17:34:32 <Diziet> I'm done with consensus decisionmaking.
17:34:38 <Diziet> W
17:34:40 <rra> I would certainly be interested in someone working out the exact dependency changes required and testing them.  I think that would make the discussion more concrete.
17:34:54 <rra> And allieviate some of Tollef's concerns, presumably.
17:35:10 <vorlon> to be clear, I would vote yes on such a statement if it presented itself, but as I don't think it would affect the outcome, I'd rather we get to the root question instead
17:35:28 <vorlon> i.e., to have the actual discussion about the default changing on upgrade
17:35:31 <cjwatson> I think I agree with vorlon on that
17:35:34 <Diziet> That's not to say I'm not open to convincing.  But everything done by my opponents in this whole war has been done on a majoritarian basis and I see no reason to limit myself to consensual acts.
17:35:58 <ansgar> Could we please stop this "war" thing?
17:36:01 <Diziet> cjwatson, vorlon: OK, thanks.  I'm going to action myself to write that up.
17:36:32 <Diziet> #action Diziet to write up (a) clarification of February thing as suggested by rra (b) "invite changes" (as separate options)
17:36:48 <dondelelcaro> Diziet: we can always go to majoritarian, but if we can agree, so much the better.
17:37:10 <rra> Yeah, we do seem to be able to reach consensus agreement on things in this area still, such as the libpam-systemd dependencies.
17:37:17 <Diziet> dondelelcaro: I and my allies have been being shat on by the majoritarians since February.  It's too late for that.
17:37:34 <Diziet> Of course I'm happy to work to get consensus where we can do so quickly.
17:38:00 <Diziet> But perhaps we could shelve this metaargument which is going rapidly downhill.
17:38:09 <bdale> Diziet: please
17:38:45 <dondelelcaro> do we have anything else to discuss on this issue currently?
17:39:06 <rra> I don't think so.
17:39:10 <dondelelcaro> #topic #766708 Request to override gcc maintainer changes breaking multiarch packages
17:39:50 <Diziet> I find myself tending to agree with Helmut.  It's difficult because we've had so little interaction with Matthias but (a) I think he has a responsibility to put his case (even though Helmut has been trying to do it for him) and
17:39:52 <dondelelcaro> I think this one is still being developed, but I'm currently not sure what the maintainer overhead is of this particular patch, and why it needs to be removed this close to jessie release
17:39:57 <rra> I thought I'd made up my mind on this, and then there was a message to the bug recently that basically argued that this facility is not needed, and I haven't digested that.
17:40:10 <Diziet> (b) making a change like this at this stage of the release, without discussion, is not very constructive.
17:40:29 <rra> If we really don't need this facility, then simplicity is obviously better, but when the people who are actively working on cross-compilation say they need something and are willing to maintain it, I'm inclined to defer to them.
17:40:29 <cjwatson> Matthias does need to put his case clearly.  That said as I indicated in my most recent message I'm not sure it actually prevents Helmut doing what he needs (though I have not tested my theory).
17:40:34 <rra> Also, what Diziet just said.
17:41:02 <cjwatson> But I don't think it's in a place where we can really put a motion yet.
17:41:05 <Diziet> rra: I think you're thinking of Dmitri's message, to which Helmut presented a cogent-seeming response.
17:41:05 <rra> It's a little late in the game to be turning stuff off at this point.
17:41:16 <rra> Diziet: Ah, okay.  I hadn't digested that part of the thread at all.
17:41:25 <Diziet> cjwatson: I don't think we should encourage Helmut to play core wars.
17:41:53 <Diziet> I think we should draft a proposal to overrule and see if any further counterarguments come up.
17:41:57 <dondelelcaro> ok
17:42:03 <keithp> my question was whether we should expect there to be a fix which doesn't require reverting the change available in the short term
17:42:04 <dondelelcaro> I think a limited proposal is pretty easy to draft here
17:42:04 <cjwatson> Perhaps not, but it depends why that change was made exactly, and I want to hear more from Matthias there.
17:42:20 <dondelelcaro> yeah; I'd like to hear more too
17:42:31 <rra> Matthias's one response seemed to basically say because he disagreed with that approach and didn't want to maintain two approaches.
17:42:35 <Diziet> It seems obvious to me that Matthias must have known that this change would be unpopular in certain quarters.
17:42:49 <Diziet> I think therefore that he ought to have explained himself in advance.
17:42:55 <bdale> yes
17:42:58 <Diziet> It's not like this is a trivial thing that no-one was using.
17:43:12 <bdale> given that folks actually doing the work on porting are complaining, something went wrong
17:43:20 <rra> Yeah.
17:43:21 <vorlon> I am not sanguine about a maintainer override here; I don't understand why this has come to the TC in that vein.  Is this not a case where everyone would be better served by the TC acting in a mediation role?
17:43:35 <bdale> vorlon: seems likely
17:43:41 <keithp> vorlon: that would be ideal
17:43:43 <rra> That would certainly be better.
17:43:45 <Diziet> vorlon: I think Matthias should have discussed the matter with his users.
17:43:48 <cjwatson> There seems to be a substantial amount of bad feeling either way; I've heard passionate complaints from both sides
17:43:52 <cjwatson> clearly very upset
17:43:58 <bdale> there is, however, the timing issue around jessie freeze at issue, though, right?
17:44:01 <Diziet> Indeed.
17:44:04 <rra> Yeah, we're on the clock here.
17:44:05 <dondelelcaro> right; mediation woudl be best going forward
17:44:07 <cjwatson> But I don't at this point understand the details well enough to mediate, and would need to read up quickly
17:44:12 <Diziet> If this had come up 2 months ago, mediation, sure.
17:44:15 <ansgar> Do the porters use the *source* package from jessie?
17:44:21 <rra> Which is part of the reason why I have less sympathy than I would otherwise; this isn't the sort of change that I would make near the release freeze.
17:44:31 <rra> ansgar: Oh, good question.
17:44:43 <keithp> Diziet: so, if we ask Matthias to revert *for jessie*, and work on mediating a longer term solution, could we get him to agree to that?
17:44:45 <cjwatson> ansgar: The theory being that if so then they could sed it?
17:44:54 <vorlon> Diziet: I don't disagree that Matthias should have done this; the question is whether a maintainer override is the right tool here or if it will just make it harder for folks to work together in the future
17:44:58 <Diziet> keithp: Even Helmut is only asking for an overrule for jessie.
17:45:10 <Diziet> That was clear from his initial mail and Matthias hasn't said "sure"
17:45:14 <ansgar> cjwatson: No, the question is how relevant to change the source package in jessie is.
17:45:23 <keithp> Diziet: right, I'd rather we get Matthias to agree to that instead of forcing the issue
17:45:31 <cjwatson> Ah.  Well, I think they do use it, but as I say I'm fuzzy on the details.
17:45:35 <rra> Yeah, if the porters are always working from sid, I'm not sure whether it matters that much what jessie releases with.
17:45:40 <Diziet> keithp: He's _already_ acted unilaterally, and _already_ declined to agree to that.
17:45:50 <cjwatson> Oh perhaps you meant to stress *jessie* rather than *source*? :-)
17:45:53 <Diziet> We can't force him to answer emails (obviously) and we mustn't wait.
17:45:58 <dondelelcaro> can someone approach Matthias to try to get him to respond to this?
17:46:34 <Diziet> I am very uncomfortable with the idea that we would decline to overrule a maintainer who has made a high-handed decision with poor communication and is unresponsive, precisely _because_ the maintainer is unresponsive.
17:46:52 <Diziet> The maintainer being unresponsive ought to make us more willing to overrule.
17:47:12 <bdale> I agree
17:47:45 <dondelelcaro> how about I give a week for a specific response; if not, I draft the override
17:47:47 <rra> That said, if someone is willing to reach out, it certainly doesn't *hurt* to do so.
17:47:57 <Diziet> I think we should put forward a draft within the next few days, and perhaps give it another week to see if the imminent formal process produces a response.  But I would expect to want to have dealt with this issue within a matter of 2-3 weeks which means starting a vote in 1-2.
17:48:04 <dondelelcaro> right
17:48:06 <Diziet> rra: Certainly.
17:48:09 <rra> I like Don's proposal.
17:48:13 * vorlon nods
17:48:15 <bdale> works for me
17:48:16 <Diziet> dondelelcaro: I would prefer sooner but that would be tolerable.
17:48:23 <dondelelcaro> I can draft the override, and just shove it into git for the time being
17:48:33 <dondelelcaro> then we can move on it rapidly if necessary
17:48:34 <bdale> dondelelcaro: yes, please do
17:48:35 <Diziet> And formally propose it in a week or so ?
17:48:35 <keithp> I agree as well
17:48:41 <Diziet> dondelelcaro: That sounds great.
17:48:43 <cjwatson> Yep
17:48:49 <bdale> making it clear we're going to vote on this if it isn't resolved "naturally" is a good strategy
17:49:10 <keithp> and making it pretty darn clear what the vote is likely to be :-)
17:49:10 <dondelelcaro> #action dondelelcaro to draft override for #766708 in git; move forward formally in a week
17:49:25 <dondelelcaro> #topic #750135 Maintainer of aptitude package
17:49:31 <dondelelcaro> I think aba was going to head this issue up
17:50:05 <dondelelcaro> #action aba to propose a process to try out on #750135 Including the maintainer perhaps providing private response to us, and also perhaps an irc conversation
17:50:08 <dondelelcaro> that's what I have for it
17:50:16 <dondelelcaro> I don't know if anything has happened there
17:50:20 <Diziet> NAFAIAA
17:50:24 <rra> Yeah, at hte moment I have no data.
17:50:29 <bdale> ditto
17:50:32 <dondelelcaro> ok
17:50:44 <dondelelcaro> I'll try to ping aba about that later
17:50:44 <Diziet> Would someone else like to take this action item ?
17:51:26 <Diziet> ... tumbleweed ...
17:51:28 <dondelelcaro> heh
17:51:31 <dondelelcaro> #action dondelelcaro to ping aba about process for #750135
17:51:38 <dondelelcaro> I'll just ping aba about it, and see what happens
17:51:43 <Diziet> I guess that will do.
17:51:54 <dondelelcaro> #topic Additional Business
17:52:01 <Diziet> We skipped the constitutional stuff.
17:52:05 <dondelelcaro> oh, right
17:52:08 <dondelelcaro> let me go back to that
17:52:19 <dondelelcaro> #topic #636783 constitution: super-majority bug
17:52:22 <cjwatson> I think I have to go in 5
17:52:24 * rra has about three minutes and then has to go to a work meeting.
17:52:26 <dondelelcaro> ok
17:52:28 <Diziet> I have put that on hold.  If someone else wants to pick it up that's fine of course.  I don't propose to do anything about it until after the init system GR vote is concluded.
17:52:38 <rra> That sounds right to me.
17:52:38 <bdale> good
17:52:44 <dondelelcaro> OK
17:52:46 <Diziet> OK
17:52:47 <bdale> one GR at a time is enough
17:52:53 <dondelelcaro> #topic #636783 constitution: casting vote
17:52:54 <rra> One GR at a time is generally enough-- yeah.  :)
17:53:08 <dondelelcaro> should I go through these individually? Or?
17:53:08 <Diziet> That goes for all of these.
17:53:19 <dondelelcaro> #topic #636783 constitution: TC member retirement/rollover
17:53:23 <rra> AJ is pursuing the term limit thing.
17:53:24 <cjwatson> Oh, so we skipped the constitutional stuff, but it's fine for us to have done so.
17:53:27 <dondelelcaro> I think this is the only one which has some stuff
17:53:29 <Diziet> rra: Yes.
17:53:33 <bdale> Diziet: did you get to a concrete draft on these yet?  I've read things but forget the current status.
17:53:38 <Diziet> That's fine.  But I haven't read his proposal in detail.
17:53:40 <rra> I'm inclined to let AJ run with it if he wants to.
17:53:47 <Diziet> bdale: Most of them I think.
17:53:50 <bdale> ok
17:54:00 <Diziet> rra: I certainly don't feel I have standing to stop him but I wish he'd wait a bit.
17:54:10 <rra> Diziet: Well, I don't think he's ready to propose something.
17:54:23 <rra> I'd like to have a conversation about it when debian-vote is a bit less of a madhouse.
17:54:27 <Diziet> Yes.
17:54:31 <rra> Right now, the conversation would be lost.
17:54:58 <dondelelcaro> right
17:55:06 <dondelelcaro> anything else with this?
17:55:10 <rra> But my impression is that we've mostly hammered out the broad strokes and it's mostly about the details.
17:55:17 <Diziet> Yes.
17:55:23 <Diziet> #agreed Postpone constitutional questions until -vote less mad.
17:55:27 <rra> So I suspect we could resolve it relatively quickly when there's some quiet.
17:55:40 <dondelelcaro> #topic Additional Business
17:55:48 <bdale> rra: I think so
17:56:21 <Diziet> AOB> None from me.
17:56:31 <rra> I have nothing further, and actually need to run.
17:56:47 <Diziet> I have to go, too.
17:56:49 <Diziet> Goodnight all.
17:56:53 <dondelelcaro> nite
17:56:55 <dondelelcaro> #endmeeting