16:58:52 <dondelelcaro> #startmeeting
16:58:52 <MeetBot> Meeting started Thu Oct 25 16:58:52 2012 UTC.  The chair is dondelelcaro. Information about MeetBot at http://wiki.debian.org/MeetBot.
16:58:52 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
16:58:57 <dondelelcaro> #topic Who is here?
16:59:11 <dondelelcaro> Don Armstrong
16:59:14 <cjwatson> Colin Watson
17:01:04 <dondelelcaro> Diziet, vorlon, bdale, aba: ping
17:02:07 <dondelelcaro> rra (Russ Allbery) sent his regrets
17:02:34 <Diziet> Hi.
17:02:43 <Diziet> Just catching up on the scrool.
17:02:51 <dondelelcaro> no worries
17:03:11 <dondelelcaro> (for the record, what's in the scrollback is 07:44:59  * buxy would like to suggest tech-ctte members to have a look at http://paste.debian.net/203534/)
17:03:26 <dondelelcaro> and commentary about that
17:03:40 <Diziet> I just wanted to say I object to being described as biased.  When this whole n-m thing started I didn't have an opinion.  I have formed that opinion through discussion, consideration, etc., and now I have one.  That's not bias, that's how things are supposed to work.
17:04:37 <bdale> I'm here
17:04:53 <bdale> Bdale Garbee
17:04:58 * vorlon waves
17:05:02 <Diziet> I'm Ian Jackson.
17:05:06 <dondelelcaro> ah, awesome. Only aba is missing
17:05:31 <dondelelcaro> lets get started
17:05:41 <dondelelcaro> #topic Next Meeting?
17:05:53 <dondelelcaro> #chair Diziet bdale vorlon cjwatson
17:05:53 <MeetBot> Current chairs: Diziet bdale cjwatson dondelelcaro vorlon
17:05:56 <bdale> fwiw, I just had a very productive private chat with jordi on IRC and he will follow up with email articulating the answer to Don's question that's been pending about what the *technical* motivations are for wanting n-m always present with gnome3
17:06:11 <dondelelcaro> awesome
17:06:36 <bdale> wrt next meeting, I'm fairly flexible .. what's the default?
17:06:50 <dondelelcaro> it's currently scheduled for the 29th at the same UTC time
17:06:51 <cjwatson> 22 Nov, I think
17:06:54 <cjwatson> Oh, 29, OK
17:07:00 <bdale> checking
17:07:11 <dondelelcaro> however, I think all of us are switching to standard time
17:07:18 <bdale> I'll be at FOSScon Korea
17:07:19 <Diziet> Are we going to bring it forward due to DST ending ?
17:07:26 <dondelelcaro> Diziet: yeah, that was my question
17:07:31 <Diziet> I would prefer to.
17:07:34 <bdale> may or may not be able to participate on th 29th
17:07:51 <dondelelcaro> Diziet: I'd prefer to also
17:07:56 <cjwatson> I would prefer to adjust the UTC time for DST as well, but I can make either time that day
17:07:57 <dondelelcaro> bdale: would the 22nd work better?
17:08:02 <bdale> I'm relaxed about time of day, whatever works for you guys
17:08:02 <vorlon> 22 would be Thanksgiving, that's probably not good for those of us in the US?
17:08:08 <dondelelcaro> oh, yeah
17:08:12 <bdale> the 22nd is Thanksgiving
17:08:17 <dondelelcaro> forgot about that. ;-)
17:08:18 <bdale> that would not be good
17:08:21 * dondelelcaro will be busy then assuredly
17:08:25 <bdale> go for the 29th, I'll be on from Seoul if I can
17:08:28 <dondelelcaro> ok
17:08:33 <dondelelcaro> so the 29th at 18:00 UTC?
17:08:45 <vorlon> sounds fine to me
17:08:50 <Diziet> date -d @1353607200
17:09:16 <bdale> fine .. dondelelcaro, I assume you'll update the calendar?
17:09:18 <dondelelcaro> #agreed next meeting november 29th at 18:00 UTC 'date -d @1353607200'
17:09:21 <dondelelcaro> bdale: yeah
17:09:24 <bdale> thanks
17:09:27 <bdale> that's working fine, btw
17:09:29 <dondelelcaro> #action dondelelcaro to update the calendar
17:09:34 <dondelelcaro> #topic #636783 super-majority conflict;
17:09:58 <Diziet> I have done nothing about this.  My energy has been sucked by the n-m thing TBH
17:10:12 <dondelelcaro> ok
17:10:21 <dondelelcaro> anything else on this?
17:10:24 <Diziet> I do wonder whether given the current heat surrounding the n-m thing it would be better to wait for perhaps for someone else to push it.
17:10:25 <bdale> not from me
17:10:40 <Diziet> But we can leave it for another month...
17:10:42 <bdale> I don't feel any extreme urgency, waiting is ok with me
17:10:47 <Diziet> OK
17:10:49 <dondelelcaro> waiting is fine too
17:11:10 <bdale> let's move on, then
17:11:11 <dondelelcaro> #agreed wait on #636783 (super majority) for another month
17:11:12 <dondelelcaro> #topic #681419 Depends: foo | foo-nonfree
17:11:14 <cjwatson> I'm afraid I only got round to this today, but I sent a strawman draft.  I'd appreciate feedback.
17:11:21 <Diziet> cjwatson: I will send feedback.
17:11:24 <bdale> I read it, seems ok to me, I favor A
17:11:33 <cjwatson> Especially from those whose opinions I may have mischaracterised :-)
17:11:40 <Diziet> I guess the right thing is probably for me to make my arguments, which I hope will convince you but frankly I'm not that optimistic.
17:11:49 <Diziet> And I will rewrite your B.  Thanks for the work btw.
17:11:55 <Diziet> It's a very good starting point.
17:11:57 <cjwatson> I found myself arguing myself into both positions while writing that draft.
17:11:58 <bdale> sounds good
17:12:04 <Diziet> cjwatson: Heh.
17:12:05 <cjwatson> Which is probably the right place to start in a useful debate.
17:12:08 <dondelelcaro> yeah
17:12:08 <bdale> cjwatson: that's good, actually
17:12:34 <Diziet> #action Diziet to respond to cjwatson's foo|non-free draft
17:12:35 <bdale> I, too, could go either way but over history have accumulated a strong sense that | non-free-variant is ok
17:12:57 <cjwatson> I'm probably about 60/40 at this point.  Happy to wait for Diziet's redraft.
17:13:02 <bdale> me too
17:13:13 <dondelelcaro> cool. should we move on?
17:13:16 <bdale> yes
17:13:16 <Diziet> t
17:13:22 <dondelelcaro> #topic #688772 gnome Depends on network-manager-gnome
17:13:36 <bdale> as I mentioned a bit ago, jordi will be sending us email
17:13:41 <Diziet> I guess this is going to go on for another month then :-(
17:13:47 <bdale> I hope not
17:13:51 <dondelelcaro> hopefully not
17:13:55 <bdale> I'd like to think we can come to closure fairly quickly
17:14:00 <Diziet> That's what you guys all said last month.
17:14:19 <Diziet> And frankly it should have been done after we voted on it the first time.
17:14:36 <Diziet> But if you don't agree I can't make you.
17:15:08 <dondelelcaro> I've got the two options, one of which I've dropped
17:15:11 <vorlon> I'm not thrilled about the timeline, but I also don't think forcing it to a vote helps us make good decisions
17:15:35 <dondelelcaro> I'd also like to at least consider having the status quo as an explicit option
17:15:45 <cjwatson> Mediation with jordi is helpful.  Thank you.  I'm concerned that the "refuse to implement TC decision on pretext that we are acting beyond our mandate" thing might spread.
17:15:51 <Diziet> I think we should take the view that people who don't send us good reasons don't have good reasons, rather than trying to invent reasons and/or delay for months waiting for them to turn up.
17:16:05 <dondelelcaro> (NM Depends ok, with release notes)
17:16:13 <bdale> to be honest, based on what I read in the pastebin from buxy's conversation *and* my own chat with jordi this morning, I now understand much better why the gnome guys really really want n-m to be installed as part of gnome3.  I remain unconvinced that this requires a depends instead of a recommends, though.
17:16:51 <Diziet> OK well I'll look forward to seeing these reasons then.
17:16:59 <vorlon> Diziet: so even if I accept that they don't have good reasons, I haven't convinced myself that in the case of the gnome metapackage (as distinct from the gnome-core one) that this is worth a maintainer override
17:17:00 <cjwatson> The point about lenny not installing Recommends by default is a useful one to have explicitly made, which was not previously one I had seen put forward
17:17:01 <dondelelcaro> bdale: sounds like it would be useful to explicitly
17:17:26 <dondelelcaro> bdale: sounds like it would be useful to explicitly acknowledge those reasons in the decision text too
17:17:28 <bdale> cjwatson: I'm actually unswayed by the lenny->squeeze->wheezy upgrade thing
17:17:29 <cjwatson> (Because it impinges directly on the question of whether this is user choice)
17:17:39 <vorlon> cjwatson: I'm pretty sure that's wrong, timeline-wise
17:17:50 <cjwatson> Then I'd be happy to see a rebuttal :-)
17:17:55 <cjwatson> My own memory is a little fuzzy here
17:17:56 <vorlon> lenny's apt may not have installed *new* Recommends by default, but I'm fairly certain it did install recommends
17:18:04 <bdale> dondelelcaro: yes, I think so.  it turns out many gnome'ish apps now change behavior based on knowledge of the networking status that only n-m is currently communicating
17:18:05 <vorlon> i.e., upgrade vs. install
17:18:31 <cjwatson> (And I do take seriously support for Debian users who have upgraded over many releases)
17:18:49 <bdale> the problem we are going to be left with is the difficulty of distinguishing between the two reasons someone might have removed n-m
17:18:56 <Diziet> So we would be talking about people who installed etch?
17:19:01 <dondelelcaro> right
17:19:23 <dondelelcaro> and who have been without NM during the entire timeframe
17:19:43 <vorlon> * apt-pkg/depcache.cc:
17:19:44 <vorlon> - set "APT::Install-Recommends" to true by default (OMG!)
17:19:44 <Diziet> If we take those people and they get n-m added during the upgrade, isn't their system going to break ?
17:19:53 <vorlon> -- Michael Vogt <mvo@debian.org>  Tue, 23 Oct 2007 14:58:03 +0200
17:20:14 <Diziet> vorlon: Early in lenny's development then.
17:20:58 <vorlon> yes; and according to the changelog, support for installing new recommends on upgrade was also merged earlier
17:21:11 <cjwatson> lenny shipped with, uh, 0.7.20.2, right?
17:21:48 <Diziet> installing new recommends on upgrade> So people who did a dist-upgrade from etch to lenny would have acquired n-m then.
17:22:03 <vorlon> Diziet: the etch->lenny upgrade would have been done with the etch version of apt, I think
17:22:08 <vorlon> so possibly not
17:22:09 <Diziet> Oh yes.
17:22:17 <Diziet> And when did this recommends: n-m turn up anyway ?
17:22:18 <vorlon> cjwatson: 0.7.20.2, yes
17:22:21 <Diziet> Has it been there forever ?
17:22:31 <Diziet> Maybe we should take this discussion to email.
17:22:40 <Maulkin> [I seem to remember we asked people to install apt first in the release notes, but I could be wrong.]
17:22:46 <cjwatson> The lenny release notes recommended upgrading apt first.
17:22:53 <vorlon> ok
17:23:19 <vorlon> so evidence suggests that users who don't have NM installed have made an explicit choice?
17:23:36 <cjwatson> (brb, phone)
17:23:37 <Diziet> vorlon: I think even with this discussion, mostly, yes.
17:23:52 <Diziet> Although we should check the case for people who installed etch or earlier.
17:24:05 <dondelelcaro> ok
17:24:27 <vorlon> well, since cjwatson says the release notes said to upgrade apt first, I'm satisfied that this is the case
17:24:52 <cjwatson> For those who followed them, anyway ..
17:24:56 <Diziet> Uh, just to be clear: that's installing new recommends.
17:24:57 <vorlon> if someone wants to try to make the case that users have ended up in that state without an explicit decision, let them demonstrate how it happened
17:25:02 <Diziet> OK.
17:25:15 <dondelelcaro> sounds good
17:25:31 <dondelelcaro> is there anything else specific on this part of the dicsussion?
17:25:36 <bdale> what about the gnome guys argument that n-m today isn't really the same thing as n-m then, just as gnome3 isn't much like gnome2?
17:25:48 <vorlon> so for me, the sticking point is this:
17:26:02 <bdale> this goes to the question of whether a choice in the lenny era is of particular interest today
17:26:13 <Diziet> bdale: The n-m refuseniks one sees complaining don't seem to be convinced.  Of course that's a self-selecting samplbe.
17:26:14 <vorlon> installing NM on upgrade could take over and break the existing network config
17:26:34 <bdale> right, breakage on upgrade is my biggest concern
17:26:49 <vorlon> that for me is the key difference from any other kind of upstream change that might be encapsulated in the metapackage
17:26:52 <Diziet> bdale: The question for me is: is it OK for the gnome maintainers (whose recommendation the user has already overridden) to be allowed to declare that the user's decision is no longer inapplicable ?
17:27:02 <Diziet> vorlon: Yes.
17:27:09 <vorlon> i.e., this falls into "Breaks the whole system" territory, not just "your desktop might be a mess after upgrade" territory
17:27:24 <vorlon> even users who just have gnome installed without using it are potentially affected
17:27:35 <dondelelcaro> right
17:27:38 <vorlon> is there a possibility of a compromise in *that* space?
17:27:39 <bdale> vorlon: good point
17:27:55 <vorlon> i.e.: figure out how to ensure NM doesn't break things when newly installed on upgrade
17:28:08 <vorlon> because if we solve that, we don't really care that the package is pulled in, right?
17:28:11 <dondelelcaro> vorlon: that's what I was trying to propose with the nm|wicd thing, but it was relatively roundly rejected
17:28:16 <bdale> there was discussion this morning about a new feature in n-m that would have it not break wicd
17:28:35 <bdale> <jordi> 18:09 <mbiebl> if nm is installed and it detects that the wicd binary is  installed
17:28:35 <bdale> <jordi> 18:10 <mbiebl> it pops up a dialog, explaining that running both at the same  time is usually not a good idea
17:28:35 <bdale> <jordi> 18:10 <mbiebl> and if NM should be disabled
17:28:35 <bdale> <jordi> 18:10 <mbiebl> i.e. it would run update-rc.d nm disable
17:28:36 <vorlon> dondelelcaro: right, except that only helps users who already have wicd installed
17:28:40 <dondelelcaro> vorlon: right
17:28:56 <Diziet> bdale: That's a band-aid.  The real answer is just that users who choose not to have n-m should ... not have n-m !
17:29:21 <Diziet> And _that_ is the real sticking point because apparently the maintainers _want_ users to have n-m even when the users want the opposite.
17:29:23 <vorlon> Diziet: I think that's over-specifying
17:30:00 <Diziet> vorlon: Installing a package and somehow disabling it is always going to be technically inferior to not installing it, if the latter is feasible.
17:30:13 <dondelelcaro> vorlon: I just think that the cases where people don't have wicd or nm tend to be either totally basic configurations, in which case NM shouldn't break anything, or super-complicated configurations, where the user probably isn't running gnome anyway
17:30:16 <Diziet> And again this is a decison the user has made.
17:30:25 <dondelelcaro> vorlon: but that's probably my own personal biases speaking
17:30:40 <vorlon> Diziet: if I have a package that recommends: libfoo3 for plugins/foo.so; then in a later version the functionality of foo.so is merged into the main binary, or the maintainer decides that foo.so is important enough that its dependencies should be Depends instead of Recommends; should that be disallowed?
17:31:08 <Diziet> vorlon: Well that depends if libfoo3 suddenly starts to possibly break your system!
17:31:25 <vorlon> Diziet: right, which is why I think we should focus on solving "break your system" instead of "pulled in"
17:31:38 <Diziet> See also was it Russ's list of preconditions which apply to the n-m case but not to others.
17:31:49 <Diziet> vorlon: Fixing "pulled in" is trivial.
17:32:00 <Diziet> Fixing "breaks" is nontrivial and will not be perfect.
17:32:10 <vorlon> sure, it's trivial, all it requires is for us to override the maintainers
17:32:11 <vorlon> ;)
17:32:24 <Diziet> vorlon: Exactly.  All we have to do is point out that the maintainer is wrong
17:32:38 <Diziet> Fixing "breaks" will certainly involve actual config effort by the user.
17:32:42 <cjwatson> Back.  Sorry, Dad just got back from a long holiday and I could hardly tell him I was in the middle of a meeting :-)
17:32:44 <vorlon> why should it?
17:33:05 <dondelelcaro> Diziet: it shouldn't involve configuration work by the user. If it does, it's still broken.
17:33:06 <vorlon> I consider it the responsibility of the NM maintainers to make sure that package doesn't break the existing system when installed, REGARDLESS of whether it's pulled in automatically
17:33:07 <Diziet> vorlon: Because the idea seems to be to tell people to disable n-m in the release notes.
17:33:12 <vorlon> that's one idea
17:33:17 <vorlon> that's not an idea I would sign off on
17:33:40 <Diziet> Is anyone suggesting n-m would automatically spot all possible situations where it's not wanted (ie, all situations where it wasn't installed before) and disable itself ?
17:33:41 <vorlon> I consider it a serious bug if NM ever takes over a network stack that it doesn't know how to handle
17:33:43 <bdale> vorlon: I agree that n-m breaking existing network config is just a bug
17:33:48 <cjwatson> Does anyone know if anyone has considered other approaches to encouraging users to give NM another try, such as nag notifications in software that could make good use of it?
17:34:16 <vorlon> cjwatson: I don't think the GNOME team would want to do the work to implement that
17:34:23 <cjwatson> Perhaps not.
17:34:28 <bdale> cjwatson: buxy suggested something like that on irc, I don't believe there's any code to do that
17:34:35 <Diziet> vorlon: I agree with you that those things are bugs.  But software will always have bugs.  This is why we don't install every package on every user's computer.  (Well bandwidth too but.)
17:34:42 <cjwatson> Nevertheless I can hope for compromises ...
17:34:50 <dondelelcaro> cjwatson: I'd sort of suggest shoving that into the release notes
17:35:05 <Diziet> Such an imprecation could be put in the release notes.
17:35:14 <cjwatson> Certainly another possibility.
17:35:34 <vorlon> Diziet: so my underlying motivation in suggesting this plan of attack is that it's one the GNOME team will actually be motivated to enact because it aligns with their own goals (have NM installation be a smooth experience that doesn't get them hate mail)
17:36:26 <vorlon> instead of one that makes more work for them (carrying a delta from the upstream experience) without contributing to the quality of the preferred configuration
17:36:33 <vorlon> ("preferred" from their perspective)
17:36:43 <Diziet> vorlon: delta> They have to carry that anyway because of bsd etc.
17:36:46 <bdale> so one thing I "learned" this morning is that n-m is now being seen as more than just a way to get a working network interface, ala ifup/ifdown
17:37:23 <bdale> and that the fact that it uniquely provides information about the class of network connection that other gnome'ish apps can differentiate their behavior based on is considered a significant feature
17:37:36 <vorlon> Diziet: except they would otherwise only really have to have that delta in experience on the non-Linux ports - which GNOME upstream has effectively written off entirely
17:37:41 <cjwatson> vorlon said "upstream experience" (to which I would add [on Linux]), not "upstream code"
17:38:06 <bdale> this leads me to be more inclined to be sympathetic to the notion that a gnome3 experience isn't complete without n-m
17:38:17 <Diziet> Well in terms of the experience they already have n-m which means all users who haven't explicitly decided otherwise get precisely what the gnome people work.
17:38:20 <Diziet> s/work/want/
17:38:30 <bdale> I remain completely unconvinced that a depends is better than a recommends, though
17:38:31 <Diziet> They already have n-m by default I mean.
17:38:40 <Diziet> bdale: And that is precisely the point at issue.
17:38:50 <Diziet> All that is needed is to allow the users to choose.;
17:39:14 <cjwatson> bdale: So I definitely agree that this is a useful thing for some users, and an excellent subject for a Recommendation
17:39:27 <Diziet> No-one is objecting to the Recommends AFAIAA
17:39:37 <Diziet> It's the Depends that is troublesome.
17:39:56 <cjwatson> (I suspect you might find that many of the users who chose to disable NM really don't give a fig about empathy knowing whether they're online, but maybe this is anecdotal evidence talking)
17:40:00 <vorlon> the user still has the opportunity to choose, they can run 'sudo service network-manager disable'
17:40:01 <Diziet> Because it removes the user's choice; they can go along with it, or they can try to assemble the package list themselves.
17:40:13 <vorlon> so it's not that they don't have a choice
17:40:19 <vorlon> it's that they have to reaffirm their previous choice
17:40:25 <vorlon> (in a different manner)
17:40:44 <Diziet> vorlon: during the middle of an upgrade when their networking has maybe been broken.
17:41:03 <Diziet> We don't in any other situation ask people to reaffirm their previous choices, at least not unless we can't help it.
17:41:22 <vorlon> Diziet: so you're unwilling to even posit that the GNOME maintainers might manage to prepare an NM package that is free of critical install-time bugs?
17:41:25 <Diziet> We have indeed gone to some lengths in many parts of the system to preserve users' choices.
17:41:59 <Diziet> vorlon: Completely free for something as complex as n-m seems unlikely.  Indeed I pointed out one rare scenario where n-m's very purpose is inimical.
17:42:09 <Diziet> Those bugs might be rare.
17:42:15 <cjwatson> That was the "unmanaged interface" scenario?
17:42:16 <vorlon> the argument I see for asking them to reaffirm the choice is that the situation on the ground has changed, as mentioned - the NM we have now is not the terribly broken software that the user opted out of before
17:42:19 <Diziet> cjwatson: Yes.
17:42:26 <cjwatson> buxy did point out that "manual" is available for that, but of course you have to know about it.
17:42:39 <Diziet> cjwatson: Don't you have to list all the interfaces in advance ?
17:42:48 <Diziet> I mean all the manual ones.  What if you don't know what it's going to be called ?
17:42:59 <cjwatson> Oh, if you have an arbitrarily large / unknown set of interface names, yes.
17:43:18 <Diziet> cjwatson: That's what real hardware does if you (say) plug a random series of usb network dongles into it.
17:43:23 <vorlon> Diziet: so I guess I find it disappointing that we're having this *particular* conversation then; if we really think NM can't be made to not break the network on install, I would consider *that* a release-critical bug on the NM package
17:43:28 <Diziet> Along with 75-persistent-names wossname.
17:43:44 <vorlon> which we should file, and then the gnome maintainers can make their own decision about whether to let that bug keep all of gnome out of the release
17:44:00 <Diziet> vorlon: Surely there could be bugs which are rare enough that they aren't RC but occur and are relevant ?
17:44:12 <Diziet> vorlon: And you seem to assume we can find them all before release.
17:44:17 <Diziet> Which seems very unlikely.
17:44:28 <Diziet> We don't manage to find all the bugs in anything else before release...
17:44:35 <Diziet> Even quite bad bugs.
17:44:46 <vorlon> I assume that any such bug that we can't find before release, or fix in short order afterwards, isn't worth a maintainer override for
17:44:48 <bdale> yet we do release
17:45:14 <vorlon> but I do think any such bug that a user encounters should be treated as being of the highest severity
17:45:20 <cjwatson> If it would be helpful (for the tone of the conversation, if nothing else), I'd be willing to draft some kind of exhortatory text for the release notes saying that NM is fabulous and users should consider re-enabling it if they disabled it before
17:45:30 <Diziet> vorlon: I think the real difficulty here is what you say about "worth a maintainer override".  It seems to me that you are saying you agree with my view and think the maintainer is doing a worse thing than they should do, but you're not going to stop them.
17:45:35 <cjwatson> Or words to that effect
17:45:50 <jordi> I wonder if we'll be able to find all bugs in the 3.2 kernel that might get the network interface not work at all after the post upgrade reboot
17:45:53 <vorlon> Diziet: mmm, not exactly
17:46:17 <ansgar> Diziet: So override the GNOME maintainers because you are not sure all bugs were found?
17:46:20 <Diziet> vorlon: To an extent I think you are allowing yourself to be influenced by the maintainers' histrionics.  If the maintainer were calm and reasonable and it was obvious that whatever we decided they would implement without ranting and raving, would you feel the same way ?
17:46:36 <cjwatson> Because I use NM pretty much everywhere, find it exceptionally useful, and yet I am taking a position against upgrading a Recommends on it to Depends and am thus branded as an NM-hater
17:46:39 <vorlon> Diziet: I'm saying that speculation without evidence that NM might break the network on upgrade does not persuade me that the maintainer should not be allowed to pull the package back in on upgrade
17:46:58 <bdale> Diziet: you're not exactly coming across as calm and reasonable on this, either, fwiw
17:47:03 <vorlon> Diziet: to the extent that we have *concrete* examples of breakage on upgrade when pulling it in, we should hold their feet to the fire
17:47:19 <cjwatson> ansgar: Of course, even the RC bugs open on it at the moment (some for extended periods of time) ought to be fairly concerning
17:47:29 <vorlon> but if we don't have those, then I have no quarrel with the GNOME maintainers for pulling in the software on upgrade
17:47:50 <Diziet> vorlon: Even for users who have deliberately decided to remove it ?
17:48:07 <bdale> yes, #642136 is a huge problem (correctly at an RC severity) even if one isn't using gnome
17:48:08 <Diziet> vorlon: So you're saying you're happy to overide the users (thousands of them according even to popcon!) but not the maintainer.
17:48:14 <vorlon> Diziet: yes, for previously stated reasons (the software they decided to remove was a significantly worse piece of software)
17:48:20 <vorlon> Diziet: I override users all the time
17:48:21 <vorlon> sorry
17:49:04 <dondelelcaro> I think we should take this part of the discussion to e-mail if necessary; right now it seems like we have at least the technical rationale that bdale is providing, coupled with a need for release notes
17:49:26 <dondelelcaro> we also have the specific cases of breakage in NM, which, if they exist may need additional bugs filed.
17:49:27 <vorlon> part of the remit of the maintainer is to make opinionated decisions about what users really need vs. what they think they want
17:49:39 * bdale hopes jordi will be able to find time to follow-through on posting some technical details in email
17:49:50 <vorlon> and sometimes trading off one set of user requirements against another incompatible set
17:51:02 <jordi> bdale: we're working on it, but what I've been reading here really puts me off a bit; I don't think any soft words will help this case, despite my best efforts to bring a third path.
17:51:37 <cjwatson> Well, it depends on what your goals are.  The committee has seven members with a range of opinions ...
17:51:46 <cjwatson> (As evident from the extended discussion)
17:51:49 <Diziet> vorlon: Well fundamentally I think it's one thing to be opinionated in that way and another to play core wars against the determined user on their own system.  I didn't join the Free Software movement so that people could have computers which did what their vendors said rather than what the users said.
17:52:05 <cjwatson> And one thing that is definitely not going to *hurt* is more technical details
17:52:34 <Diziet> jordi: You don't need to convince me, although if you come up with the right arguments you might.  You only need to convince one or two of my colleagues.
17:53:02 <cjwatson> Who knows, we might even manage to net improve wheezy. :-P
17:53:07 <dondelelcaro> heh
17:53:42 <dondelelcaro> #action jordi to provide more technical detail on the NM case
17:53:55 <dondelelcaro> #action dondelelcaro to add in bits on possible release text
17:54:05 <dondelelcaro> #action dondelelcaro to draft status quo option
17:54:10 <dondelelcaro> ok, we should move on now
17:54:16 <dondelelcaro> #topic #685795 New ctte member
17:54:36 <vorlon> the nomination window has closed, yes?
17:54:37 <dondelelcaro> I believe we were still waiting on the confirmation of everyone to serve
17:54:40 <dondelelcaro> yeah, nominations are closed
17:54:50 <dondelelcaro> it's most definitely no longer early october
17:54:54 <vorlon> "the confirmation" - who had that action?
17:54:59 <dondelelcaro> bdale, I believe
17:55:04 <bdale> right, action to me to get that done
17:55:11 <Diziet> Can someone with a good email client volunteer to send me the -private backlog again ?  My email config which I thought I had fixed was broken.  Sorry :-(
17:55:15 <cjwatson> Sorry, I've been pretty terrible at following up with opinions.  I'll try to put that together during stray moments at next week's conference ...
17:55:22 <Diziet> Ideally as a mime digest.
17:55:28 <dondelelcaro> #action bdale to confirm with nominees that they are ok to serve
17:55:42 <bdale> I'll try to have that done by next Monday
17:55:43 <dondelelcaro> #action dondelelcaro to send Diziet -private backlog
17:55:48 <Diziet> dondelelcaro: Thanks.
17:56:03 <dondelelcaro> I drafted the announcement e-mail too, but anyone who wants, feel free to modify it in git
17:56:08 <bdale> won't bore you all with the details, but my time has not been my own this week so far
17:56:14 <dondelelcaro> no worries
17:56:53 <dondelelcaro> ok. anything more on that?
17:56:58 <bdale> I don't think so
17:57:01 <dondelelcaro> #topic Additional Business
17:57:07 <dondelelcaro> anything additional to discuss?
17:57:13 <Diziet> Not from me.
17:57:22 <vorlon> anyone want to tackle the package salvaging discussion? :)
17:57:31 <bdale> whazzat?
17:57:32 <Diziet> Is that on our plate ?  I do hope not...
17:57:33 <vorlon> that may not be a 3-minute topic
17:57:37 <dondelelcaro> yeah
17:57:40 <vorlon> bdale: thread on debian-devel
17:57:45 <bdale> will read
17:57:46 <dondelelcaro> I think the no-nack is basically the right process
17:58:07 <dondelelcaro> and anything else can be brought to the TC if it has to be
17:58:28 <vorlon> Diziet: well, we might want to consider a vote on whatever process proposal comes out of it, since authority to decide maintainership derives from the TC
17:58:28 <dondelelcaro> but yeah, probably a lot longer conversation
17:58:49 <Diziet> vorlon: I guess the Dev Ref is theoretically perhaps within our remit too.
17:59:02 <vorlon> or alternatively, maybe we should write our own process proposal that we agree with and vote on that instead, since otherwise I'm not sure I see a consensus emerging from that thread any time soon
17:59:15 <Diziet> But really I would like to do it by consensus and eventually a DPL decision or something since it seems more like a management question.
17:59:33 <dondelelcaro> vorlon: since you've done most of the work talking in it, maybe you could draft one? ;-)
17:59:56 <vorlon> dondelelcaro: would like to know if my position actually represents rough TC consensus before I spent the time on drafting :)
17:59:59 <cjwatson> This seems like a case where individual TC members should weigh in on their own account rather than the TC drafting "our" proposal
18:00:01 <dondelelcaro> vorlon: true
18:00:03 <cjwatson> To me, anyway
18:00:19 <dondelelcaro> cjwatson: yeah, probably a good idea. especially since we don't actually have to be involved
18:00:41 <Diziet> Yes.
18:00:52 <vorlon> having more TC voices in the thread would be nice
18:01:16 <vorlon> what I /don't/ want is to get into an unproductive email pattern with some of the participants in that thread
18:01:21 <vorlon> (which I feel I'm on the edge of)
18:01:33 <dondelelcaro> sounds reasonable
18:01:37 <dondelelcaro> should we stop here?
18:01:40 <Diziet> Yes.
18:01:45 <vorlon> sounds good
18:01:50 <dondelelcaro> #endmeeting