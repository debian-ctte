16:55:38 <dondelelcaro> #startmeeting
16:55:38 <MeetBot> Meeting started Thu Aug 30 16:55:38 2012 UTC.  The chair is dondelelcaro. Information about MeetBot at http://wiki.debian.org/MeetBot.
16:55:38 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
16:55:43 <dondelelcaro> #topic Who is here?
16:55:47 * rra is here.
16:55:48 <dondelelcaro> Don Armstrong
16:55:51 <aba> Andi Barth
16:55:52 <rra> Russ Allbery.
16:56:01 <Diziet> Ian Jackson
16:56:07 <dondelelcaro> cjwatson, vorlon: ping
16:56:29 <ChrisKnadle> Chris
16:57:29 <dondelelcaro> I guess we'll just get started; hopefully vorlon and cjwatson will show up in a bit
16:57:42 <Diziet> Yes.
16:57:59 <dondelelcaro> bdale is out flying rockets
16:58:00 <dondelelcaro> #topic Next Meeting?
16:58:38 <dondelelcaro> I believe the next appropriate slot is the 27th of september at the same time; any objections?
16:58:39 <aba> the 27th doesn't sound so bad?
16:58:59 <rra> That should work here.
16:59:20 <Diziet> Is that before or after the clocks change in the US ?
16:59:25 <rra> Before.
16:59:28 <dondelelcaro> Diziet: before
16:59:33 <Diziet> OK, same here.
16:59:35 <Diziet> Good.
16:59:52 <rra> We don't change until I think early November.  (It used to be late October, but then I think we pushed it back.)
17:00:02 <dondelelcaro> ok; lets go with that and we can alter it if necessary via e-mail
17:00:27 <dondelelcaro> #agreed Next meeting date -d 'September 27 2012 17:00 UTC'
17:00:38 <dondelelcaro> #topic #682010 mumble/celt client/server compatibility [vote if you haven't]
17:00:55 <dondelelcaro> I think today is the last day to vote on that
17:01:14 <aba> who has? I think up to now only two AF votes?
17:01:26 <dondelelcaro> rra, Diziet, and myself have voted
17:01:38 <aba> rra is not in the bug I think?
17:01:49 <dondelelcaro> aba: yeah, he just sent it to the list
17:01:56 <rra> Oh, sorry.
17:02:03 * dondelelcaro didn't actually notice that
17:02:06 <rra> I kept getting bitten by the mail-followup-to.  Should be fixed now.
17:02:07 <Diziet> That's due to the m-f-t I guess but that's fixed now.
17:02:08 <aba> the list setup makes that easy
17:02:15 <Diziet> At least the Subject has the bug#
17:02:36 <rra> Gnus really, really likes honoring m-f-t.  I have to actually cut and paste to fix it; none of the normal reply commands will discard it.
17:02:47 <Diziet> Do we need to say anything more about #682010 ?
17:02:51 <dondelelcaro> I don't think so
17:03:03 <dondelelcaro> I'm going to skip over #topic #573745 python maintainer [vorlon to write up resolution]
17:03:10 <dondelelcaro> #topic #636783 super-majority conflict;
17:03:20 <dondelelcaro> are we still delaying this for a while?
17:03:32 <Diziet> I think our workload is probably low enough now that I should push on ahead.  What do people think ?
17:03:44 <dondelelcaro> I'm ok with pushing on
17:03:52 <aba> Diziet: thanks if you do that
17:04:15 <Diziet> #action Ian Jackson to press on with the constitutional amendments etc.
17:04:24 <dondelelcaro> #chair Diziet
17:04:24 <MeetBot> Current chairs: Diziet dondelelcaro
17:04:27 <dondelelcaro> #chair rra
17:04:27 <MeetBot> Current chairs: Diziet dondelelcaro rra
17:04:28 <rra> If you're good to push on, I think we have enough time to look at it, yes.
17:04:30 <dondelelcaro> #chair aba
17:04:30 <MeetBot> Current chairs: Diziet aba dondelelcaro rra
17:04:34 <Diziet> #action Ian Jackson to press on with the constitutional amendments etc.
17:04:43 * dondelelcaro assumes that'll work
17:04:44 <Diziet> Is that supposed to produce an ack ?
17:04:51 <dondelelcaro> Diziet: I don't know. I think it might just record it
17:04:58 <Diziet> Right, whatever.
17:05:21 <dondelelcaro> #topic #681419 Depends: foo | foo-nonfree
17:05:33 <Diziet> We haven't really discussed this since the last time.
17:06:04 <aba> so, the definition of main is that it is a closure.
17:06:07 <Diziet> I think it would help if someone from the "it's OK" camp wrote up the arguments on-list.
17:06:17 <aba> which I don't think is violated by that.
17:06:36 <rra> Where we seem to have left this is with a semi-consensus that alternative dependencies are okay as long as the non-free version never gets installed by default.  Is that a correct understanding?
17:06:49 <Diziet> If you don't count me in your semi-consensus.
17:06:52 <rra> Oh, right, there was some discussion of using virtual packages as well.
17:06:54 <Diziet> I don't think it's OK.
17:06:57 <Diziet> Yes.
17:06:58 <rra> I forgot about that angle of things.
17:07:01 <aba> Diziet: that's the semi, I'd assume
17:07:14 <rra> Yeah, it was, but also I was forgetting the alternative resolution via virtual packages.
17:07:25 <rra> I'm still kind of fond of the virtual package approach.
17:07:46 <rra> It feels conceptually cleaner somehow.  But I'm not sure that battle plan would survive contact with the actual packages in the archive.
17:07:48 <dondelelcaro> I'm personally ok with either approach so long as non-free doesn't get pulled in automatically
17:07:51 <Diziet> So afaict everyone agrees that dependencies that might be satisfied out of non-free or contrib are ok; but that that shouldn't happen by default.
17:07:58 <dondelelcaro> right
17:08:02 <rra> Yes.  that's a much better way of putting it.
17:08:17 <Diziet> The only remaining disagreement is whether it is OK to actually name a non-free package in Recommends or Depends.
17:08:20 <Diziet> (Or for that matter Suggests.)
17:08:23 <aba> .. and that packages need to be able to be installed with only packages in main
17:08:33 <dondelelcaro> aba: right
17:08:44 <Diziet> aba: That's a weaker condition than that they are satisfied _by default_ only by packages in main.
17:09:00 <rra> Naming them in Suggests is quite widespread.
17:09:07 <Diziet> rra: Yes, I know.
17:09:08 <rra> That's another can of worms if we don't agree with that.
17:09:09 <aba> Diziet: that's why I put "and" in there.
17:09:19 <Diziet> aba: (X && y
17:09:23 <Diziet> aba: (X && Y) && Y   ?
17:09:41 <Diziet> Anyway, fine, if it makes you happy to clarify that.
17:10:19 <aba> Diziet: I don't think we need to explicitly name it in the resolution because I don't see anyone discussing it
17:10:33 <rra> The approach of asking for virtual packages will mean more change, since that's definitely contrary to existing practice.  that's the main drawback that I can see, provided that it actually works.
17:10:44 <Diziet> I really would like to see us avoiding "pushing" non-free stuff in Suggests too.  I know this may seem a bit overzealous, but in the context of a Suggests field there's not really any space to qualify things.
17:10:45 <rra> And I don't see any reason off-hand why it wouldn't work.
17:10:52 <aba> rra: and one can't provide on "from version X on only" with virtual
17:10:58 <aba> s,provide,depend
17:11:10 <rra> aba: True, but we did a survey and that facility isn't currently used.  But yes, someone may want it later.
17:11:16 <Diziet> aba: That can be done with a bit more faff but it's not really relevant because we checked the archive and there are hardly any of those.
17:11:22 <rra> You can sort of fake it by putting an ABI number in the virtual package.
17:11:26 <Diziet> As rra says.
17:11:52 <Diziet> qualify things> I mean, by putting in the kind of warning or deprecatory statements we would put in documentation or debconf prompts or whatever.
17:12:25 <rra> What I usually do is use the package long description to explain any Recommends or Suggests.
17:12:26 <Diziet> And I think it should be possible for Debian to satisfy a reasonable interpretation of the requirement not to promote non-free stuff.
17:12:33 <dondelelcaro> #agree dependencies that might be satisfied out of non-free or contrib are ok; but that that shouldn't happen by default nd that packages need to be able to be installed with only packages in main
17:12:35 <Diziet> rra: Yes, but.
17:12:53 <aba> Would it work ok for the ctte to just say "dependencies that might be satisfied out of non-free or contrib are ok; but that that shouldn't happen by default", and not explicitly talk about virtual packages etc? or should that be part of the ruling?
17:13:17 <rra> If we just want to show the users, when we present a Suggests from non-free, that this is from non-free and not part of Debian, that much could actually in theory be automated by the software we have that presences package metadata.
17:13:26 <rra> I mean, aptitude knows perfectly well that package is in non-free, or could.
17:13:29 <dondelelcaro> aba: this was a policy bug that got kicked to us, so we probably should show a recommended method of action to make that happen
17:13:32 <rra> But I'm probably over-complicating this.
17:13:38 <vorlon> dondelelcaro: cjwatson is in the wilds of Scotland today fwiw, and I'm apparently not managing to keep track of the schedule without calendars reminding me, sorry
17:13:40 <Diziet> Given that we've been asked to rule on this - and also Stefano's efforts on FSF collaboration - I think we do need to think about whether to insist on virtual packages.
17:13:59 <aba> dondelelcaro: k. but I'd prefer to make that not as part of the required behaviour to use virtual packages.
17:14:00 <dondelelcaro> rra: yeah, there was an Automatic: avoid in Release discussed too with http://lists.debian.org/msgid-search/20120717083004.GB21400@frosties
17:14:21 <dondelelcaro> #chair vorlon
17:14:21 <MeetBot> Current chairs: Diziet aba dondelelcaro rra vorlon
17:14:24 <dondelelcaro> vorlon: no worries
17:14:39 <rra> dondelelcaro: Oh, that's an interesting idea.
17:14:48 <rra> I missed that message.
17:15:16 <dondelelcaro> but I don't think that's supported with the tools yet...
17:15:24 <rra> No, indeed.
17:15:29 <aba> it sounds like a good idea, but not supported yet.
17:15:35 <aba> so not really yet for policy
17:15:42 <rra> aba: Yes, I'd kind of like to let Policy provide some concrete advice to package maintainers about what they should do.
17:15:43 <Diziet> I'm afraid I don't think extra automation of that kind gets rid of the problem.  I think it's sophistry to claim that when we write "Recommends:" in our metadata we don't mean "recommends".
17:15:55 <rra> Right now, everyone gets completely confused by what Policy says and different people interpret the same words differently.
17:16:08 <dondelelcaro> would it be ok if we "should" the non-default install of packages, and "should" the virtual package method?
17:16:19 <dondelelcaro> err, would it be ok if we "must" the non-default install of packages, and "should" the virtual package method?
17:16:28 <Diziet> rra: Barring the question we don't agree on, would what we have tentatively agreed so far suffice ?
17:16:32 <rra> That would work for me, but I'm not sure if that works for Ian.
17:16:35 <Diziet> I mean, suffice to clarify things.
17:16:52 <rra> Yes, the statement that the packages must not be pulled in by default is already a lot more clarity than we have.
17:17:10 <rra> The remaining question is just whether they can be listed as alternatives or whether one is required to use a virtual package.
17:17:17 <rra> I think we're agreed on the rest of it.
17:17:34 <aba> I don't think we should *require* virtual packages. We might suggest doing so however.
17:17:48 <Diziet> Another option would be to point out that the virtual packages thing is a political question as much as a technical one and punt on it, or make a tentative decision and request feedback, or ask leader@ or something.
17:17:57 <Diziet> I would like to require virtual packages.
17:18:02 <rra> Personally, I'm happy to have the virtual package solution be preferred ("should" as dondelelcaro says).
17:18:21 <vorlon> I don't mind encouraging virtual packages; I don't want to constrain maintainers by requiring them
17:18:30 <aba> vorlon: ack
17:18:31 <Diziet> I do definitely want to require them.  It's not a difficult constraint.
17:18:50 <aba> ok, do we need two different options then to vote on?
17:18:51 <Diziet> I don't think that making non-free second-class is in any way a problem.
17:18:55 <vorlon> Diziet: but it is a constraint, and I don't agree with your position that Recommends is a recommendation and therefore non-free must not be listed
17:19:44 <vorlon> so from my perspective it's an unnecessary constraint, and we shouldn't over-specify
17:19:45 <Diziet> If we go to a vote on this and I am defeated, I think Stefano might find that he wants to overrule us with a GR from an FSF collaboration POV
17:20:05 <Diziet> I would certainly sign up as sponsor to such a GR
17:20:10 <rra> If we just said "Recommends: foo-nonfree", I agree that is a recommendation that people use foo-nonfree.  When we say "Recommends: foo | foo-nonfree", I really don't agree that this is a recommendation that people use foo-nonfree.  It's a recommendation that people use some foo, and an acknowledgement that one of the foos is foo-nonfree.
17:20:39 <Diziet> foo | foo-nonfree is easily dealt with by  foo-nonfre -provides-> foo.
17:20:44 <rra> I do think a virtual package says that in a clearer and more comfortable fashion, though.
17:20:55 <Diziet> It's recommends: foo | bar   where bar is nonfree that's more of a problem.
17:21:03 <Diziet> It doesn't even mention the nonfreeness of bar in that case.
17:22:18 <dondelelcaro> in addition to the constraining of maintainers, I'm also concerned about making those packages instantly RC buggy when it was perfectly ok to do that, and satisfies the most important "pulls in by default"
17:22:18 <rra> Meh, the fact that we don't clearly show that bar is a non-free package in that case feels like a UI issue rather than a metadata issue to me.  I mean, we could require every package in non-free have "-nonfree" in the name, but that's just a hack solution to what's really a UI presentation issue.
17:22:49 <Diziet> UI issue> How do you propose to fix every UI anyone anywhere uses to display metadata for Debian or its derivatives ?
17:23:09 <vorlon> here's a further wrinkle:  the rules are that main is a closure, but package metadata can reference not only packages in non-free, but also packages that aren't in the archive at all
17:23:12 <Diziet> If you're going to say this is a ui issue I look forward to your patches for packages{,.qa}.d.o, apt, launchpad,....,...
17:23:15 <dondelelcaro> but I think we should move on, and maybe come back to this at the end if we have additional time; perhaps writing up a resolution with either option and then a more complete list discussion will be useful
17:23:23 <vorlon> what if foo-nonfree is a third-party package that provides the interface, which is non-redistributable and therefore not in non-free at all?
17:23:36 <vorlon> you don't get to modify foo-nonfree to add a Provides: to it
17:23:46 <rra> Diziet: I know, it's not an easy UI issue.  But I hate making decisions around technical metadata on the grounds that humans who read the metadata directly get the wrong political impression from it.  That just sort of bugs me.
17:23:47 <vorlon> so if you want it to satisfy the Recommends, you have to be able to name it explicitly
17:23:50 <Diziet> vorlon: I think that's fine _if it's a virtual package_ and if it's not then stuff them.
17:24:15 <vorlon> Diziet: what you're doing then is penalizing the user who /already has foo-nonfree installed/ by making them install another version of the package in addition
17:24:24 <Diziet> vorlon: I don't think we should allow ourselves to be bounced into naming non-free software done by people who are partially working against our values.
17:24:26 <vorlon> s/version of the package/package implementing the interface/
17:25:16 <Diziet> vorlon: But you're also saving the rest of the users from the bad memes that bar is a good thing.
17:25:17 <dondelelcaro> any objections to moving on?
17:25:30 <Diziet> We're not going to get agreement here, so please yes.
17:25:33 <dondelelcaro> ok
17:25:40 <Diziet> If you all want to outvote me you know how.
17:25:45 <dondelelcaro> we'll come back to this is there's time
17:25:48 <dondelelcaro> #topic #681634 gnome Depends on network-manager
17:26:20 <Diziet> I got by private email one suggestion for a minor wording amendment to my version of rra's text, which I haven't folded in or posted yet.
17:26:39 <rra> So, my remaining concern here is that I think Tollef made a really good point: currently, the *definition* of gnome-core is "all packages that the GNOME upstream considers core."  I think it's sort of unfortunate that we've confused that with "the packages required for core GNOME functionality," but still that's apparently the intended meaning.
17:26:56 <rra> I'm not horribly comfortable with telling the GNOME maintainers that they're not allowed to have a metapackage named gnome-core with that meaning.
17:26:58 <aba> is the bug# right?
17:27:21 <dondelelcaro> aba: no. sorry.
17:27:38 <Diziet> I think (a) that definition is served by Recommends: (b) we are perfectly at liberty to adjust that meaning to "packages that we consider ought to be considered core by GNOME upstream"
17:27:39 <dondelelcaro> #topic #681834 gnome Depends on network-manager
17:28:09 <rra> We can do that, but if I were a GNOME maintainer, it would bug me a lot to have the technical committee do that.
17:28:10 <Diziet> If you can't stomach my opinion (b) I still think (a) is almost irrefutable.
17:28:14 <vorlon> rra: so while that's true, the conflation is historical and the only way to unwind it in a way that doesn't impact users is to decide that gnome-core means "the packages we want users to get on upgrade when they had gnome-core installed previously"
17:28:24 <rra> It does bother me that no one else has stepped forward to maintain the "GNOME without network-manager" metapackage.
17:28:26 <Diziet> And what vorlon said.
17:28:39 <rra> vorlon: Yeah, I do agree that the upgrade behavior right now is awful.
17:29:00 <Diziet> rra: If we decreed that they were allowed the binary package name "gnome-core" in wheezy then I bet we'd have no shortage of volunteers.
17:29:09 <rra> I'm inclined to say that the upgrade behavior overrides, but it still makes me uncomfortable.
17:29:26 <rra> Yeah, using a different name doesn't fix the upgrade problem.
17:29:28 <dondelelcaro> is there any reason to not just do a very narrow decision about demoting network-manger to Recommends: so we can get this resolved quickly?
17:29:30 <Diziet> The reason we've not had volunteers for "gnome-core-reasonable" is because it doesn't fix their problem.
17:29:46 <rra> We *at least* need something in release notes for the upgrade issue, and I'd really rather not solve this particular problem only through documentation.
17:29:56 <Diziet> dondelelcaro: I don't think there is any reason but them I'm very gung-ho on this.
17:30:29 <dondelelcaro> FWICT, the main concern is that we're stepping on the gnome-maintainer's feet... but then, any time we override a maintainer, that's a problem
17:30:36 <Diziet> dondelelcaro: As you say.
17:30:41 <rra> dondelelcaro: If we demote network-manager to Recommends, I think the argument is that we're effectively, even though it's a narrow wording, overriding their decision about what "gnome-core" means.  Although I'm sympathetic to Diziet's counter that Recommends is still a strong enough relationship.
17:30:46 <rra> True.
17:31:03 <vorlon> rra: it makes me uncomfortable as well; is there something more we should do here to ensure the same problem doesn't happen to other maintainers in the future?
17:31:10 <vorlon> for instance, wheezy now installs xfce by default
17:31:13 <aba> other options then stepping on their toes?
17:31:14 <rra> I'm still inclined to vote in favor of something that says network-manager should be Recommends.  I just wanted to raise that.
17:31:23 <aba> which don't give people too many surprises on upgrades, that is.
17:31:28 <vorlon> are we sure that's being done in a way that the xfce maintainers won't be in a similar quandary in jessie?
17:31:33 <Diziet> And I don't think that the gnome-maintainers should be allowed to treat virtual packages that everyone has been encouraged to install as a sort of vanity thing that they and/or GNOME upstream can just declare "means exactly what I say it means"
17:31:44 <rra> vorlon: No, I'm not... I haven't even looked at it.
17:31:57 <rra> although some of these problems are fairly unique to network-manager.  If it just installed another unnecessary package, it wouldn't be a big deal.
17:32:01 <dondelelcaro> What about we split this out into two things? one the quick "make it recommends", and the other a more general guidance to maintainers?
17:32:09 <rra> It's because it installs a package that takes over core OS functionality that we got into this whole conversation.
17:32:14 <vorlon> Diziet: it's more than "everyone has been encouraged to install", AIUI; the installer selected this package for them as part of the desktop task
17:32:52 <Diziet> vorlon: Well, yes.  I think it's fine for the desktop task to have done that.  The alternative is to have separate "vanity" and "useful" metapackages and ask the installer people to refer only to "useful" metapackages.
17:32:53 <vorlon> seems to me there really needs to be a debian-default-desktop metapackage
17:33:14 <Diziet> vorlon: You don't want that because then when you upgrade it might switch your desktop.  Urrrgh!
17:33:21 <rra> Diziet: I'm somewhat more sympathetic to the definition of gnome-core than that.  It *is* useful for people across multiple distributions for "GNOME core" to mean the same thing, and upstream's definition is likely the only one that everyone could agree on.  But as you say, Recommends is still quite strong.
17:33:32 <vorlon> Diziet: I was going to have the jessie version depend on unity, of course ;)
17:33:40 <aba> Recommends still contains it by default.
17:34:07 <Diziet> rra: Also upstream are just entirely wrongheaded.  Even n-m upstream say it's not suitable for everyone so GNOME upstream effectively saying everyone who wants GNOME should install it is just stupid.
17:34:30 <aba> Diziet: or that gnome is just not suiteable for everyone.
17:34:31 <Diziet> aba: Indeed.  Recommends is used all the time for exactly this kind of thing.
17:34:32 <rra> dondelelcaro: I'm not sure we can issue a decision on making network-manager Recommends instead of Depends without providing some rationale for why we're doing so.  Although I was probably much too long-winded about it.
17:34:51 <dondelelcaro> rra: yeah, and thinking about it, we kind of hit on the rationale in #681783
17:35:10 <dondelelcaro> ok. maybe we just need to refine the texts slightly, and try to get this voted on soon
17:35:17 <rra> Agreed.  I think "stop avoiding Recommends because you're worried people won't install them" is a good general principle.
17:35:39 <Diziet> OK so if we're agreed on the conclusion and only arguing about the rationale would someone less gung-ho than me and less longwinded than rra please suggest something ? :-)
17:35:44 <rra> The whole point of Recommends is to allow us to provide expected behavior to general users while allowing sophisticated users to fine-tune their package set.
17:35:52 <rra> If people don't use Recommends, we lose that nice ability.
17:36:03 <dondelelcaro> rra: yeah, I think this came up in -devel again with the check_v46 package
17:36:32 <dondelelcaro> ok, I guess I should dive in and see if I can syntesize both versions
17:36:45 <rra> I can take a crack at Ian's version of my text and try to whittle it down a bit more if that seems like the right approach.
17:36:56 <dondelelcaro> sounds fine to me
17:37:04 <Diziet> dondelelcaro: Good luck.  You may do better to throw both away and write three sentences of your own.  The more you write the more people find to quibble about...
17:37:13 <Diziet> rra: That would be fine by me.
17:37:20 <rra> Okay, let's do that.
17:37:28 <dondelelcaro> rra: why don't we both give it a shot, and check back in a week or so and see what happened
17:37:29 <rra> Diziet: Yeah, isn't standard work fun?  :)
17:37:34 <rra> dondelelcaro: Sounds good.
17:37:55 <dondelelcaro> #action rra and dondelelcaro to try to write consensus agreement on #681834 (gnome meta package) separately
17:38:03 <dondelelcaro> #topic #685795 New ctte member
17:38:05 <rra> Diziet: If you want the full experience, try writing Policy text for something large like triggers or maintainer script dependencies.  :)
17:38:32 <Diziet> Yers.
17:38:46 <jcristau> rra: or symbols
17:38:55 <rra> jcristau: Indeed.
17:39:02 * rra is about to dive back into that and propose text for multi-arch.
17:39:14 * rra will be back in five, apologies.
17:40:09 <dondelelcaro> I don't really know what to do for this one; I suppose we can suggest people privately and see what people think?
17:40:14 <dondelelcaro> maybe zack has suggestions too
17:40:45 <Diziet> As I say, if we want to improve diversity we should have a more structured process that will encourage applications from previously-underrepresented groups.
17:40:46 <dondelelcaro> (though zack is out for the time being)
17:41:08 <dondelelcaro> should we ask people to nominate people?
17:41:16 <Diziet> The private suggestions and "nod from all around" approach is what produces homogeneity.
17:41:27 <dondelelcaro> ok
17:41:40 <rra> (back)
17:41:47 <Diziet> I'm not sure exactly what the process ought to be but it ought to include a public call for candidates on dda.
17:41:51 <dondelelcaro> I'm ok with nominations being open or closed; I just suggested closed nominations and discussion to avoid unpleasantness
17:41:57 <dondelelcaro> ok
17:42:08 <Diziet> I'm happy with private discussion.
17:42:14 <rra> Yeah, public calls are one of the standard techniques to get diversity, and I think that would work here.
17:42:17 <dondelelcaro> I can write up an announcement; where do we want nominations to be sent?
17:42:32 <Diziet> Do we want people to nominate each other or themselves ?
17:42:39 <aba> both?
17:42:44 <dondelelcaro> yeah, I'd prefer both
17:42:58 <aba> dondelelcaro: there's a private tech ctte list, or at least used to be one
17:43:02 <rra> So, one thing we could consider is to use a nominating committee approach.
17:43:07 <Diziet> And are we going to ask someone doing a nomination to find a couple of sponsors or something to write a "how great is this person" email ?
17:43:29 <Diziet> private tech ctte list> I tried to get that resurrected but it's not happened yet.
17:43:41 <aba> Diziet: I wouldn't like that. This is not trying to vote for the most popular candidate
17:43:58 <rra> The IETF does this thing where they request volunteers for a nominating committee, and then they chose a ten-member nominating committee by lot from all of the volunteers.
17:44:12 <rra> That committee then puts forward a recommended slate of candidates, which are reviewed by the board.
17:44:24 <dondelelcaro> aba: though it would be useful just in case we haven't personally interacted with the nominated person extensively
17:44:29 <aba> I think we can do that if we get more than 5 recommendations?
17:44:39 <Diziet> I thought something like "If you know of someone who would be good, please let us know and ideally write to us how good they are.  If you can get someone else to write how good they are too, please get them to do so - especially if you know this from some time you each disagreed with each other"
17:44:50 <rra> The full IETF process is probably overkill, but it may point us in the right direction.
17:44:57 <aba> dondelelcaro: in that case, I'd think we could ask the nominee "with whom did you work in the past", and ask then
17:45:05 <rra> (I highly doubt that we'd get the 70+ volunteers for the NomCom that the IETF gets.)
17:45:06 <dondelelcaro> aba: true
17:45:20 <aba> dondelelcaro: though if none of us has, it might be ... interessting for other reasons ;)
17:45:36 <vorlon> for my part, I'm of two minds on the question of diversity.  I think it's an interesting situation and there could be side benefits to having greater diversity of origin on the TC, but ultimately we need to make sure we're choosing people who have the right mentality / expertise for the work.  So I think it's good to seek out diverse candidates, but we shouldn't choose an unqualified candidate because we're trying to be diverse
17:45:37 <Diziet> Also volunteering for a nomcom is taking a small chance of having to do a lot of work which is not something all of the most useful people will want to sign up for.
17:45:53 <Diziet> vorlon: I very much agree with that.
17:45:58 <vorlon> (Affirmative Action is a good policy for a lot of reasons, but the Debian TC is not a place where it should be applied :)
17:46:04 <rra> Yeah, I do want us to maintain our current momentum.
17:46:04 <aba> vorlon: let's say if we have to otherwise equally candidates, I'd be interessted in diversity
17:46:09 <vorlon> aba: full ack
17:46:20 <Diziet> So a public call is good for making sure we aren't introducing an old boys' network.  But we should be rigorously meritocratic.
17:46:30 <dondelelcaro> I think we're agreed on that
17:46:43 <dondelelcaro> #agreed public call is good for making sure we aren't introducing an old boys' network.  But we should be rigorously meritocratic
17:46:44 * rra tends to give a slight weight to diversity to correct for my own subconscious bias, but generally agreed on principle.
17:47:28 <dondelelcaro> ok. let me prod the private ctte list so that we can use that for nominations, and then I'll draft the e-mail, and send it to -ctte first. does that sounds reasonable?
17:47:30 <rra> In other words, I'm likely to assume people who are like me are better-qualified than people who are not like me, so internally I try to do a little bit of what could be called "affirmative action" to correct for the fact that by default I'm going to slightly favor people who are like me.
17:48:04 <aba> rra: agreed. But that doesn't mean non-diversity candidates are not considered, or that like.
17:48:11 <rra> Right, agreed.
17:48:32 <aba> dondelelcaro: good
17:48:39 <rra> I don't think lack of diversity is our overwhelming problem, just something that we should be considering more than we have.
17:48:53 <dondelelcaro> #action dondelelcaro to prod the private ctte list so that we can use that for nominations, and then I'll draft the call for nominations e-mail, and send it to -ctte first.
17:49:03 <dondelelcaro> ok, anything more on this?
17:49:19 <dondelelcaro> #topic #573745 python maintainer [vorlon to write up resolution]
17:49:31 <CIA-5> debian-ctte: 03ijackson * re268e43 10debian-ctte/681834_gnome_recommends_networkmanager/rra-draft-ijackson: 681634 wording improvement from private email
17:49:57 <vorlon> been ENOTIME
17:50:05 <vorlon> sorry
17:50:06 <Diziet> rra: FYI I have just committed to the git repo that suggestion I had from private email which was just a grammar improvement.  So please pull before you or anyone else starts hacking.
17:50:13 <rra> Diziet: Danke.
17:50:13 <Diziet> vorlon: Is that likely to change ?
17:50:21 <CIA-5> debian-ctte: 03don * r4ea4180 10debian-ctte/meetings/agenda.txt: fix gnome network manager bug number
17:50:22 <CIA-5> debian-ctte: 03don * r42a1d6e 10debian-ctte/681419_free_non_free_dependencies/681419_free_non_free_dependencies.org: we should consider suggests
17:50:23 <CIA-5> debian-ctte: 03don * r71fe67d 10debian-ctte/681834_gnome_recommends_networkmanager/rra-draft-ijackson: Merge branch 'master' of ssh://git.debian.org/git/collab-maint/debian-ctte
17:50:29 <vorlon> Diziet: three-day weekend coming up, yeah
17:50:41 <dondelelcaro> horray for labor day
17:51:22 <dondelelcaro> #topic additional business
17:52:11 <Diziet> vorlon: OK good.  How about we say if you don't do it by next Thursday we action someone else (who?)
17:52:18 <rra> Just a quick mention that I intend to forward Policy bugs one at a time as we resolve the previous ones, and will try to pick and choose the ones that seem to be the most interesting and pressing.
17:52:33 <rra> We discussed that some last time, but I thought I'd mention that's still the plan.
17:52:44 <vorlon> Diziet: deadlines are good ;)
17:52:45 <Diziet> OK
17:52:49 <Diziet> vorlon: :-)
17:52:52 <Diziet> vorlon: (I sympathise.)
17:53:23 <Diziet> I would rather not volunteer for the python thing.  Does anyone else feel like writing up the two alternatives we agreed on voting on IIRC ?
17:53:24 <dondelelcaro> rra: sounds good
17:53:51 <dondelelcaro> I suppose I can take whatever vorlon manages to get to and work on something, but if someone else wants it, that'd be fine too
17:54:06 * dondelelcaro would just like to see it get resolved
17:54:26 * vorlon nods
17:54:39 <vorlon> so I expect it's a Sit Down And Do It thing
17:54:54 <vorlon> you'll either have it from me by Thursday, or you'll have nothing from me by Thursday ;P
17:55:19 <Diziet> #action vorlon to write up a resolution by 1346929200 failing that dondelelcaro to do so
17:55:25 <dondelelcaro> sounds good
17:55:40 <dondelelcaro> ok, anything last minute?
17:55:49 * rra has to drop out for a day-job meeting.  Thanks, everyone!  These meetings are really useful.
17:56:08 <Diziet> I don't have anything.
17:56:11 <vorlon> nothing here
17:56:14 <Diziet> Thanks everyone.
17:56:24 <vorlon> yep, thanks!
17:56:30 <dondelelcaro> #endmeeting