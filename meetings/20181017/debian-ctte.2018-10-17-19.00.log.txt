19:00:24 <marga> #startmeeting
19:00:24 <MeetBot> Meeting started Wed Oct 17 19:00:24 2018 UTC.  The chair is marga. Information about MeetBot at http://wiki.debian.org/MeetBot.
19:00:24 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
19:00:41 <marga> #topic roll call
19:00:57 <OdyX> I'm likely to drop in ½ hour; I can only chair for that part.
19:01:01 <OdyX> Didier Raboud
19:01:02 <fil> Philip Hands
19:01:08 <gwolf> Gunnar Wolf
19:01:09 * marga Margarita Manterola
19:01:10 <smcv> Simon McVittie
19:01:17 <ntyni> Niko Tyni
19:01:20 <bremner> David Bremner
19:02:11 <Mithrandir> Tollef Fog Heen
19:02:16 <Mithrandir> (distracted too)
19:02:56 <marga> #topic Review of previous meeting AIs
19:03:32 <marga> I think Simon did the summing up really nicely.
19:03:41 <smcv> https://lists.debian.org/debian-ctte/2018/10/msg00015.html
19:04:30 <OdyX> (crazy how fast months go; I haven't dared to open my TC folder yet :-(
19:05:00 <marga> Mithrandir, you were supposed to send the vote for the other issue, right?
19:05:31 <Mithrandir> yes, I still have that outstanding
19:05:43 <Mithrandir> will do ASAP
19:05:45 <marga> Is there anything in the way?
19:05:50 <Mithrandir> time on my side
19:06:11 <gwolf> Absolute time? Negative time? ;-)
19:06:16 <Mithrandir> free time.
19:06:16 <marga> Ok, would you rather someone else took over?
19:06:37 <Mithrandir> give me a week, if I fail to do something within then, I'm fine with somebody taking over.
19:06:41 <Mithrandir> sounds ok?
19:06:54 <fil> Mithrandir: BTW were you taking my (and possibly Ian's) ramblings into account re the vote? (I think there was some merit in there)
19:07:01 <Mithrandir> fil: yes.
19:07:09 <gwolf> Mithrandir: I'd add a bit to it - If in a week you cannot do this, please bring it up explicitly on the rest of us!
19:07:20 <Mithrandir> gwolf: sounds good
19:07:42 <gwolf> (so somebody can raise their hand - I guess we'd all prefer if Somebody Else™ did it)
19:08:21 <fil> well, I seem to have touched it last, so I could give it a go
19:08:38 <marga> #action Mithrandir should raise the vote in the upcoming week. If that doesn't happen, fil can take over
19:08:45 <OdyX> Cool
19:08:48 <marga> Sounds good?
19:08:53 <fil> fine
19:09:32 <Mithrandir> yup
19:11:31 <OdyX> #topic #904302 Whether vendor-specific patch series should be permitted in the archive
19:12:14 <Mithrandir> is there anything more we need to discuss on that?
19:12:16 <gwolf> not #topiced
19:12:42 <OdyX> (I just assumed marga lost connectivity, and can't chair myself)
19:13:05 <OdyX> Also, I'm totally useless on all these issues, I really haven't caught up :-(
19:13:09 <gwolf> Right... Never mind, then :-]
19:13:27 <gwolf> I... Think we should be ready to vote on this one as well
19:13:44 <Mithrandir> this is the one we just talked about that I'm going to write up the actual vote text for.
19:13:51 <gwolf> I mean, there were some more rounds of argument, but...
19:13:56 <gwolf> Right
19:14:12 <OdyX> So next topic?
19:14:12 <marga> Ok, we can skip the next topic I think?
19:14:13 <gwolf> (oh, yes, sorry - ape brain multitasking here)
19:14:38 <marga> #topic #904558 What should happen when maintscripts fail to restart a service
19:15:31 <marga> So, this one is still very much under discussion...
19:15:54 <Mithrandir> I think smcv's summary is a good writeup.
19:16:06 <marga> Agreed
19:16:17 <marga> But the question is how we move forward
19:17:05 <smcv> Ian and Wouter proposed an interface based on policy-rc.d where sysadmins can arrange for service restart failures to be ignored or not
19:17:09 <marga> My impression was that there was some rough agreement that in general we want to recommend maintainers not to fail their maintscripts unless they think it's worth the breakage
19:17:14 <gwolf> Right. The discussion in the last few weeks *did* illustrate very big expectative differences in very different use cases :-/
19:17:56 <marga> Right, they proposed doing some changes which might be interesting to have. But I don't think we can rul such solutions should be implemented...
19:18:41 <fil> The fact that most things used to quietly ignore restart failures under sysvinit seemed like a rather important point that was mentioned recently
19:18:54 <gwolf> marga: I tend to side with your interpretation and motivation, but I didn't feel there was a "rough agreement" towards that end
19:19:09 <Mithrandir> fil: did actually most things do that?  I haven't seen numbers one way or the other.
19:19:33 <smcv> I am wary of solutions involving policy-rc.d, because you have a problem, and you think "I know, I'll solve it with policy-rc.d", and now you have an unpackaged file in /usr/sbin that may or may not get overwritten by debootstrap, and two problems
19:19:51 <Mithrandir> smcv: yeah, I agree
19:19:59 <smcv> or at least that's been my experience in Debian derivatives with non-trivial policy-rc.d
19:20:08 <OdyX> Also, policy-rc.d is vastly unknownk
19:20:27 <marga> fil, yes, I was also surprised that that was the case.
19:20:34 <smcv> SteamOS is meant to have a policy-rc.d that suppresses restarts when we are upgrading during shutdown because $reasons
19:20:48 <smcv> it turns out that it does not, in practice, exist, because it gets overwritten by debootstrap
19:21:06 <fil> Mithrandir: well, that was Michael Biebl's assertion (which seemed plausible, but I've not checked)
19:21:32 * gwolf prefers not to even try to wrap brain around smcv's last idea. Sounds too perferse and obscure :-P
19:21:35 <smcv> re ignored restart failures: it partly depends how carefully the daemon was written
19:22:22 <smcv> there is a Right Way, that nobody implements, where you only double-fork after you have read all configuration, done everything that could fail (except the things that you can only do after double-forking), etc.
19:22:23 <gwolf> OdyX: I agree that this would require pushing policy-rc.d closer to the consciousness of developers -- And users.
19:23:02 <marga> There was a mention of 'the daemon was running before the upgrade and it's not running after the upgrade, so we know it's a problem', but I don't know most maintscripts actually know that much.  Most only know that it failed to restart not that it was running before... Am I wrong there?
19:23:36 <smcv> initscripts often implement restart as stop; start
19:23:41 <gwolf> marga: That would be easy to solve, even if it brought up a mass instant-buggy - `status` before `restart`
19:23:53 * bremner tries to catch up
19:24:08 <smcv> aka `systemctl try-restart`
19:24:18 <marga> gwolf, sure, I understand it's possible, I'm asking whether maintscripts actually check.
19:24:23 <smcv> I'm not sure whether there was a traditional sysvinit equivalent of try-restart
19:24:54 <gwolf> smcv: I would really not like tech-ctte to mandate systemd interfaces...
19:25:00 <marga> Agreed
19:25:05 <OdyX> on the other hand, the good argument for /usr/sbin/policy-rc.d is that executables should not be in /etc, but it really feels ugly (.d for a binary, managed through alternatives, really?)
19:25:09 <marga> So, the thing is, what do we want to mandate on?
19:25:25 <smcv> gwolf: no, of course not, I'm just saying there is prior art for "restart this, but only if it's running"
19:25:32 <gwolf> right
19:25:48 <smcv> I wonder whether deb-systemd-invoke uses it
19:25:56 <smcv> /usr/share/debhelper/autoscripts/postinst-init-restart doesn't
19:26:21 <marga> I expect most packages use dh_installinit, and will keep using it for the near future
19:26:26 <smcv> neither does /usr/share/debhelper/autoscripts/postinst-systemd-restart
19:27:41 <smcv> so, indeed, most of the time the maintscript that responds to a restart failure doesn't even know whether it's a regression
19:27:49 <marga> Right
19:28:17 <marga> So, we were asked to provide at least a recommendation.  Do you think we could recommend something?
19:28:56 <marga> I think we can also encourage people to develop better methods for both informing the user and handling errors, but we can't really rule about non-existant things...
19:29:11 <bremner> I thought the original question was about whether a sensible policy could exist
19:29:44 <bremner> so, no, would be an (unhelpful) answer
19:30:07 <gwolf> This has proven to be thorny enough. Again, the use cases are distant enough... That shoehorning a set of users into a given behavior can be seen as bad no matter what
19:30:44 <marga> We can recommend without shoehorning
19:31:02 <OdyX> that makes a policy-rd.d route somewhat "the least worst" idea: a middle ground.
19:31:59 <gwolf> Yes. But not in a way that makes it mandatory (or we will just have most daemons buggy because they don't check for status beforehand)
19:32:14 <marga> How would that look like as a ruling?
19:32:43 <smcv> I think if we are going for wouter's policy-rc.d route, then it would have to be something like
19:33:07 <bremner> we could block the tech-ctte bug on a policy-rc.d wishlist bug
19:33:18 <smcv> if invoke-rc.d exits with nonzero status when restarting a service, this should make the maintainer script fail
19:33:39 <bremner> oh, that's more sensible than my idea.
19:34:05 <smcv> (sysadmins are reminded that policy-rc.d can give invoke-rc.d error-handling behaviour that results in it recovering from failures and exiting 0)
19:34:36 <smcv> ... but there are a couple of reasons I find that unsatisfying
19:34:56 <bremner> ignorant question, is invoke-rc.d used by non-sysvinit things?
19:35:11 <smcv> no, good point
19:35:15 <gwolf> smcv: I would add to this that restarting a service SHOULD follow querying for its status
19:35:19 <marga> How would this look like in a real life use case?
19:35:27 <smcv> s/invoke-rc.d/invoke-rc.d or deb-systemd-invoke/
19:35:30 <gwolf> (and, of course, not restart non-running stuff)
19:35:51 <marga> Say, a service fails to start because the config is now outdated and fails to parse?
19:35:56 <smcv> (or an actual ruling would have to have some words about possible alternative implementations but we would all know that it meant invoke-rc.d and deb-systemd-invoke)
19:36:27 <smcv> for context, pure systemd services don't use invoke-rc.d, they use deb-systemd-invoke
19:36:40 <smcv> but both invoke-rc.d and deb-systemd-invoke call out to policy-rc.d
19:36:48 <smcv> I don't know whether deb-systemd-invoke implements the 106 thing
19:37:04 <smcv> it does not
19:37:21 <marga> I fear we are going too much into implementations details... Is that something we want to / should do?
19:37:50 <smcv> perhaps not, but now we know that wouter's plan doesn't work as stated
19:38:03 <OdyX> / can do? (re: $6.3.5)
19:38:35 <Mithrandir> bremner: invoke-rc.d and policy-rc.d is also randomly-ish used by things like ifup scripts and so on
19:38:53 <bremner> Mithrandir: that's documented as unsupported, more or less?
19:39:10 <gwolf> OdyX: Re 6.3.5, we can sketch it in text without describing the tooling?
19:39:24 <gwolf> That would free us from invoke-rc.d and deb-systemd-invoke
19:39:56 <bremner> I think the constitutional meta discussion happened before didn't it?
19:39:59 <smcv> so, what, we'd say "Someone™ should implement an interface for sysadmins to decide what happens on this class of failure?"
19:40:31 <marga> Yeah, exactly, I think we need to refrain from describing the exact implementation and instead focus on what are the results we want
19:40:36 <gwolf> bremner: yes, (IIRC I brought it up last meeting) but I think it's sensible to be reminded of
19:41:21 <smcv> marga: you wanted a description of what happens in real world use cases, do you still?
19:41:51 <marga> Not sure, I guess it depends on how we plan to move forward
19:42:05 <marga> I mostly want to move forward and not keep stalling
19:42:07 <smcv> so what wouter and ian were advocating in recent discussion was
19:42:20 <smcv> your daemon fails to restart because its config is outdated or whatever
19:42:56 <smcv> in a system with no special configuration, invoke-rc.d fails, the maintscript fails, the package fails to configure, dpkg fails, apt fails
19:44:02 <smcv> in a system that has been preconfigured by marga's sysadmin team, the restart fails, the failure is caught, magic happens, invoke-rc.d does not fail, everything continues as though it was all OK
19:44:38 <fil> that (global failure) seems like a poor default to me -- even more so if the scripts are not checking whether the daemon was able to run before we started
19:44:44 <smcv> (implementation detail: the magic that would happen, according to wouter's plan, is that you install a policy-rc.d that tells invoke-rc.d "on failure, just stop it")
19:44:52 <marga> How far away are we from making this possible? It wasn't clear to me from the discussion if this was actually something already implemented or something that needed more work.
19:46:02 <smcv> for invoke-rc.d, afaics it can happen already, *if* you drop in a suitable policy-rc.d
19:46:08 <OdyX> I haven't read the thread (booo), but as framed, it feels weird to use the dpkg -> apt pipeline to signal for $any service restart failure.
19:46:33 <bremner> it's about unconfigured packages?
19:46:37 <smcv> for systemd-only services, afaics it can't, because deb-systemd-invoke doesn't implement that particular little-known corner of the policy-rc.d API
19:46:40 <OdyX> (sorry if that's rehash)
19:47:30 <bremner> OdyX: we could state the default in terms of "package fails to configure"
19:47:43 <smcv> hmm, wait, I'm not actually even sure whether 106 does what wouter said it does
19:48:23 <marga> OdyX, I agree with you, but some people don't and they insist on using the output of maintscripts as a signal of failures
19:49:10 <marga> Alright, we are at 50 minutes past the hour and it doesn't seem we are getting anywhere... We should stop discussing the issue and instead decide how we want to reach consensus...
19:49:34 <OdyX> on the other hand, if SSH failed to restart and died, having apt in a weird state is the least of my concerns
19:49:48 <Mithrandir> I think we need to reach for ietf consensus, ie what are you willing to live with, more than what's a perfect solution.
19:50:14 <marga> Well, we are already living with "each maintainer decides whatever they want"
19:50:31 <fil> is there some way to make the default something like "carry blithely on unless the maintainer has somehow tagged their package as being too important to fail" ?  and then let the local admin override that (to be more or less fragile) it they feel the need
19:50:59 <gwolf> marga: Right. Some lines ago, there was mention (by whom? Don't remember - again, multitasking here :-P) of "depends on the nature of your service"...
19:51:14 <smcv> fil: for the maintainer part, dh_installinit --error-handler=function
19:51:15 <gwolf> So, yes, this could be _legitimately_ left as part of the maintainer's decision grounds
19:51:30 <OdyX> There's likely value in having two classes of services, yes.
19:51:37 <Mithrandir> gwolf: I think it's important that we give advice about what the default should be
19:51:49 <Mithrandir> since else we have the current situation where it's half and half (or thereabouts)
19:51:58 <smcv> fil: not implemented by dh_installsystemd but I think it's a reasonable feature request, unless we come to the conclusion that there is only one legitimate value for the error handler, in which case there's no point in implementing it
19:52:01 <bremner> Mithrandir: that feels strange if the default is not possible with current tooling
19:52:15 <bremner> *shrug*, maybe that's the least of our problems
19:52:19 <smcv> for the "sysadmin can override to be more or less fragile", no that's not currently possible
19:52:25 <gwolf> Mithrandir: the default should be, the new package works like a charm and never fails in any way :-]
19:52:51 <Mithrandir> gwolf: not what we're talking about though. :-)
19:52:56 <gwolf> yup, sadly
19:52:56 <smcv> I would prefer the signalling mechanism to not be policy-rc.d
19:53:21 <bremner> what cross-init alternatives are there?
19:53:23 <gwolf> I also think policy-rc.d is too obscure to most people
19:53:43 <smcv> this is not the init's problem
19:54:02 <smcv> both invoke-rc.d and systemctl will give you an exit status and let you do what you want with it
19:54:47 <smcv> this is entirely an issue for dh_install{init,systemd} (and their open-coded equivalents, for people who like artisanal handcrafted maintainer scripts)
19:55:05 <OdyX> We're left 5 minutes. Could anyone wrap these arguments up in a mail?
19:55:28 <smcv> unless we want to mandate that invoke-rc.d and dh-systemd-invoke gain new error handling behaviour
19:56:02 <gwolf> OdyX: Right. I hope to be more useful next meeting (that means, no, I'm not volunteering to wrap up), as I am too attention-dilluted right now :-(
19:56:24 <marga> I'll volunteer for trying to move the discussion forward, but I feel like we didn't make much progress today :-/
19:56:35 <marga> I feel like we are running in circles
19:56:37 <smcv> I volunteer to reply to wouter re the policy-rc.d thing, since I've done the research now
19:56:49 <marga> Ok, thanks :)
19:57:06 <smcv> if we want a summary of this meeting's discussion I'd prefer that to be not me
19:57:11 <marga> #action smcv to clarify what can be done through policy-rc.d to the mailing list
19:57:25 <marga> #action marga to try to summarize the discussion and move the discussion forward
19:57:29 <marga> Does that seem fair?
19:57:32 <smcv> great
19:57:41 <OdyX> Cool
19:57:41 <marga> #topic Recruiting efforts
19:58:00 <marga> We are in mid-October and I think we need to start recruiting again
19:58:24 <marga> It's just OdyX that leaves us after December, right?
19:58:31 <gwolf> and Mithrandir IIRC
19:58:36 <marga> Ah, yes...
19:58:44 <fil> fair enough -- I'll fire up the randomizer :-)
19:58:49 <marga> So sad, time goes so quickly
19:59:00 <OdyX> Yep. Two members go.
19:59:03 <marga> Yes, fil, please fire the randomizer.
19:59:03 <Mithrandir> time flies when we're having fun.
19:59:12 <Mithrandir> I need to nominate some folks, I think I have a small list somewhere.
19:59:26 <marga> Alright, I'll action you two
19:59:46 <marga> #action fil will do a few rounds of the random emailer to try to get nominations
19:59:48 <gwolf> Just FTR - We *can* operate with a shortage in membership. We are supposed to have between 4 and 8 members. So, yes, we should act on it. But it's not like we are time-stressed to get new bl00d.
19:59:55 <marga> #action Mithrandir will nominate people
20:00:15 <marga> And maybe I should email d-d-a?
20:00:43 <gwolf> Yes, to seek self-nominations
20:00:44 <marga> gwolf, true, but we don't want to be understaffed. A plurality of opinions is good.
20:00:49 <marga> Alright, then.
20:00:52 <OdyX> gwolf: true. But the TC loses two every year, it goes quick
20:01:06 <marga> #action marga to email d-d-a to encourage self-nominations.
20:01:09 <gwolf> Right to both of you - We are not _constitutionally_ stressed yet.
20:01:37 <fil> 10 mails sent
20:01:43 <marga> Awesome :)
20:01:48 <gwolf> Efficient!
20:01:51 <marga> #topic Any other business
20:01:59 * fil has a little script
20:02:02 <gwolf> I have to go. I'm late to have lunch with my kids
20:02:20 <gwolf> So... I'll leave the AOB (including the new bug) to you.
20:02:30 <marga> I wanted to ask whether we should meet more often now that we have open issues and apparently more controversial than initially expected?
20:03:11 <OdyX> As long as the calendar is kept up-to-date, I'll try.
20:03:22 <OdyX> But not more than every 3 weeks
20:03:23 <marga> Would it make sense to schedule an "extraordinary meeting" in 2 weeks instead of next month?
20:03:32 <OdyX> or that, yes.
20:03:47 <fil> or just chat in IRC a bit more?
20:04:06 <bremner> it's probably important to have fixed meeting times.
20:04:12 <marga> I feel like having the actual meeting deadline / time helps.
20:04:18 <bremner> from a transparency point of view, alaso
20:04:20 <bremner> also
20:04:25 <marga> yup
20:05:14 <fil> fair enough
20:05:23 <marga> Anyway, we are over time. I'll send this to the mailing list as well.
20:05:32 <bremner> I can make a meeting in two weeks.
20:05:49 <marga> Let's end here and leave people free to go. Thanks everyone for attending!
20:05:56 <marga> #endmeeting