====================
#debian-ctte Meeting
====================


Meeting started by dondelelcaro at 17:59:05 UTC. The full logs are
available at
http://meetbot.debian.net/debian-ctte/2016/debian-ctte.2016-02-24-17.59.log.html
.



Meeting summary
---------------
* Who is here?  (dondelelcaro, 17:59:12)

* Next Meeting?  (dondelelcaro, 18:01:21)

* #741573 Menu systems - Debian Policy followup  (dondelelcaro,
  18:02:19)
  * ACTION: dondelelcaro to finally upload debian-policy  (dondelelcaro,
    18:03:39)

* #797533 New CTTE members  (dondelelcaro, 18:03:46)
  * ACTION: everyone to actually review all of the candidates and report
    to the committee on -private  (dondelelcaro, 18:06:21)

* Additional Business  (dondelelcaro, 18:06:58)

Meeting ended at 18:19:24 UTC.




Action Items
------------
* dondelelcaro to finally upload debian-policy
* everyone to actually review all of the candidates and report to the
  committee on -private




Action Items, by person
-----------------------
* dondelelcaro
  * dondelelcaro to finally upload debian-policy
* **UNASSIGNED**
  * everyone to actually review all of the candidates and report to the
    committee on -private




People Present (lines said)
---------------------------
* dondelelcaro (42)
* OdyX (16)
* Mithrandir (5)
* keithp (3)
* MeetBot (2)




Generated by `MeetBot`_ 0.1.4

.. _`MeetBot`: http://wiki.debian.org/MeetBot
