16:59:31 <dondelelcaro> #startmeeting
16:59:31 <MeetBot> Meeting started Thu May 30 16:59:31 2013 UTC.  The chair is dondelelcaro. Information about MeetBot at http://wiki.debian.org/MeetBot.
16:59:31 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
16:59:36 <dondelelcaro> #topic Who is here?
16:59:38 <dondelelcaro> Don Armstrong
16:59:43 <bdale> Bdale Garbee
17:00:04 <dondelelcaro> I know rra is probably not going to be here
17:00:14 <bdale> yup, saw his note
17:00:33 <cjwatson> Colin Watson
17:00:43 <vorlon> Steve Langasek
17:00:51 <vorlon> (but possibly only intermittently)
17:01:47 <dondelelcaro> cool
17:01:59 <dondelelcaro> I guess I'll move on to the next topic
17:02:02 <dondelelcaro> #topic Next Meeting?
17:02:04 * aba is here
17:02:31 <dondelelcaro> currently, the next meeting is scheduled for the same time on the 27th of June
17:02:38 <dondelelcaro> does that work for everyone?
17:03:12 <bdale> looks ok here
17:03:15 <aba> should
17:03:18 <vorlon> AFAIK yes
17:03:22 <dondelelcaro> ok, cool
17:03:31 <dondelelcaro> #agreed next meeting to be 27th of June at same time
17:03:41 <dondelelcaro> #topic #688772 Do the changes to NM address the concerns properly? [Bdale+RMs to rule]
17:03:43 <cjwatson> 27th works for me
17:03:54 <dondelelcaro> I think for this, bdale just needs to ok it
17:04:01 <dondelelcaro> I believe we've now passed the window where this actually matters
17:04:06 <bdale> I keep forgetting to get around to this
17:04:17 * aba agrees with dondelelcaro
17:04:18 <bdale> yes, I think so
17:04:41 <dondelelcaro> cool
17:04:44 <bdale> should we just close it?
17:04:47 <dondelelcaro> sure
17:04:59 <cjwatson> Agreed, either it's OK or it's overtaken by events ...
17:05:02 <dondelelcaro> if you close it, that's good enough
17:05:11 <bdale> working on it
17:05:56 <dondelelcaro> #topic #698556 and #700759 -- don to deal with these two
17:06:12 <dondelelcaro> I've failed, and forgot to deal with these myself; I'll get to them today.
17:06:36 <cjwatson> Hm, in what respect is #698556 not dealt with?
17:06:48 <dondelelcaro> cjwatson: I need to e-mail -announce
17:06:53 <cjwatson> Ah, OK
17:06:55 <dondelelcaro> at least, I think I haven't done that yet
17:07:06 <aba> looks like
17:07:11 <dondelelcaro> #action dondelelcaro to deal with #698556
17:07:14 <dondelelcaro> #action dondelelcaro to deal with #700759
17:07:19 * rra gets back earlier than expected from the auto mechanic.
17:07:24 <dondelelcaro> #topic #636783 super-majority conflict;
17:07:43 <dondelelcaro> rra: hopefully less expensively than expected too! ;-)
17:08:10 <aba> who wanted to take care of it?
17:08:33 <dondelelcaro> I think last time we had agreed to backburner it until after the release
17:08:38 <rra> Thankfully, just a routine service.  At least so far, and I'm clinging to my illusions.  :)
17:08:46 <dondelelcaro> but this was Diziet's issue
17:08:47 <aba> It looks like we're there
17:09:03 <dondelelcaro> (at least, iirc, that was the case)
17:09:46 <dondelelcaro> I think we can ask Diziet to write up a proposal and send it to -ctte, and then start the discussion of it there
17:09:57 <dondelelcaro> does that sound reasonable?
17:10:06 <aba> would work for me
17:10:13 <bdale> sure
17:10:46 <dondelelcaro> #action Diziet to write up a proposal (or start of one) for #636783 (super majority conflcit)
17:10:52 <dondelelcaro> #topic #681419 Depends: foo | foo-nonfree
17:11:02 <dondelelcaro> cjwatson: did Diziet send the review of this to you?
17:11:18 <cjwatson> No, I don't think so
17:11:26 <dondelelcaro> ok
17:11:39 <cjwatson> If we both make it to the pub last night then I shall prod him there :)
17:11:44 <cjwatson> Er.  Tonight.
17:11:47 <dondelelcaro> ah, awesome
17:11:50 <cjwatson> Apparently I have a time machine.  Cool.
17:11:53 <dondelelcaro> heh
17:12:04 <rra> If you both make it to the pub last night, you could have him take care of it before this meeting.  :)
17:12:21 <cjwatson> A fair point and well argued.
17:12:40 <dondelelcaro> #topic #685795 New ctte member
17:12:44 <bdale> cjwatson: I thought maybe you'd already been to the pub today for a minute there...
17:12:48 <dondelelcaro> heh
17:13:17 <bdale> so I should probably engage Lucas for a chat about the new member process?
17:13:25 <dondelelcaro> yeah, sounds reasonable
17:13:36 <aba> yes.
17:13:55 <aba> though technically spoken the first move is ours
17:13:56 <bdale> I think where things were left is that I was supposed to summarize who said yes in private email to all of you, then we'd make a recommendation, but that kind of stalled while we waited across the DPL transition
17:14:01 <dondelelcaro> right
17:14:23 <dondelelcaro> yeah, once we have an accepted list of nominees, we can check with lucas, and then send a mail to -announce
17:14:25 <bdale> aba: yes, of course, but I think there's value in having the DPL engaged in the process
17:14:38 <aba> bdale: not disagreeing
17:14:58 <aba> (that's what I meant with "technically spoken", because there is still some value in speaking before)
17:15:08 <bdale> ok, so my next action will be to talk to Lucas about the process and where we are.  I'm off-net all of next week (really off net) so it'll be the week of the 10th when I get to this
17:15:26 <bdale> I'll commit to action on this before our next meeting
17:15:41 <dondelelcaro> cool
17:16:22 <dondelelcaro> #action bdale to talk to lucas about process, and e-mail -ctte-private with accepted nominees
17:16:28 <dondelelcaro> #topic Additional Business
17:16:56 <aba> so, who plans to be at debconf?
17:17:09 <bdale> I do
17:17:10 <dondelelcaro> I will not be there, unfortunately
17:17:13 <cjwatson> I will be there
17:17:17 <bdale> I put in a request for a tech ctte bof, too
17:17:29 <aba> I plan to be there as well
17:17:32 * rra won't be there this year but will be there next year.
17:17:46 <aba> rra: this way we're never going to meet I fear
17:17:48 <cjwatson> Though staying some distance away with family; not sure how many evening things I'll get to
17:18:11 <dondelelcaro> aba: you should come to portland; it's pretty awesome
17:18:16 <rra> aba: Yeah, the idea of a 10-hour international plane flight fills me with existential dread.  :)
17:18:25 <bdale> cjwatson: been there.  I'll be at the venue this year, if we have an organized BOF session that'll at least give all of us present a chance to meet and interact with folks
17:18:38 <aba> dondelelcaro: I'd say probabilities are lower for next year than for this
17:18:47 <bdale> rra: you'd not have liked my Jan/Feb adventures this year, then
17:18:56 <cjwatson> rra: nine-hour (or so) international train trip for me, but that's fine :)
17:19:07 <rra> Oh, yeah, if I could take the train, I'd totally go.
17:19:09 * bdale likes trains
17:19:12 * rra does too.
17:19:21 <aba> rra: you could take the boat at least.
17:19:28 * bdale lol
17:19:38 <bdale> "take one step, come up for air..."
17:19:39 <rra> I hear Carnival Cruise Lines run very well-maintained and clean ships.
17:20:00 <aba> h01ger wanted to move debconf to a ship. Let's see if this works out one day
17:20:32 <bdale> a few more cruise ship incidents, and it might become a really cheap venue?
17:20:42 <aba> .oO(how do you know the plan?)
17:20:47 * bdale suspects getting bandwidth to a cruise ship wouldn't be cheap, though
17:21:01 <dondelelcaro> yeah
17:21:03 <rra> Yeah, that would be the main thing I'd wonder about.
17:21:04 <bdale> anyway
17:21:13 <bdale> I have no other serious business today.  does anyone else?
17:21:20 * aba not
17:21:23 * dondelelcaro is good
17:21:28 <rra> Nothing more here.
17:21:43 <cjwatson> Not I
17:22:01 <dondelelcaro> #endmeeting