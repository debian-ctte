17:09:41 <dondelelcaro> #startmeeting
17:09:41 <MeetBot> Meeting started Thu Sep 26 17:09:41 2013 UTC.  The chair is dondelelcaro. Information about MeetBot at http://wiki.debian.org/MeetBot.
17:09:41 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
17:10:21 <dondelelcaro> cjwatson: sounds good
17:10:39 <vorlon> hello
17:11:16 <dondelelcaro> #topic #681419 Depends: foo | foo-nonfree
17:11:38 <cjwatson> OK, I'll repeat for the benefit of the log
17:11:47 <cjwatson> Well; I have finally caught up on #681419, and I think that (a) Ian has persuaded me to support his option by way of his addition of the phrase "non-release-critical" (I'm not convinced about the practical effects of the rest of it, but that addition means it isn't entirely hard-line and there's a get-out for exceptional cases), and (b) I'd like to call for a vote soon
17:12:01 <cjwatson> Unless there are objections of the form "need more time to consider it, and am actually going to do so soon" :-)
17:12:27 <vorlon> so I still don't agree, and will still vote against, and still have not made the time to write up my alternate resolution
17:12:48 <cjwatson> (I will check when Ian gets back first though, since it'd be rude to call for a vote on something he cares about when he's not available to participate)
17:12:54 <cjwatson> vorlon: You also don't agree with option A?
17:13:23 <vorlon> cjwatson: can you point me to option A?
17:13:42 <cjwatson> vorlon: http://anonscm.debian.org/gitweb/?p=collab-maint/debian-ctte.git;a=blob;f=681419_free_non_free_dependencies/cjwatson_draft.txt;h=f07b7d1c25adeb69d113640b1a5a900923cc0621;hb=HEAD
17:14:31 <vorlon> yeah, it appears that matches my opinion
17:14:45 <vorlon> so I guess I was supposed to only write up on the mailing list *why* I agree with it
17:15:39 <cjwatson> I would find that useful, since I appear to be a swing vote
17:16:56 <vorlon> ok; drafting the mail now ;)
17:16:59 <cjwatson> Thanks
17:17:13 <cjwatson> OK, that's probably all, move on :)
17:18:10 <vorlon> btw, could we still add bug #719738 to the agenda for today?
17:18:37 <dondelelcaro> yep
17:18:46 <bdale> cjwatson: I'm fine with #681419 moving to a vote
17:19:42 <KGB-3> 03Steve Langasek 05master 3b3c6ee 06debian-ctte 10meetings/agenda.txt Add bug #719738 to the agenda (late)
17:20:36 <dondelelcaro> #topic #717076 Decide between libjpeg-turbo and libjpeg8 et al.
17:21:08 <bdale> it seemed to me that we had concensus last time modulo any new information coming to us .. I haven't seen anything new?
17:21:19 <dondelelcaro> diziet is supposed to draft this, so that's all that needs to be said here
17:21:31 <dondelelcaro> #topic #685795 New ctte member
17:21:46 <bdale> regrettably, I've made no time for this since our last meeting
17:21:54 <dondelelcaro> no worries; just hitting everything
17:22:09 <dondelelcaro> #topic #636783 super-majority conflict;
17:22:44 <bdale> I think I was supposed to provide input to this, and haven't
17:22:45 <dondelelcaro> bdale: I think this one was just waiting for a comment from you before we proceed
17:22:49 <dondelelcaro> yeah
17:23:10 <dondelelcaro> #topic #719738 lvm2 - Add systemd support
17:24:01 <dondelelcaro> vorlon: I think that your point that we should have the technical component of this discussion in the open is well taken
17:24:10 <vorlon> ok
17:24:18 <vorlon> is there a consensus that we should just go ahead and do that?
17:24:34 <vorlon> I have my own technical concerns about the generator stuff
17:24:41 <vorlon> which I suspect are aligned with waldi's
17:24:48 <dondelelcaro> yeah
17:24:53 <vorlon> but I think the right place to figure that out is in the public bug, not in a private mediation
17:25:08 <dondelelcaro> I agree with that
17:25:28 <bdale> right.  I believe mediation should be about people, technical issues should be decided publicly.
17:26:28 <dondelelcaro> we should probably delegate the lead mediation to someone, and then continue the technical discussion in the bug
17:26:48 <vorlon> so since Michael (Stapelberg) has now said yes to having the technical discussion in public, I'm happy to take the action to start that up
17:27:02 <dondelelcaro> ok
17:27:13 <vorlon> bug #719738 is not assigned to us currently and is marked closed... should I reopen/reassign, clone, file a new bug?
17:27:38 <dondelelcaro> reopening and reassigning to us sounds reasonable
17:27:44 <vorlon> ok, will do
17:27:50 <dondelelcaro> (you'll have to unarchive it too)
17:27:55 <bdale> who closed it?
17:28:00 <vorlon> Bastian had done so
17:28:16 <bdale> oh, right, I see that now
17:28:17 <vorlon> (triggering the private CTTE query, I think)
17:28:29 <bdale> yep, makes sense
17:28:35 <bdale> I had missed the detail that the bug was now closed
17:28:45 <bdale> or forgot
17:29:41 <vorlon> ok - seems there's nothing further to say on that right now
17:30:00 <dondelelcaro> #action vorlon to restart the technical discussion
17:30:40 <dondelelcaro> I suppose I probably should keep working on the mediation side of things, probably with Ian
17:31:09 <dondelelcaro> ok
17:31:16 <dondelelcaro> #topic Next Meeting?
17:31:22 <bdale> it would certainly be better if all parties re-engaged in the technical discussion, mediation might help that
17:31:41 <dondelelcaro> yeah
17:31:56 <dondelelcaro> currently it's the same time on the 31st of november
17:32:05 <dondelelcaro> err, october
17:32:16 <dondelelcaro> is that okay for everyone?
17:32:21 <vorlon> I'm traveling that week and likely won't make it
17:32:26 <dondelelcaro> ok
17:32:45 <dondelelcaro> is a week earlier (the 24th) better for people?
17:32:48 <vorlon> for me yes
17:32:52 <bdale> checking
17:33:06 <bdale> yes
17:33:19 <dondelelcaro> ok
17:33:28 <bdale> it's in the middle of ELCE, but I've decided not to travel to Edinburgh since my daughter will be home that week
17:33:31 <cjwatson> ditto vorlon
17:33:35 <cjwatson> (on both counts)
17:33:51 <dondelelcaro> #agreed next meeting on the 24th instead of the 31st
17:34:06 <bdale> I trust you'll update the file so my calendar will just get updated?
17:34:12 <dondelelcaro> yep
17:34:17 <bdale> great, thanks
17:34:25 <dondelelcaro> #action dondelelcaro to update calendar for meeting
17:34:41 <dondelelcaro> #topic Additional Business
17:34:58 <dondelelcaro> anything else?
17:35:28 <vorlon> not from me
17:35:51 <bdale> nothing here
17:36:04 <dondelelcaro> cool
17:36:11 <dondelelcaro> #endmeeting