17:02:22 <dondelelcaro> #startmeeting
17:02:22 <MeetBot> Meeting started Thu Mar 28 17:02:22 2013 UTC.  The chair is dondelelcaro. Information about MeetBot at http://wiki.debian.org/MeetBot.
17:02:22 <MeetBot> Useful Commands: #action #agreed #help #info #idea #link #topic.
17:02:38 <dondelelcaro> #topic Who is here?
17:02:43 <dondelelcaro> Don Armstrong
17:02:52 <cjwatson> Colin Watson
17:03:05 <Diziet> Ian Jackson
17:03:22 <rra> Russ Allbery
17:03:33 <dondelelcaro> vorlon and bdale are marginally around, but may not keep up with everything
17:03:42 <dondelelcaro> #topic Next Meeting?
17:04:20 <dondelelcaro> is anyone here not ok with the 25th of april at the same time?
17:04:49 <rra> That should be good here, and maybe this time I'll remember to put it on my calendar and not rely on reminder messages.  :)
17:05:04 <dondelelcaro> rra: there is an ics file if that would help
17:05:13 <dondelelcaro> (it has future meetings in it too)
17:05:15 <Diziet> I can't make that.
17:05:18 <dondelelcaro> ok
17:05:26 <bdale> 25th April should work for me
17:05:29 <rra> I'm not sure -- I've never tried to import one into our Zimbra thing.
17:05:35 <bdale> Bdale Garbee
17:05:36 <rra> But thanks, I'll see if that helps.
17:05:38 <Diziet> Also consider whether to adjust for DST.
17:05:51 <bdale> Diziet: fwiw, I prefer to not adjust
17:05:52 <cjwatson> I can make 25th April (currently assuming same time UTC)
17:06:01 <dondelelcaro> Diziet: I think we did adjust
17:06:29 <Diziet> I think so too BICBW
17:06:57 <dondelelcaro> ok; I'll keep the 25th, unless there is a big outcry about it
17:07:03 <dondelelcaro> #topic #688772 Do the changes to NM address the concerns properly? [Bdale+RMs to rule]
17:07:07 <Diziet> Anyway either would do, but I can't make the 25th at all.
17:07:54 <dondelelcaro> ok
17:08:25 <dondelelcaro> this one basically just needs bdale to rule on it with the RMs, but if someone thinks that NM doesn't resolve our concerns, they should probably try to convince bdale of that
17:08:46 <dondelelcaro> (or vice versa, perhaps)
17:09:22 <rra> Someone, I forget who, raised some concerns after the public call for testing, but at least to me they seemed more on the order of normal bugs than anything that would mean the original problem wasn't resolved.
17:09:42 <dondelelcaro> yeah; I think that was resolved with a release notes bug and patch
17:10:15 <dondelelcaro> (I think it was Chris Knadle)
17:10:19 <rra> Yes, that's right.
17:10:24 <dondelelcaro> any comments on this right now?
17:10:52 <Diziet> I have to go now I'm afraid.
17:10:53 <dondelelcaro> Otherwise, I guess people can just e-mail the bug; it would be nice to get something out of this quickly if we're going to do something besides approve the NM changes
17:10:59 <Diziet> ttfn
17:11:03 <dondelelcaro> Diziet: ok; thanks for being around
17:11:21 <bdale> I'm inclined to just approve the changes unless someone here objects
17:11:42 <dondelelcaro> #action bdale to deal with NM bug (#688772)
17:12:00 <bdale> I'll try to do that tomorrow
17:12:05 <dondelelcaro> cool
17:12:08 <dondelelcaro> #topic #698556 override isdnutils maintainer's decision to not fix isdnutils
17:12:18 <dondelelcaro> I think this one is resolved; I just have to e-mail -announce
17:12:24 <dondelelcaro> I promised to do that, but then got busy
17:12:33 <bdale> ok
17:12:33 <dondelelcaro> #action dondelelcaro to e-mail -announce about #698556
17:12:40 <dondelelcaro> #topic #700759 Shared library policy on private libraries
17:13:40 <dondelelcaro> I'm not sure what we should do about this bug; I think it might need more discussion, possibly in -policy
17:13:46 <dondelelcaro> does anyone have any comments about it?
17:13:56 <rra> Yeah, I did, but haven't had a chance to write them up in more detail.
17:13:59 <vorlon> I think I already suggested in the bug log that we refer it to -policy?
17:14:35 <rra> A library that is used by other packages is de facto public.
17:14:36 <dondelelcaro> vorlon: right, you did.
17:14:39 <rra> Hence normally Policy would apply.
17:14:53 <rra> I'm not adverse to making special exceptions for unusual cases, but it's not clear to me that this is one such.
17:15:37 <cjwatson> The original reason the ntfs-3g maintainer quoted for doing it this way was that upstream changed SONAME sufficiently frequently that it was a serious pain having to go through the NEW queue on nearly every upload
17:15:50 <cjwatson> So to some extent it's a workaround, but I can understand that
17:16:00 <vorlon> rra: do you think it would be sufficient to take it to -policy and try to provide guidance on what counts as an unusual case that deserves an exception?
17:16:07 <rra> I must admit I'm not horribly sympathetic to that argument.
17:16:09 <cjwatson> And a library that changes interface quite that often is not really very convenient to link to
17:16:36 <rra> vorlon: I'm really not sure there's anything here that Policy should change on, basically.  We can talk about it and see if the shlibs workaround is something we want to bless in Policy, but honestly I'm not sure it is.
17:16:41 <cjwatson> testdisk only started doing so in Ubuntu because we were trying to get rid of the old pre-3g ntfs libraries, and we missed the fact that libntfs-3g wasn't really set up properly for public use
17:16:58 <cjwatson> libntfs-3g has no other packages linked against it in Debian
17:17:04 <rra> That's more compelling -- that we have to binNMU all the time because the SONAME changes in every release.
17:17:16 <rra> Do the new versions of the library actually break testdisk?
17:17:27 <rra> Or is the API used by testdisk stable?
17:17:33 <cjwatson> Not enough information on that, sorry
17:17:53 <rra> If the new versions actually break testdisk, well, we have to binNMU anyway, so we may as well just handle this like any other shared library.
17:18:04 <vorlon> rra: so my position on this is fairly binary: I think if we're not willing to describe in policy what warrants an exception, we should prohibit all the exceptions currently in the archive
17:18:11 <vorlon> so e2fsprogs, openldap, etc.
17:18:21 <rra> How is openldap an exception?
17:18:28 <vorlon> slapd provides: libslapi-2.4-2
17:18:34 <cjwatson> We have to (at least) binNMU no matter what because the SONAME changes, and we can't leave orphaned libraries in the archive ...
17:18:44 <rra> vorlon: But that's for dynamically loaded modules.
17:18:47 <rra> I'm not seeing how that's related.
17:19:16 <vorlon> dynamically loaded modules which exist in a separate binary package and load that library, though?
17:19:24 <cjwatson> So it's either binNMU because the package it depends on has become NBS, or binNMU because the DT_NEEDED entry is no longer resolvable
17:19:40 <cjwatson> Makes little odds as long as dependencies are strict enough that britney can notice
17:19:43 <vorlon> granted the fact that you always want the libslapi that matches the running slapd means it's a somewhat different case
17:19:52 <vorlon> but then, I think *that* is a case that should be spelled out in policy :)
17:20:03 <rra> vorlon: Yeah, I agree that the openldap case should be spelled out in Policy.
17:20:08 <rra> It's equivalent to the Perl module case with perlapi.
17:20:12 <rra> I think that's a different thing.
17:20:16 <vorlon> ok
17:20:33 <rra> I don't understand why e2fslibs is structured the way that it is, as opposed to just packaging the shared libraries.
17:21:04 <vorlon> so it sounds like there's agreement that there are some legitimate exceptions and those should be documented in policy; but there's not agreement that the ntfs-3g usage is legitimate
17:21:13 <rra> That's where I'm at.
17:21:14 <vorlon> so maybe we want to split the bug, and revisit ntfs-3g on the list?
17:21:32 <dondelelcaro> that sounds resonable to me
17:21:33 <rra> The trip through NEW just isn't that big of a deal any more.  When we're not in freeze, I was averaging about 2 days to a week for waiting for new SONAMEs to be blessed.
17:21:37 <rra> Sometimes less than that.
17:21:48 <rra> I have a hard time considering that particularly burdensome.
17:22:02 <cjwatson> I think my position is that in general ntfs-3g's practice is not something we want to encourage, because it results in inevitable breakage during upgrade; but in the case at hand it's not a practical problem
17:22:29 <rra> Well, it's a practical problem in that testdisk is in a weird state, no?  I mean, that's what brought it up in the first place.
17:22:47 <cjwatson> There are two separate issues
17:23:05 <cjwatson> (And the original bug description was very confused)
17:23:20 <cjwatson> One problem is the mere fact of libntfs-3g being bundled with ntfs-3g, rather than being in a separate libntfs-3gNNN binary
17:23:46 <cjwatson> The other problem is that the shlibs-generated dependencies don't adequately express this so britney didn't notice that testdisk was broken
17:24:09 <cjwatson> The Provides trick is an adequate workaround for the second problem
17:24:20 <rra> Ah, I see.
17:24:39 <cjwatson> Given that, the only remaining practical problem as it stands at the moment is that testdisk is unavoidably broken for a while during upgrades
17:24:40 <rra> Okay, yeah, we should split the bug and refer the dyanmically loaded module case to debian-policy for better wording.
17:24:53 <rra> For the rest of the problem, I probably need to spend more time absorbing Sam's comments in the bug log.
17:25:22 <dondelelcaro> ok; any problems with that? We can revisit the ntfs-3g issue itself in the bug log
17:25:37 <vorlon> so the practical impact of having it as a Provides: is that ntfs-3g and testdisks must be upgraded in lockstep; for two packages, that's not particularly burdensome, but it's a slippery slope
17:25:47 <vorlon> dondelelcaro: sounds good to me
17:25:48 <cjwatson> Sam is at least somewhat confused in that he believes that Ubuntu has no equivalent of testing migration :-)
17:26:03 <vorlon> ;)
17:26:06 <dondelelcaro> #agreed split #700759 into a dyanmically loaded module case which is referred to -policy and the ntfs-3g case
17:26:09 <cjwatson> (Though I think people can be forgiven in being out of date by five months)
17:26:31 <dondelelcaro> #action dondelelcaro clone and reassign #700759 as appropriate
17:26:39 <dondelelcaro> #topic #636783 super-majority conflict;
17:26:46 <vorlon> dondelelcaro: thanks for taking the action :)
17:26:49 <rra> I'm actually inclined to believe that we should clean up all these cases.  I'd like to see if Ted has a reason for e2fslibs being structured the way it is other than just history.
17:26:54 <dondelelcaro> I think this one just requires Diziet and probably will happen after release
17:27:03 <dondelelcaro> rra: yeah, probably true
17:27:10 <dondelelcaro> #topic #681419 Depends: foo | foo-nonfree
17:27:20 <rra> Agreed.  I don't think we want to push on this right now, not so much because of hte release than because of the DPL election.
17:27:37 <rra> Someone was going to write up wording for this one, right?
17:27:44 <cjwatson> I think that may still be on me
17:27:47 <dondelelcaro> ah, cool.
17:27:51 <cjwatson> I wrote up wording which has some pending revision
17:27:52 <cjwatson> s
17:28:02 <cjwatson> However I was also waiting for Diziet's review, since I was attempting to channel his position
17:28:17 <dondelelcaro> ah; got it.
17:28:20 <dondelelcaro> ok
17:28:33 <dondelelcaro> #action Diziet to send review of #681419 to cjwatson
17:28:46 <dondelelcaro> #action cjwatson to write up wording for #681419
17:28:51 <dondelelcaro> ok; anything more on this one?
17:29:05 * cjwatson adds a kanban card for that in an attempt not to forget
17:29:08 <rra> No, I think we'd largely talked it out and it was pending wording.
17:29:13 <dondelelcaro> #topic #685795 New ctte member
17:29:36 <dondelelcaro> I think this just requires bdale to finish contacting everyone, and write up the list, and then write to -announce
17:29:41 <rra> I think bdale pinged all the people who were nominated, but I haven't seen a final list of people who accepted the nomination.
17:29:44 <rra> Yes, that sounds right.
17:29:55 <dondelelcaro> I wrote up the suggested announcement e-mail, but it's waiting for a list of people
17:30:42 <dondelelcaro> #action bdale to send list of accepted nominees to -ctte-private and
17:30:57 <dondelelcaro> #action dondelelcaro or bdale to send announcement mails once previous is done
17:31:04 <dondelelcaro> #topic Should we continue having these meetings?
17:31:20 <rra> I would definitely like to -- that I've not been at the last few is totally on me.
17:31:21 <dondelelcaro> everyone ok with continuing to meet like this? [I'll try to continue reminding everyone of them]
17:31:23 <rra> I find them very clarifying.
17:31:35 <bdale> so all the replies were I thought CC'ed so you could all see them, but I recognize I said I'd summarize but have never actually gotten that done.  my bad.
17:31:36 <dondelelcaro> yeah, the past few I've bene bad about myself
17:31:45 <bdale> sorry, lagged
17:31:49 <dondelelcaro> bdale: no worries. ;-)
17:31:53 <rra> Having a little real-time back and forth often helps me wrap my head around a bug in a way that sometimes reading email doesn't.
17:32:02 <bdale> I think the meetings are a good idea
17:32:21 <vorlon> yes, happy to continue having the meetings
17:32:22 <cjwatson> If nothing else I find them a useful prod to get things done.
17:32:23 <bdale> we should keep trying even if we don't always succeed at making them uniformly useful
17:32:28 <dondelelcaro> ok
17:32:29 <bdale> cjwatson: exactly
17:32:35 <dondelelcaro> #agreed keep meeting
17:32:39 <cjwatson> Which otherwise falls by the wayside among a bazillion other things ...
17:32:42 <rra> Yeah, nothing like having to report status to make you do the things you said you'd do.  :)
17:32:51 <dondelelcaro> #topic Additional Business
17:33:06 <dondelelcaro> anything else?
17:33:17 <bdale> so, btw, the notion of not actually naming another person until after the DPL election this year kind of makes philosophical sense to me
17:33:31 <bdale> gives the new DPL the opportunity to be part of the process
17:33:31 <dondelelcaro> ok
17:33:50 <rra> Yeah, I don't feel like there's much rush, and we're probably going to end up taking that long regardless.
17:33:59 <dondelelcaro> sounds reasonable to me. Plus, I doubt we'd be able to get it done that quickly anyway.
17:34:18 <rra> Also, the results of the DPL election will make one person ineligible.  :)
17:34:27 <dondelelcaro> #agreed wait to name another ctte member until after dpl election with consent of new dpl
17:34:31 <rra> I'm not sure if it's anyone who was nominated, since I don't remember the list off hand, but there is that.
17:34:50 <cjwatson> No reason we should block on internal discussions though
17:34:53 <dondelelcaro> right
17:34:54 <rra> No, agreed.
17:35:13 <dondelelcaro> ok; anything else?
17:35:20 <rra> (Hm, actually, is the DPL ineligible to be on the tech-ctte?  Now that I think about it, maybe they're not.)
17:35:33 <vorlon> so you all may have noticed that there have been a few instances recently of people having their rights in the Debian project removed
17:35:34 <rra> (Probably still not a great idea anyway.)
17:36:11 <vorlon> and there have been a variety of opinions expressed (in private) about what impact this has / should have on those people's continued status as package maintainer
17:36:21 <vorlon> I wonder if the TC should be proactive about ruling on this question
17:36:57 <vorlon> to me it seems obvious that the right thing to happen is that when their upload rights are removed, their maintainership status is also reset (de facto orphaning), and they can go through the process again to become a maintainer if they have a willing sponsor
17:37:24 <dondelelcaro> rra: I think so long as they are not the chair of the ctte, it should be ok
17:37:27 <vorlon> but as technically the determination of maintainers lies with the TC, and the question has come up, it might be worth explicitly confirming this?
17:37:45 <rra> DM status was sort of weird because of how it was created by GR; does the DAM have final say over who has DM status the way that they do with DD?
17:38:09 <rra> If the DAM has authority over DM status, then I don't think the TC should get involved; I'd rather leave it to the DAM.
17:38:17 <vorlon> they have authority over the status, yes
17:38:20 <dondelelcaro> vorlon: I think that who is the package maintainer may be separate from who can actually upload a package
17:38:38 <vorlon> the question is whether removing someone's DD or DM status means they are no longer the maintainer
17:38:39 <dondelelcaro> vorlon: so while I'm certain that the CTTE can decide who is the maintainer, I'm not sure that it's directly coupled to who has upload rights
17:38:40 <rra> Since that means the DAM decides on both DD and DM, which means they can revoke both, and if they revoke both, you effectively already have the reset vorlon talks about.
17:38:44 <vorlon> I think "yes" is the correct default answer
17:38:50 <vorlon> but people are arguing this point
17:38:57 <rra> What does "maintainer" mean if you have no upload rights?
17:39:08 <vorlon> rra: it means you have to seek sponsorship
17:39:17 <dondelelcaro> rra: you're in the Maintainer: field, and are primarily repsonsible for the package, but are backed up by a sponsor
17:39:27 <rra> Right, but the assumption is that the sponsor has authority over packages they sponsor, yes?
17:39:56 <rra> So at the point at which they're seeking sponsorship, I thought it was accepted practice that the sponsor can intervene, decline to upload, require changes be made, orphan the package if needed, etc.
17:40:02 <vorlon> which is valid; I just think that if someone is having upload rights removed, they're unlikely to either seek sponsors or find them, and it's best to reflect this reality in the package state
17:40:06 <rra> Or am I giving too much authority to the sponsor?
17:40:21 <vorlon> rra: assumes the process gets far enough along that there *is* a sponsor
17:40:29 <vorlon> rather than the package winding up in limbo
17:40:37 <vorlon> with a de jure maintainer only
17:40:41 <rra> If they have no sponsor, then I don't think we need a special procedure to orphan the packages; surely that's a standard MIA case, no?
17:40:44 <bdale> do we actually have a requirement that the maintainer of record be a DD or DM?
17:41:10 <bdale> it would make sense that we do, I think, but I'm struggling with whether that's actually explicit policy
17:41:19 <rra> If they can't upload the package to Debian because no one will sponsor it, they're effectively MIA from Debian's perspective as a package maintainer.
17:41:21 <vorlon> rra: hmm, trying to work through how to make this point without exposing details of private discussions
17:41:22 <svuorela> bdale: of course we don't. that would be silly.
17:41:48 <rra> vorlon: Should we take it to email and ctte-private, maybe?
17:41:59 <vorlon> ok by me
17:42:26 <rra> It's an interesting question what the role of a non-uploading maintainer is.
17:42:35 <rra> I think that's a corner of Debian's social structure that we don't really have a good handle on.
17:42:48 <bdale> rra: I suspect we never consciously thought about it "back then"
17:42:53 <rra> I'd be very interested in getting the opinion of the DPL and the project more generally on that, separate from the case of people losing privileges.
17:42:54 <cjwatson> bdale: we certainly can't, since we have people working their way towards DD/DM-ship by being sole maintainer on packages
17:43:09 <cjwatson> And I don't think in general that's a bad thing (as long as the packages are such that team maintainership isn't terribly important)
17:43:13 <rra> We have some social conventions, but I don't think we have a firm statement about what rights we want to extend such maintainers.
17:44:10 <vorlon> rra: to summarize though, I think the issue here is that people only get expelled or have their rights revoked due to major social issues with exactly those people who should be the obvious choice for sponsor; having to go through the long MIA process, or having to let the maintainer exhaustively search the set of DDs for a possible sponsor, before declaring the package orphaned feels to me like the wrong standard
17:44:32 <vorlon> and I don't think the TC should need to be in the middle of each case
17:44:40 <rra> vorlon: In general, I'm reluctant to attach too much "automatic" process to someone losing privileges in the project for social reasons.  It's already going to be pretty traumatic, and I think getting all one's packages taken away could easily feel like piling on.  So if it's necessary, I'd rather see it dealt with as part and parcel of disciplinary action, not have it just be automatic.
17:45:09 <vorlon> rra: but do you think the TC needs to be involved in that disciplinary action, or can we "delegate" this case to the DAM?
17:45:14 <rra> In other words, if the DAM feels like the packages also need to be orphaned, maybe they could approach the tech-ctte during the disciplinary phase privately and see if a joint action is appropriate?
17:45:19 <rra> That's an interesting question.
17:45:21 <vorlon> ok
17:45:28 <rra> I'm not sure.
17:45:39 <vorlon> sounds like a topic for further discussion then :)
17:45:42 <rra> To some extent, this is more of a social question than a technical question, so it does feel a bit outside our remit.
17:45:57 <rra> But we do determine package maintainership.
17:46:11 <dondelelcaro> right
17:46:14 <rra> So I'm not sure we constitutionally *can* throw it to the DAM.
17:46:19 <svuorela> rra: as someone who theoretically could be have been involved in revoking priviledges, I kind of feel like it is part of 'cleaning up' to ensure that packages either gets maintained by someone or properly orphaned.. but waiting 6 months for a package to rot sufficiently to do that doesn't seem like the right way forward.
17:46:30 <bdale> rra: right, I was going to ask why we'd want to inject ourself in a largely disciplinary process
17:46:33 <rra> svuorela: Yeah, that's a good point.
17:46:55 <bdale> svuorela: I agree with you
17:46:58 <rra> bdale: I definitely don't *want* to.  :)  But it's not clear that the DAM has constitutional authority to orphan packages or give them to other maintainers.
17:46:59 <cjwatson> Regarding determining package maintainership, I think I would rather that be in the context of somebody asking to take over an expelled developer's packages, rather than simply orphaning them
17:47:20 <vorlon> rra: there are plenty of cases where packages change maintainers by consensus and the TC doesn't need to be involved in each of those; I think giving our imprimatur to a disciplinary process that has consensus among DDs is in the same ballpark
17:47:24 <bdale> rra: to me, it seems like a natural consequence of being kicked out / demoted, I guess I really don't see the value in the distinction
17:47:30 <rra> vorlon: Point.
17:47:34 <svuorela> bdale: but I don't actually have the legitimate power to do such 'cleaning up' according to at least some people, as only you, dear ctte, can change maintainerships.
17:47:47 <cjwatson> Our power to decide maintainership derives from a section on "technical matter[s] where Developers' jurisdictions overlap"
17:48:05 <cjwatson> Which seems to me to be more about disputes between >=2 people, rather than between one person and the void
17:48:10 <bdale> so, if policy *did* require that maintainer of record for a package was a DD or DM or whatever grouping, this would be more obvious, right?
17:48:13 <rra> bdale: I can just see cases where someone has their own pet packages that they were maintaining before they got involved in Debian, got involved in some other part of Debian, something went horribly wrong, and they left that part of Debian, but they still have their pet packages (for which they may be upstream or something) that they'd like to keep maintaining.
17:48:18 <dondelelcaro> would it just be enough for the packages to be automatically orphaned, but the existing maintainer can ITA them?
17:48:36 <dondelelcaro> and then once they have a sponsor, the ITA bug can be closed?
17:48:43 <bdale> dondelelcaro: interesting thought
17:49:22 <dondelelcaro> and with the assumption that if the existing maintainer ITAs them within a suitable timeperiod, they will override all existing ITAs, unless that case is appealed to the CTTE?
17:49:30 <rra> Also, as vorlon points out, the changing maintainership thing is weird anyway.
17:49:32 <vorlon> dondelelcaro: I think that's fine, provided we also agree that the demoted developer has standing to bring it to the TC if they still want to maintain the package but a DD disagrees
17:49:37 <dondelelcaro> vorlon: right
17:49:38 <rra> Since maintainers change all the time and the tech-ctte is rarely ever involved.
17:49:43 <rra> Including forced changes through orphaning.
17:49:53 <rra> So it's not like we're weighing in on every maintainer change anyway.
17:49:56 <dondelelcaro> vorlon: I sort of think everyone is entitled to bring things to the CTTE, but that seems reasonable.
17:50:01 <rra> By and large people just do stuff, and it all works out.
17:50:15 <dondelelcaro> yeah
17:50:35 <rra> So I guess I would lean towards only involving the TC if there's an actual, not theoretical, conflict.
17:50:44 <dondelelcaro> right
17:50:45 <rra> As in, there's a specific package that someone thinks a person should not be maintainer of and that person agrees.
17:50:54 <rra> Er, disagrees.
17:51:05 <cjwatson> rra: Yes, exactly.
17:51:22 <cjwatson> We have jurisdiction, but that doesn't mean we need to sign off on every change if there's no real dispute.
17:51:24 <rra> If it's just a question of "you can't change maintainers since you're not the TC" but no one else is actually saying they should be the maintainer, then meh, I'm not seeing how we're adding value here.
17:51:52 <vorlon> so are we happy to affirm that the DAM has the authority to orphan packages in this case if they think that's appropriate, and that the TC continues to arbitrate any disagreements?
17:52:21 <bdale> vorlon: I'd be fine with that
17:52:32 <rra> I would not specifically call out the DAM.  I think they have that power just by being DDs.  Any DD has the power to orphan packages -- the MIA team isn't a formal delegation, IIRC.
17:52:37 <dondelelcaro> vorlon: I think so, especially as the orphaning is primarily advisory until someone actually uploads a package with a different maintainer.
17:53:12 <vorlon> ok
17:53:19 <rra> In other words, I think DDs should feel empowered to do what they think the right thing is, and if there's some conflict, or if they want our advice, appeal to the TC.
17:53:34 <vorlon> I'll take a stab at writing up a position statement, then
17:53:37 <dondelelcaro> ok
17:53:40 <rra> Thanks!
17:53:52 <dondelelcaro> #action vorlon write up a position statement about orphaning packages when upload rights are revoked
17:53:57 <bdale> rra: sure, but I think the point of this is to provide aid and comfort to svuorela, et al, so that they don't feel hampered in doing what sensibly needs to be done
17:54:29 <rra> Right -- that's part of why I don't really want to call out the DAM, since I don't see why folks in that situation should necessarily have to have DAM blessing either if they're doing the right thing.
17:54:51 <rra> And if they want someone to help give them cover by having a more official position, I think we're the constitutionally correct body to ask for that cover rather than the DAM.
17:55:04 <bdale> ok, I see your point
17:55:05 <rra> Although I suppose we could delegate to the DAM.  But I'm not sure if that's better or not.
17:55:15 * bdale suspects vorlon can come up with acceptable text
17:55:26 <dondelelcaro> cool
17:55:30 <dondelelcaro> anything else?
17:56:09 <bdale> not from me
17:56:19 <bdale> giving a Debian talk here at #posscon in about 30 mins
17:56:30 <dondelelcaro> #endmeeting