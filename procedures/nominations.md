Nominations to the Technical Committee
--------------------------------------

Constitutional basis
====================

  - §6.1.6 Together with the Project Leader, (the TC may) appoint new members
to itself or remove existing members.
  - §6.3.4 Confidentiality of appointments. The Technical Committee may hold
confidential discussions via private email or a private mailing list or other
means to discuss appointments to the Committee. However, votes on appointments
must be public.

Procedure
=========

This procedure assumes there is one seat to fill. Some of its steps will be
common or run in parallel for multiple seat fillings.

Call for nominations
--------------------
At this point, the TC needs to fill its set of nominees with interested project
members. To achieve this, an email is sent to debian-devel-announce@l.d.o
calling for (self-)nominations to be sent to the private TC alias
debian-ctte-private@debian.org. Current TC members can obviously also nominate
project members.

  - Every nomination is acknowledged.
  - Every non-self nominee is informed of their nomination and is asked to
indicate if they are willing to be considered for appointment. Nominees are
informed of the conditions below.

Communication to the project:

  - The TC *does not* make nominations or their acceptances public;
  - The TC *does* make public:
    - the number of nominees,
    - the number of accepted nominations.

Data collection
---------------------
In order to enhance their decision-making data, TC members will collect some
data about the nominees and publish their findings to the private alias. The
point of that data collection is to get a better understanding of their skills,
their history within the project, their effect on conversations, etc.

Communication to the project:

  - The TC *does not* make the data public.

Shortlisting and picking a preferred candidate
-----------------------------------------------------------------
Given the above data, any current TC member *can* vet for one or multiple
nominees they would like to see on an *internal* ballot. The TC would then use
the Standard Resolution Procedure **in private** to sort the list of candidates
(+ FD).

Communication to the project:

  - The TC *does not* make the ballot with vetted nominees public.
  - The TC *does not* make the result of the **private vote** public.

Question:

  - Does this private vote respect letter and/or intent of §6.3.4 "votes on
appointments must be public"?


Check for eventual DPL veto
----------------------------------------
In order to avoid any potential drama, the TC would **privately** ask the DPL
if they would later accept the nomination of the preffered candidate.

Public vote
----------------
The TC would use the Standard Resolution Procedure **in public** on a ballot
containing only the preferred candidate and FD. Once the result is known or no
longer in doubt (but ideally, once every current member has voted), the DPL is
informed.

Appointment by the DPL
-----------------------------------
When they see fit, the DPL would appoint the candidate as selected by the
public vote; ideally on debian-devel-announce@l.d.o .

(This is really outside of the TC's realm.)

Welcome and Thanks
------------------------------
The TC would then:

  - Congratulate and welcome the new member
  - Thank all nominees for volunteering.
