../scripts/pocket-devotee \
        --option 'A: Andreas Barth <aba>' \
        --option 'B: Don Armstrong <don>' \
        --option 'C: Keith Packard <keithp>' \
        --option 'D: Didier Raboud <odyx>' \
        --option 'E: Tollef Fog Heen <tfheen>' \
        --option 'F: Sam Hartman <hartmans>' \
        --option 'G: Phil Hands <philh>' \
        --option 'H: Margarita Manterola <marga>' <<EOF
keithp: D > A = B = E = F = G > C > H
hartmans: D > C=E=F=G >H > A=B
marga: D > A = B = C = E = F = G = H
philh:  D > A = B = C = E = F = H > G
odyx: D > F > C = E > G = H > A = B
tfheen: D > A = B = C = E = F = G = H
aba: D > C=E=F=G >H > A=B
EOF

