Background/Rationale:

1. In #750135, the Technical Committee was asked by Manuel Fernandez
   Montecelo who should be the maintainer of the Aptitude project.

   Manuel Fernandez Montecelo had been actively committing until his
   commit access was removed by Daniel Hartwig.
  
   Manuel Fernandez Montecelo and Daniel Hartwig took over development
   of Aptitude in 2011 with the support of Christian Perrier, an admin
   for the Aptitude alioth project.
  
   There was friction between Manuel Fernandez Montecelo and Daniel
   Hartwig, which eventually resulted in Manuel Fernandez Montecelo's
   commit access being revoked by Daniel Hartwig.
  
   Since then, Daniel Hartwig has become inactive, and did not comment
   on the issue when requested by the Technical Committee.

2. During the discussion of this issue, Christian Perrier proposed
   that he and Axel Beckert could watch the social aspects of Aptitude
   development and restore Manuel Fernandez Montecelo's commit access.

   Christian still has administrative rights and believes he has the
   technical power to implement his proposal, but requested the advice
   of the technical committee before doing so.

Using the power of the technical committee to provide advice (§6.1.5):

1. The Technical Committee agrees that Christian has the power to
   implement his proposal and encourages him to do so.

2. We hope that Christian and Axel will work to manage the social
   aspects of the Aptitude project, working to recruit new developers,
   building a stronger Aptitude development community, and
   establishing policies and procedures that promote a collaborative
   team.

3. We thank Manuel Fernandez Montecelo for bringing this matter to our
   attention and apologize for our delay in resolving this matter.
