#!/bin/sh
echo "Debian Menu systems"
../scripts/pocket-devotee \
    --option 'A: adopt changes proposed by Charles Plessy (…)' \
    --option 'B: consider that the procedure resulted in consensus, and adopt changes proposed by Charles Plessy (…)' \
    --option 'C: adopt changes proposed by Bill Allombert (…)' \
    --option 'D: adopt changes proposed by Charles Plessy, additionally resolve that packages providing a .desktop file shall not also provide a menu file (…)' \
    --option 'Z: Further Discussion' \
    --default-option 'Z' \
    --quorum 2 \
    << EOF
hartmans: D > B > A > Z > C
don: D > A = B > C > Z
tfheen: D > A = B > Z > C
odyx: D > B > A > Z > C
keithp: D > B > A > Z > C
bdale: D > A = B > Z > C
aba: D > A > Z > B > C
EOF
