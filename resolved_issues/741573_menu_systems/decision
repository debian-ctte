===== TITLE

Debian Menu System

===== WEB SUMMARY

The technical committee adopts the changes to policy regarding menu
entries proposed by Charles Plessy, and additionally resolves that
packages providing desktop files shall not also provide a menu file.

===== EMAIL INTRO

The technical committee was asked in #741573 to decide an issue of
Debian technical policy regarding menu regarding the menu system.

===== EMAIL EPILOGUE

The technical committee would like to thank everyone who participated
in the discussion of #741573 and the patience of the Policy Editors as
the technical committee worked through this issue very slowly.

===== DECISION

Whereas:

   1. The Debian Policy Manual states (§9.6) that 'The Debian menu
      package provides a standard interface between packages providing
      applications and "menu programs"'. It further states that 'All
      packages that provide applications that need not be passed any
      special command line arguments for normal operations should
      register a menu entry for those applications'.

   2. All details about menu system requirement are delegated to the
      Debian Menu sub-policy and Debian Menu System manuals (the
      "Debian menu system").

   3. An external specification, the Freedesktop Desktop Entry
      Specification (the ".desktop spec"), with native support in many
      X desktop environments, has appeared since the Debian Menu
      system was developed. The .desktop spec offers a fairly strict
      super-set of Debian Menu system functionality.

   4. The .desktop specification has significant technical benefits
      for users over the Debian menu system. The .desktop
      specification works together with the freedesktop.org mime type
      and icon specifications to provide operations expected by
      desktop users from other environments, such as Mac OS X or
      Windows. As such, applications must provide a .desktop file to
      operate well in most desktop environments.

   5. The Debian Technical Committee has been asked to resolve a
      dispute between maintainers of Debian Policy over a change that

      i. incorporates the description of the FreeDesktop menu system
         and its use in Debian for listing program in desktop menus
         and associating them with media types

     ii. softens the wording on the Debian Menu system to reflect that
         in Jessie it will be neither displayed nor installed by
         default on standard Debian installations.

 Therefore:

   The Technical Committee has reviewed the underlying technical
   issues around this question and has resolved that Debian will be
   best served by migrating away from our own Debian Menu System and
   towards the common Freedesktop Desktop Entry Specification, and
   that menu information for applications should not be duplicated in
   two different formats.

   To encourage this change, we make menu files optional, ask that
   packages include .desktop files as appropriate and prohibit
   packages from providing both menu and .desktop files for the same
   application.

Using its power under §6.1.1 to decide on any matter of technical
policy, and its power under §6.1.5 to offer advice:

   1. The Technical Committee adopts the changes proposed by Charles
      Plessy in ba679bff[1].

   2. In addition to those changes, the Technical Committee resolves
      that packages providing a .desktop file shall not also provide a
      menu file for the same application.

   3. We further resolve that "menu programs" should not depend on the
      Debian Menu System and should instead rely on .desktop file
      contents for constructing a list of applications to present to
      the user.

   4. We advise the maintainers of the 'menu' package to update that
      package to reflect this increased focus on .desktop files by
      modifying the 'menu' package to use .desktop files for the
      source of menu information in addition to menu files.

   5. Discussion of the precise relationship between menu file
      section/hints values and .desktop file Categories values may be
      defined within the Debian Menu sub-policy and Debian Menu
      System.

   6. Further modifications to the menu policy are allowed using the
      normal policy modification process.

[1]: https://anonscm.debian.org/cgit/dbnpolicy/policy.git/commit/?id=ba679bff76f5b9152f43d5bc901b9b3aad257479
