#!/bin/sh
../scripts/pocket-devotee \
        --option 'A: Didier Raboud <odyx>' \
        --option 'B: Tollef Fog Heen <tfheen>' \
        --option 'C: Phil Hands <philh>' \
        --option 'D: Margarita Manterola <marga>' \
        --option 'E: David Bremner <bremner>' \
        --option 'F: Niko Tyni <ntyni>' \
        --option 'G: Gunnar Wolf <gwolf>' \
        --option 'H: Simon McVittie <smcv>' <<EOF
gwolf: D > A > B = C = E = F > G > H
smcv: A = B = C = D = E = F = G > H
odyx: A = B = C = E > D = F = G > H
ntyni: C = D > E = F = G > A = B > H
tfheen: C = D = E > A = B = F = G > H
marga: E > A = B = C = D > F = G = H
EOF
