../scripts/pocket-devotee \
        --option 'A: Keith Packard <keithp>' \
        --option 'B: Didier Raboud <odyx>' \
        --option 'C: Tollef Fog Heen <tfheen>' \
        --option 'D: Sam Hartman <hartmans>' \
        --option 'E: Phil Hands <philh>' \
        --option 'F: Margarita Manterola <marga>' \
        --option 'G: David Bremner <bremner>'  <<EOF
odyx: B > E > D = C > F = G > A
hartmans: B > F >  D > C = E  = A = G
keithp: B > A = C = D = E = F > G
marga: B > A = C = D = E = G > F
bremner: B = D > A = C = E = F > G
philh: B > C = D = F = G = E > A
tfheen: B > D = E > A = C = F = G
EOF

