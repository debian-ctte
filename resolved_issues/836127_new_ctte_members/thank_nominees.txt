From: Don Armstrong <don@debian.org>
To: debian-devel-announce@lists.debian.org
Subject: TC Nominations closed; thanks to all nominees for agreeing to serve
Reply-to: debian-ctte-private@debian.org
Mail-Followup-To: debian-ctte-private@debian.org

The Technical Committee would like to thank all of the nominees,
especially those who agreed to serve Debian on the Technical
Committee. In addition, we also appreciate the effort of everyone who
nominated someone else and provided information about positive
interactions with those nominees to the Committee.

The Technical Committee has begun private deliberations, and will
recommend a nominee to the Project Leader for approval under §6.2.2.
