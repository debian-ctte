* Issue http://bugs.debian.org/682010 http://bugs.debian.org/675971
** Mumble in unstable/testing currently cannot interact with other clients and servers
   + Due to the removal of celt http://bugs.debian.org/676592 and disabling of celt compilation options
   + Mumble dropping speex in unstable and speex not being selected at higher bandwidths
   + http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=675971#51
   + Interoperation: http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=675971#61
* Possible solutions
** Use speex instead
   + Server (and clients?) do not select speex as an option unless bandwidth is low
     + May be resolved by Thorvald Natvig with a hack
   + Clients cannot currently report speex version (support?) during codec selection process
   + Requires code modification for selection process and re-enabling speex
   + Clients do not use speex unless bandwidth is <= 32kb/s
   + After mods should be backward compatible with existing clients
** Include celt 0.7.1 as a convenience copy
   + Security Issues with embedded copies
     + Mitigated as mumble would have the only copy
   + Unspecified possible security issues
     + Potential remote crasher
   + -348 is currently this way in testing
   + Deprecated upstream in favor of opus
** Do not release with mumble
   + Unsatisfactory to users of mumble
** Upload a celt 0.7.1 package
   + No maintainer desires to deal with this (apparently?)
   + Upstream do not wish additional packages to use celt; wish transition to opus
   + Unspecified possible security issues
   + Proliferates celt library downstream
   + Deprecated upstream
** Use only opus
   + Opus itself released upstream
   + Code to enable opus in mumble has not been released
   + Will not communicate with non-opus clients or servers
   + Unlikely to be RM acceptable at this point
* Open questions
** Can speex be made to be an option?
   + Thorvald thinks so; no patch as of yet (off for a week?)
     + Ron asked to work on this with Thorvald; report back before 13th of August.
** Is a convenience copy acceptable, assuming mumble is the only thing with it?
   + Possible remote crasher bug is the primary objection to allowing this
** What are the other clients that we want to make sure the mumble servers can communicate with?
   + Note that this table is only for a single client connected to a single server
   + 348 is 1.2.3-348-g317f5a0-1 (currently in wheezy)
   + 349 is 1.2.3-349-g315b5f5-2 (currently in sid)
|--------------------+----------------------+----------------+-----------+-----------|
| client/server      | Deb 1.2.2-6+squeeze1 | Deb 1.2.3-2+b2 | Deb "348" | Deb "349" |
|--------------------+----------------------+----------------+-----------+-----------|
| Deb. Client "348"  | Yes                  | Yes            | Yes       | Yes       |
| Deb. Client "349"  | No                   | No             | Yes       | Yes       |
| Win. Client 1.2.3a | Yes                  | Yes            | Yes       | Yes       |
| Win. Client "361"  | Yes                  | Yes            | Yes       | Yes       |
| Mac  Client 1.2.2  | Yes                  | Yes            | Yes       | Yes       |
|--------------------+----------------------+----------------+-----------+-----------|
* Resolutions
  Context:

  1. The questions surrounding the codecs in mumble, especially celt,
     have been referred to the Technical Committee.

  2. The mumble maintainers have stated their willingness to follow
     our advice (Constitution 6.1(5)).  This may or may not amount to
     a delegation to us of the decision (6.1(3)) but in any case we
     merely need to state our reasoning and conclusions and are not
     being asked to overrule the maintainer.

  Release Critical status of celt 0.7.1 in mumble:

  3. mumble is a useful and fairly widely-used voice chat program.

  4. Distributions of mumble (from other distros and upstream)
     currently implement the celt 0.7.1 codec as a baseline.  It does
     not appear to the TC that (in wheezy) the provision of any other
     codec obviates the need for mumble to support celt 0.7.1.
     mumble with celt 0.7.1 has been tested and found to interoperate
     properly with nearly all other mumble versions.

  5. Consequently, we consider the lack of celt 0.7.1 support in
     mumble a release-critical bug.

  Security risks from celt 0.7.1:

  6. While the upstream security support situation for celt 0.7.1 is
     not ideal, the TC does not consider that the security risks
     associated with celt 0.7.1 in mumble are intolerable.

  7. The Debian Security Team have stated that they have no objection
     to including celt 0.7.1 in mumble in wheezy.

  8. Consequently, mumble should remain in wheezy with celt 0.7.1
     (the alternative being to remove mumble as unfit for release).

  Packaging approach:

  9. There are no other packages intended for wheezy which ought to
     want this codec.

  10. Providing separate celt library in wheezy is undesirable because
     it might promote the use of a codec which we are planning to
     retire in the medium to long term.

  11. While embedded code copies are in general to be avoided because
     lead to proliferation of multiple versions, that therefore does
     not apply in this case.

  12. The upstream mumble source already contemplates building with
     various embedded versions of celt.

  13. There is no reason to support any other version of celt in
     mumble.

  14. Consequently, the mumble source package should be configured to
     use an embedded copy of celt 0.7.1.  (If necessary the embedded
     copy of celt in the source package should be updated to the
     actual 0.7.1.)

  We therefore recommend that:

  15. The mumble maintainers, with appropriate help from other
     interested parties, should prepare an upload of mumble for wheezy
     with
       - embedded celt 0.7.1 enabled
       - no other version of celt enabled
       - whatever other release-critical bugfixes they consider
          relevant (subject to any appropriate discussion with the
          release team as necessary)
       - closing #675971.

  16. #675971 should remain at an RC severity, be untagged wontfix,
     and maintained open until it is closed as discussed above.

  17. If the release team are content with the other changes
     in the new mumble package, the new version should be unblocked
     to propagate into wheezy.

  18. After that propagation, the separate celt packages should be
     removed from wheezy.  This should be requested by the celt
     maintainer filing a removal bug in the normal way, after mumble
     with embedded celt 0.7.1 has propagated to wheezy.
* Involved parties
** Chris.Knadle@coredump.us, Ron <ron@debian.org>, 682010@bugs.debian.org, 675971@bugs.debian.org, Nicos Gollan <gtdev@spearhead.de>, Thorvald Natvig <thorvald@natvig.com>
