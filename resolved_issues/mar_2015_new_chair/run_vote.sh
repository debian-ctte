#!/bin/sh
../scripts/pocket-devotee \
    --option 'A:don' \
    --option 'B:aba' \
    --option 'C:vorlon' \
    --option 'D:keithp' \
    --option 'E:odyx' \
    --option 'F:tfheen' \
    --option 'G:hartmans' \
    --option 'H:bdale'  \
    --no-default-option \
    << EOF
bdale: A > B=D=E=F=G > C
aba: A > D > E=F=G > C > B > H
tfheen: A > B=D=E=F=G > C
odyx: A > B=D > E=F=G > C=H
don: A > B=C=D=E=F=G=H
EOF

