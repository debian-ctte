===== TITLE

Alternate Dependencies on non-free packages in main

===== WEB SUMMARY

The committee resolves that alternatiave dependencies on non-free
packages are permisible in main.

===== EMAIL INTRO

The technical committe was asked in #681419 by the policy maintainers
to determine as a matter of technical whether alternative dependencies
on non-free packages were acceptable in main.

===== EMAIL EPILOGUE

The committee would like to thank everyone who participated in the
discussion of #681419.

===== DECISION

Whereas:

1. The Debian Policy Manual states (§2.2.1) that packages in main
   "must not require or recommend a package outside of main for
   compilation or execution".  Both "Depends: package-in-non-free" and
   "Recommends: package-in-non-free" clearly violate this requirement.
   The Technical Committee has been asked to determine whether a
   dependency of the form "package-in-main | package-in-non-free"
   complies with this policy requirement, or whether virtual packages
   must instead be used to avoid mentioning the non-free alternative.

2. Both options have the following effects in common, meeting the
   standard that main should be functional and useful while being
   self-contained:

  (a) Package managers configured to consider only main will install
      package-in-main.

  (b) Package managers configured to consider both main and non-free
      will prefer to install package-in-main, but may install
      package-in-non-free instead if so instructed, or if
      package-in-main is uninstallable.

  (c) If package-in-non-free is already installed, package managers
      will proceed without installing package-in-main.

3. The significant difference between these two options is that the
   former makes the non-free alternative visible to everyone who
   examines the dependency relationship, while the latter does not.

4. Merely mentioning that a non-free alternative exists does not
   constitute a recommendation of that alternative.  For example, many
   free software packages state quite reasonably that they can be
   compiled and executed on non-free platforms.

5. Furthermore, virtual packages are often a clumsy way to express
   these kinds of alternatives.  If a package happens to require any
   of several implementations of a facility that have a certain
   option, then it can either depend on suitable alternatives
   directly, or its maintainer can first attempt to have fine-grained
   virtual packages added to each of the packages they wish to permit.
   In some cases this may be appropriate, but it can easily turn into
   quite a heavyweight approach.

Therefore:

6. The Technical Committee resolves that alternative dependencies of
   the form "Depends: package-in-main | package-in-non-free" are
   permissible in main, and do not constitute a violation of the
   policy clause cited in point 1.

7. We nevertheless recommend that packages in main consider carefully
   whether this might cause the inadvertent installation of non-free
   packages due to conflicts, especially those with usage
   restrictions.