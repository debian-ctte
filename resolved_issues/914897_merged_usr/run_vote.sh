../scripts/pocket-devotee \
        --option 'W: `weak`: both directory schemes are allowed, but packages should only be built on hosts with classical directory schemes (or in such chroots)' \
        --option 'M: `middle`: both directory schemes are allowed, and packages (including official packages) can be built on hosts with either classical or "merged `/usr`" directory schemes' \
        --option 'H: `hard`: both directory schemes are allowed, but packages should only be built on hosts with "merged `/usr`" directory schemes (or in such chroots)' \
        --option 'F: Further Discussion' <<EOF
odyx: H > M > W > F
bremner: M > W > H > F
gwolf: H > M > F > W
ntyni: M > H > W > F
marga: M > W > H > F
fil: H > M > F > W
marga (casting vote): M > H = W = F
EOF
