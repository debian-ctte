Starting results calculation at Tue Jul 21 10:02:32 2020

/--ABCDEFGH
V: 31233344 spwhitton
V: 21222233 bremner
V: 21222233 gwolf
V: 36124578 marga
V: 21122233 ehashman
V: 31233344 ntyni
V: 31233344 philh
V: 11111233 smcv
Option A "Philip Hands"
Option B "Margarita Manterola"
Option C "David Bremner"
Option D "Niko Tyni"
Option E "Gunnar Wolf"
Option F "Simon McVittie"
Option G "Sean Whitton"
Option H "Elana Hashman"

In the following table, tally[row x][col y] represents the votes that
option x received over option y.

                  Option
              A     B     C     D     E     F     G     H 
            ===   ===   ===   ===   ===   ===   ===   === 
Option A            1     0     0     1     2     8     8 
Option B      6           5     6     6     7     8     8 
Option C      5     1           5     5     6     8     8 
Option D      1     1     0           1     2     8     8 
Option E      0     1     0     0           2     8     8 
Option F      0     1     0     0     0           8     8 
Option G      0     0     0     0     0     0           1 
Option H      0     0     0     0     0     0     0       



Looking at row 2, column 1, B
received 6 votes over A

Looking at row 1, column 2, A
received 1 votes over B.



  Option B defeats Option A by (   6 -    1) =    5 votes.
  Option C defeats Option A by (   5 -    0) =    5 votes.
  Option D defeats Option A by (   1 -    0) =    1 votes.
  Option A defeats Option E by (   1 -    0) =    1 votes.
  Option A defeats Option F by (   2 -    0) =    2 votes.
  Option A defeats Option G by (   8 -    0) =    8 votes.
  Option A defeats Option H by (   8 -    0) =    8 votes.
  Option B defeats Option C by (   5 -    1) =    4 votes.
  Option B defeats Option D by (   6 -    1) =    5 votes.
  Option B defeats Option E by (   6 -    1) =    5 votes.
  Option B defeats Option F by (   7 -    1) =    6 votes.
  Option B defeats Option G by (   8 -    0) =    8 votes.
  Option B defeats Option H by (   8 -    0) =    8 votes.
  Option C defeats Option D by (   5 -    0) =    5 votes.
  Option C defeats Option E by (   5 -    0) =    5 votes.
  Option C defeats Option F by (   6 -    0) =    6 votes.
  Option C defeats Option G by (   8 -    0) =    8 votes.
  Option C defeats Option H by (   8 -    0) =    8 votes.
  Option D defeats Option E by (   1 -    0) =    1 votes.
  Option D defeats Option F by (   2 -    0) =    2 votes.
  Option D defeats Option G by (   8 -    0) =    8 votes.
  Option D defeats Option H by (   8 -    0) =    8 votes.
  Option E defeats Option F by (   2 -    0) =    2 votes.
  Option E defeats Option G by (   8 -    0) =    8 votes.
  Option E defeats Option H by (   8 -    0) =    8 votes.
  Option F defeats Option G by (   8 -    0) =    8 votes.
  Option F defeats Option H by (   8 -    0) =    8 votes.
  Option G defeats Option H by (   1 -    0) =    1 votes.


The Schwartz Set contains:
	 Option B "Margarita Manterola"



-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

The winners are:
	 Option B "Margarita Manterola"

-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

