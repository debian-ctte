A  Advice: sysvinit compatibility in jessie and multiple init support
=====================================================================

    The following is technical advice offered to the project by the
    Technical Committee under section 6.1.5 of the constitution.  It does
    not constitute an override of maintainer decisions past or future.

    Software should support as many architectures as reasonably possible,
    and it should normally support the default init system on all
    architectures for which it is built.  There are some exceptional cases
    where lack of support for the default init system may be appropriate,
    such as alternative init system implementations, special-use packages
    such as managers for non-default init systems, and cooperating
    groups of packages intended for use with non-default init systems.
    However, package maintainers should be aware that a requirement for a
    non-default init system will mean the software will be unusable for
    most Debian users and should normally be avoided.

    Package maintainers are strongly encouraged to merge any contributions
    for support of any init system, and to add that support themselves if
    they're willing and capable of doing so.  In particular, package
    maintainers should put a high priority on merging changes to support
    any init system which is the default on one of Debian's non-Linux
    ports.

    For the jessie release, all software that currently supports being run
    under sysvinit should continue to support sysvinit unless there is no
    technically feasible way to do so.  Reasonable changes to preserve
    or improve sysvinit support should be accepted through the jessie
    release.  There may be some loss of functionality under sysvinit if
    that loss is considered acceptable by the package maintainer and
    the package is still basically functional, but Debian's standard
    requirement to support smooth upgrades from wheezy to jessie still
    applies, even when the system is booted with sysvinit.

    The Technical Committee offers no advice at this time on sysvinit
    support beyond the jessie release.  There are too many variables at
    this point to know what the correct course of action will be.
