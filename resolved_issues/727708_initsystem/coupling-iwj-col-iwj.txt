L  Software may not depend on a specific init system
====================================================

  Rationale

    The default init system decision is limited to selecting a default
    initsystem for jessie.  We expect that Debian will continue to
    support multiple init systems for the foreseeable future; we
    continue to welcome contributions of support for all init systems.

  Rubric

    Therefore, for jessie and later releases, we exercise our power to
    set technical policy (Constitution 6.1.1):

  Loose coupling

    In general, software may not require a specific init system to be
    pid 1.  The exceptions to this are as follows:

     * alternative init system implementations
     * special-use packages such as managers for init systems
     * cooperating groups of packages intended for use with specific init
       systems

    provided that these are not themselves required by other software
    whose main purpose is not the operation of a specific init system.

    Degraded operation with some init systems is tolerable, so long as
    the degradation is no worse than what the Debian project would
    consider a tolerable (non-RC) bug even if it were affecting all
    users.  So the lack of support for a particular init system does not
    excuse a bug nor reduce its severity; but conversely, nor is a bug
    more serious simply because it is an incompatibility of some software
    with some init system(s).

    Maintainers are encouraged to accept technically sound patches
    to enable improved interoperation with various init systems.

  GR rider

    If the project passes (before the release of jessie) by a General
    Resolution, a "position statement about issues of the day", on the
    subject of init systems, the views expressed in that position
    statement entirely replace the substance of this TC resolution; the
    TC hereby adopts any such position statement as its own decision.

    Such a position statement could, for example, use these words:

       The Project requests (as a position statement under s4.1.5 of the
       Constitution) that the TC reconsider, and requests that the TC
       would instead decide as follows:
