../scripts/pocket-devotee \
	--option 'A: Don Armstrong' \
	--option 'B: Andreas Barth' \
	--option 'C: Phil Hands' \
	--option 'D: Sam Hartman' \
	--option 'E: Tollef Fog Heen' \
	--option 'F: Keith Packard' \
	--option 'G: Didier Raboud' <<EOF
hartmans: G>B=E>F=D=C>A
odyx: G>D>A>B=C=E=F
don: G>A=B=C=D=E=F
philh: G>A=B=C=E=F
EOF
