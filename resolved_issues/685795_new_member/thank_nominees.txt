From: Don Armstrong <don@debian.org>
To: debian-devel-announce@lists.debian.org
Subject: CTTE Nominations closed; thanks to all nominees for agreeing to serve
Reply-to: debian-ctte-private@debian.org
Mail-Followup-To: debian-ctte-private@debian.org

The Technical Committee would like to thank all of the following
nominees for agreeing to serve Debian on the Technical Committee:

<<list of nominees>>

The Technical Committee has begun private deliberations, and will
recommend a nominee to the Project Leader for approval under §6.2.2.

Anyone who wishes to submit information about any of the nominees
(both praise and concerns) should e-mail
debian-ctte-private@debian.org, and the CTTE will take the information
into account.

